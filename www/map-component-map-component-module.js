(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-component-map-component-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/map-component.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/map-component.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home/tabs/map\"></ion-back-button>\n    </ion-buttons>\n    <ion-title></ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col style=\"text-align: center;border: 0.5px solid #000;\" *ngFor='let user of userList' size=\"4\">\n        <div *ngIf=\"user.photoList.length &gt; 0\" (click)=\"getUserProfile(user)\">\n          <img style=\"border-radius: 50%;max-height: 85px;\" src=\"{{imgPath}}{{user.photoList[0]['photo_name']}}\" >\n        </div>\n        <div (click)=\"getUserProfile(user)\">{{user.name?user.name:user.email_address}}</div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/map-component/map-component-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/map-component/map-component-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: MapComponentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponentPageRoutingModule", function() { return MapComponentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_component_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map-component.page */ "./src/app/map-component/map-component.page.ts");




var routes = [
    {
        path: '',
        component: _map_component_page__WEBPACK_IMPORTED_MODULE_3__["MapComponentPage"]
    },
    {
        path: 'user-profile',
        loadChildren: function () { return __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ./user-profile/user-profile.module */ "./src/app/map-component/user-profile/user-profile.module.ts")).then(function (m) { return m.UserProfilePageModule; }); }
    }
];
var MapComponentPageRoutingModule = /** @class */ (function () {
    function MapComponentPageRoutingModule() {
    }
    MapComponentPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MapComponentPageRoutingModule);
    return MapComponentPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/map-component/map-component.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/map-component/map-component.module.ts ***!
  \*******************************************************/
/*! exports provided: MapComponentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponentPageModule", function() { return MapComponentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _map_component_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./map-component-routing.module */ "./src/app/map-component/map-component-routing.module.ts");
/* harmony import */ var _map_component_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map-component.page */ "./src/app/map-component/map-component.page.ts");







var MapComponentPageModule = /** @class */ (function () {
    function MapComponentPageModule() {
    }
    MapComponentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _map_component_routing_module__WEBPACK_IMPORTED_MODULE_5__["MapComponentPageRoutingModule"]
            ],
            declarations: [_map_component_page__WEBPACK_IMPORTED_MODULE_6__["MapComponentPage"]]
        })
    ], MapComponentPageModule);
    return MapComponentPageModule;
}());



/***/ }),

/***/ "./src/app/map-component/map-component.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/map-component/map-component.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21hcC1jb21wb25lbnQvbWFwLWNvbXBvbmVudC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/map-component/map-component.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/map-component/map-component.page.ts ***!
  \*****************************************************/
/*! exports provided: MapComponentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponentPage", function() { return MapComponentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");






var MapComponentPage = /** @class */ (function () {
    function MapComponentPage(gen, userService, router, activatedroute) {
        this.gen = gen;
        this.userService = userService;
        this.router = router;
        this.activatedroute = activatedroute;
        this.userList = [];
        this.invitaionCount = 0;
        this.imgPath = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].root_URL;
        this.mapZipIcon = this.activatedroute.snapshot.paramMap.get('mapZipIcon');
    }
    MapComponentPage.prototype.ngOnInit = function () {
        this.getuserList();
    };
    MapComponentPage.prototype.getUserProfile = function (user) {
        console.log(this.gen.getUserInfo().isPad == 0 && this.gen.getInvitaionCount() < 3);
        console.log(this.gen.getUserInfo().isPad == 0);
        console.log(this.gen.getInvitaionCount() < 3);
        if (this.gen.getUserInfo().isPad == 1 || (this.gen.getUserInfo().isPad == 0 && this.gen.getInvitaionCount() < 3)) {
            this.router.navigate(['home/map-component/user-profile', {
                    userProfile: JSON.stringify(user),
                    pageType: 0
                }]);
        }
        else {
            this.gen.presentAlert('', '', 'Please purchase package you have exceeded free limit');
        }
    };
    MapComponentPage.prototype.getuserList = function () {
        var _this = this;
        this.gen.presentLoading("please wait..");
        this.userService.getuserlistByZip(this.mapZipIcon).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                console.log(v);
                this.gen.closePresentLoading();
                if (v.success) {
                    this.userList = v.data;
                    this.gen.setInvitaionCount(v.senderCount);
                }
                return [2 /*return*/];
            });
        }); });
    };
    MapComponentPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
    ]; };
    MapComponentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map-component',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./map-component.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/map-component.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./map-component.page.scss */ "./src/app/map-component/map-component.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"], _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], MapComponentPage);
    return MapComponentPage;
}());



/***/ })

}]);
//# sourceMappingURL=map-component-map-component-module.js.map