(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registersummary-registersummary-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n    <div class=\"other\">\n        <div class=\"mainDiv uploadInfo\">\n            <div class=\"tableCell\">\n                <h2><span>Upload <b>Information</b></span></h2>\n                <h2><span>Profile <b>Picture</b></span></h2>\n                <img *ngIf=\"userPhoto!='undefined'\" (click)=\"selectImage()\" src=\"{{photoPath}}thumbnail/{{userPhoto}}_thumb.jpg\" class=\"round-button-create-profile\" style=\"height:100px;width:100px;\">\n                <a *ngIf=\"userPhoto=='undefined'\" (click)=\"selectImage()\" href=\"JavaScript:void(0)\" class=\"round-button-create-profile Photo-background\" style=\"margin-top: 20%;\">\n                    <ion-icon *ngIf=\"userPhoto=='undefined'\" name=\"add-outline\" style=\"font-size: 50px;margin-top: 17%;color: darkgrey;\"></ion-icon>\n                </a>\n                <div class=\"form-div\">\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Name</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"text\" [(ngModel)]=\"NickName\" class=\"myselect\">\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Email</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"email\" class=\"myselect\" readonly>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">username</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"username\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Birthday</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"number\" [(ngModel)]=\"userBirthday\" class=\"myselect\">\n                                        <!-- <ion-datetime class=\"general-input ion-input-class\" [(ngModel)]=\"userBirthday\" displayFormat=\"DD-MMM-YYYY\" placeholder=\"BIRTHDAY\" min=\"1950-03-14\" max=\"2001-01-01\"></ion-datetime> -->\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Mobile</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userMobile\" class=\"myselect\" readonly>\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Area</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userArea\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Pincode</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userPincode\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">State</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userState\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">City</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"usercity\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Gender</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"preferredGender\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Height</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userheight\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ht of userheightarray\">{{ht.height}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Weight</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userweight\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ht of userweightarray\">{{ht.val}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Zodiac</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userzodiac\" class=\"myselect\" placeholder=\"{{zodiactext}}\">\n                                            <ion-select-option *ngFor=\"let ch of userZodiacarray\">{{ch.type}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Child</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userchild\" class=\"myselect\" [selectedText]=\"userchildtext\">\n                                            <ion-select-option *ngFor=\"let ch of userchildarray\">{{ch.child}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Prefecen</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userPreference\" placeholder=\"Select Prefecen\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <!-- <div class=\"formElement\">\n                        <div class=\"ion-textarea-Box\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"12\">\n                                        <ion-textarea [(ngModel)]=\"userAboutme\" placeholder=\"About Me\" class=\"box-label label-padding\"></ion-textarea>\n                                    </ion-col>\n\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div> -->\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">BodyType</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userbodytype\" class=\"myselect\" [selectedText]=\"bodytypetext\">\n\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Smoke</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"usersmoke\" class=\"myselect\" [selectedText]=\"usersmoketext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Drinks</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userdrinks\" class=\"myselect\" [selectedText]=\"userdrinkstext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Race</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userRace\" placeholder=\"Select Race\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n\n                    <ion-grid>\n                        <ion-row class=\"ion-text-center\">\n                            <ion-col>\n                                <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_registerfinish()\">\n                                    <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                                </a>\n\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/registersummary/registersummary-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: RegistersummaryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPageRoutingModule", function() { return RegistersummaryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registersummary_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registersummary.page */ "./src/app/signup/registersummary/registersummary.page.ts");




var routes = [
    {
        path: '',
        component: _registersummary_page__WEBPACK_IMPORTED_MODULE_3__["RegistersummaryPage"]
    }
];
var RegistersummaryPageRoutingModule = /** @class */ (function () {
    function RegistersummaryPageRoutingModule() {
    }
    RegistersummaryPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegistersummaryPageRoutingModule);
    return RegistersummaryPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.module.ts ***!
  \******************************************************************/
/*! exports provided: RegistersummaryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPageModule", function() { return RegistersummaryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registersummary_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registersummary-routing.module */ "./src/app/signup/registersummary/registersummary-routing.module.ts");
/* harmony import */ var _registersummary_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registersummary.page */ "./src/app/signup/registersummary/registersummary.page.ts");







var RegistersummaryPageModule = /** @class */ (function () {
    function RegistersummaryPageModule() {
    }
    RegistersummaryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _registersummary_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistersummaryPageRoutingModule"]
            ],
            declarations: [_registersummary_page__WEBPACK_IMPORTED_MODULE_6__["RegistersummaryPage"]]
        })
    ], RegistersummaryPageModule);
    return RegistersummaryPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.page.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.box-left {\n  background: #37A4DC;\n  padding-right: -10px;\n}\n\n.box-label {\n  background: #CCD1D1;\n  font-size: small;\n}\n\n.label-padding {\n  padding-left: 15px;\n}\n\n.myselect-label {\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9yZWdpc3RlcnN1bW1hcnkvcmVnaXN0ZXJzdW1tYXJ5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyc3VtbWFyeS9yZWdpc3RlcnN1bW1hcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtFQUNBLG9CQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREVBO0VBS0ksZ0JBQUE7QUNISiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9yZWdpc3RlcnN1bW1hcnkvcmVnaXN0ZXJzdW1tYXJ5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teXNlbGVjdCB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIHBhZGRpbmc6IDAgMTVweCAwIDMwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uYm94LWxlZnQge1xuICAgIGJhY2tncm91bmQ6ICMzN0E0REM7XG4gICAgcGFkZGluZy1yaWdodDogLTEwcHg7XG59XG5cbi5ib3gtbGFiZWwge1xuICAgIGJhY2tncm91bmQ6ICNDQ0QxRDE7XG4gICAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmxhYmVsLXBhZGRpbmcge1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbn1cblxuLm15c2VsZWN0LWxhYmVsIHtcbiAgICAvLyAgd2lkdGg6IDEwMCU7XG4gICAgLy8gIGhlaWdodDogNDBweDtcbiAgICAvLyAgcGFkZGluZzogMThweCAxN3B4IDE4cHggMzBweDtcbiAgICAvLyAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgICBmb250LXNpemU6IHNtYWxsO1xufSIsIi5teXNlbGVjdCB7XG4gIGhlaWdodDogNTBweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uYm94LWxlZnQge1xuICBiYWNrZ3JvdW5kOiAjMzdBNERDO1xuICBwYWRkaW5nLXJpZ2h0OiAtMTBweDtcbn1cblxuLmJveC1sYWJlbCB7XG4gIGJhY2tncm91bmQ6ICNDQ0QxRDE7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59XG5cbi5sYWJlbC1wYWRkaW5nIHtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4ubXlzZWxlY3QtbGFiZWwge1xuICBmb250LXNpemU6IHNtYWxsO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.page.ts ***!
  \****************************************************************/
/*! exports provided: RegistersummaryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPage", function() { return RegistersummaryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");











var RegistersummaryPage = /** @class */ (function () {
    function RegistersummaryPage(general, filePath, file, platform, toastCtrl, webview, camera, router, activatedroute, actionSheetController, authSvc) {
        this.general = general;
        this.filePath = filePath;
        this.file = file;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.webview = webview;
        this.camera = camera;
        this.router = router;
        this.activatedroute = activatedroute;
        this.actionSheetController = actionSheetController;
        this.authSvc = authSvc;
        this.photoPath = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].root_URL;
        this.userZodiacarray = [];
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userchildarray = [];
        this.userweightarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.genderprefer = [];
        this.userDrinksarray = [];
    }
    RegistersummaryPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.username = this.activatedroute.snapshot.paramMap.get('username');
        this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('userMobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('userArea');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('userPincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('userState');
        this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
        this.preferredGender = this.activatedroute.snapshot.paramMap.get('preferredGender');
        this.userzodiac = this.activatedroute.snapshot.paramMap.get('userzodiac');
        this.userheight = this.activatedroute.snapshot.paramMap.get('userheight');
        this.userweight = this.activatedroute.snapshot.paramMap.get('userweight');
        this.bodytypetext = this.activatedroute.snapshot.paramMap.get('userbodytype');
        this.userchildtext = this.activatedroute.snapshot.paramMap.get('userchild');
        this.usereducationtext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.usersmoketext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.userdrinkstext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.zodiactext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.petpeevestext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.racetext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.userPreference = this.activatedroute.snapshot.paramMap.get('userPreference');
        this.userAboutme = this.activatedroute.snapshot.paramMap.get('userAboutme');
        this.general.presentToast('Summry Birthday ' + this.userBirthday);
        this.showsmoke();
        this.showgender();
        this.showseight();
        this.showheight();
        this.showZodiac();
        this.showchild();
    };
    RegistersummaryPage.prototype.showchild = function () {
        this.userchildarray = [{ child: 'YES' },
            { child: 'NO' }];
    };
    RegistersummaryPage.prototype.showZodiac = function () {
        this.userZodiacarray = [{ type: 'Aries' }, { type: 'Taurus' }, { type: 'Gemini' },
            { type: 'Cancer' }, { type: 'Leo' }, { type: 'Virgo' },
            { type: 'Libra' }, { type: 'Scorpio' }, { type: 'Sagittarius' },
            { type: 'Capricorn' }, { type: 'Aquarius and Pisces' }];
    };
    RegistersummaryPage.prototype.ionViewWillLeave = function () {
        this.general.closePresentLoading();
    };
    RegistersummaryPage.prototype.showheight = function () {
        this.userheightarray = [{ height: '<5 Foot' },
            { height: '>5 Foot' },
            { height: '>6 Foot' },
            { height: '<6 Foot' }
        ];
    };
    RegistersummaryPage.prototype.showseight = function () {
        this.userweightarray = [{ val: '< 100' },
            { val: '< 120' },
            { val: '< 150' },
            { val: '< 200' }
        ];
    };
    RegistersummaryPage.prototype.showgender = function () {
        this.genderprefer = [{ gendertype: 'Male' },
            { gendertype: 'Female' }, { gendertype: 'Undecided' }];
    };
    RegistersummaryPage.prototype.showsmoke = function () {
        this.usersmokearray = [{ smoke: 'YES' },
            { smoke: 'NO' }
        ];
    };
    RegistersummaryPage.prototype.goto_registerfinish = function () {
        var _this = this;
        if (this.bodytypetext === 'What\'s your body type?') {
            this.general.presentToast('Please select your body type.');
        }
        else if (this.userheighttext === 'What\'s your height?') {
            this.general.presentToast('Please select height.');
        }
        else if (this.userchildtext === 'Do you have children?') {
            this.general.presentToast('Please select details about children.');
        }
        else if (this.usersmoketext === 'do you smoke?') {
            this.general.presentToast('Please select details about smoking.');
        }
        else {
            this.general.presentLoading("Please wait... ");
            this.authSvc.register(this.NickName, this.email, this.userMobile, this.Pass, this.usercity, this.userArea, this.userPincode, this.userState, this.username, this.userBirthday, this.userPhoto, this.preferredGender, this.userbodytype, this.userheight, this.userchild, this.usersmoke, this.userdrinks, this.userzodiac, this.userRace, this.userPreference, this.userAboutme, this.latitude, this.longitude, this.userweight).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.general.closePresentLoading();
                    console.log(req.data);
                    v = JSON.parse(req.data);
                    console.log(v);
                    if (v.success) {
                        this.router.navigate(['signup/registerfinished', {
                                "userAccount": v.userAccount
                            }]);
                    }
                    else {
                        this.errorToast(v.message);
                    }
                    return [2 /*return*/];
                });
            }); });
        }
    };
    RegistersummaryPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistersummaryPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Select Photo From',
                            buttons: [{
                                    text: 'Load from Library',
                                    role: 'destructive',
                                    icon: 'images',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Use Camera',
                                    role: 'destructive',
                                    icon: 'camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistersummaryPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        });
    };
    RegistersummaryPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + '.jpg';
        return newFileName;
    };
    RegistersummaryPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.updateStoredImages(newFileName);
        }, function (error) {
            _this.general.presentToast('Error while storing file.');
        });
    };
    RegistersummaryPage.prototype.updateStoredImages = function (name) {
        var filePath = this.file.dataDirectory + name;
        var resPath = this.pathForImage(filePath);
        var newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
        this.img = newEntry;
        this.startUpload(newEntry);
    };
    RegistersummaryPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    RegistersummaryPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.general.presentLoading('Uploading the image');
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            _this.general.presentToast('Error while reading file.');
        });
    };
    RegistersummaryPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    RegistersummaryPage.prototype.uploadImageData = function (formData) {
        var _this = this;
        this.authSvc.registerImage(formData).then(function (req) {
            console.log(req);
            _this.general.closePresentLoading();
            var v = JSON.parse(req.data);
            console.log(v.data);
            if (v.success) {
                _this.userPhoto = v.data;
            }
        }).catch(function (err) { _this.general.closePresentLoading(); });
    };
    RegistersummaryPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] }
    ]; };
    RegistersummaryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registersummary',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registersummary.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registersummary.page.scss */ "./src/app/signup/registersummary/registersummary.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], RegistersummaryPage);
    return RegistersummaryPage;
}());



/***/ })

}]);
//# sourceMappingURL=registersummary-registersummary-module.js.map