(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-otheruser-profile-otheruser-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/otheruser-profile/otheruser-profile.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/otheruser-profile/otheruser-profile.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar>\n        <ion-buttons>\n            <ion-button>\n                <ion-icon name=\"arrow-back-outline\" style=\"font-size: 15px; padding-left: 15px;\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    User Profile\n</ion-content>");

/***/ }),

/***/ "./src/app/home/otheruser-profile/otheruser-profile-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/home/otheruser-profile/otheruser-profile-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: OtheruserProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtheruserProfilePageRoutingModule", function() { return OtheruserProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _otheruser_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otheruser-profile.page */ "./src/app/home/otheruser-profile/otheruser-profile.page.ts");




var routes = [
    {
        path: '',
        component: _otheruser_profile_page__WEBPACK_IMPORTED_MODULE_3__["OtheruserProfilePage"]
    }
];
var OtheruserProfilePageRoutingModule = /** @class */ (function () {
    function OtheruserProfilePageRoutingModule() {
    }
    OtheruserProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], OtheruserProfilePageRoutingModule);
    return OtheruserProfilePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/otheruser-profile/otheruser-profile.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/home/otheruser-profile/otheruser-profile.module.ts ***!
  \********************************************************************/
/*! exports provided: OtheruserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtheruserProfilePageModule", function() { return OtheruserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _otheruser_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otheruser-profile-routing.module */ "./src/app/home/otheruser-profile/otheruser-profile-routing.module.ts");
/* harmony import */ var _otheruser_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./otheruser-profile.page */ "./src/app/home/otheruser-profile/otheruser-profile.page.ts");







var OtheruserProfilePageModule = /** @class */ (function () {
    function OtheruserProfilePageModule() {
    }
    OtheruserProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _otheruser_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtheruserProfilePageRoutingModule"]
            ],
            declarations: [_otheruser_profile_page__WEBPACK_IMPORTED_MODULE_6__["OtheruserProfilePage"]]
        })
    ], OtheruserProfilePageModule);
    return OtheruserProfilePageModule;
}());



/***/ }),

/***/ "./src/app/home/otheruser-profile/otheruser-profile.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/home/otheruser-profile/otheruser-profile.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvb3RoZXJ1c2VyLXByb2ZpbGUvb3RoZXJ1c2VyLXByb2ZpbGUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/home/otheruser-profile/otheruser-profile.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/home/otheruser-profile/otheruser-profile.page.ts ***!
  \******************************************************************/
/*! exports provided: OtheruserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtheruserProfilePage", function() { return OtheruserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var OtheruserProfilePage = /** @class */ (function () {
    function OtheruserProfilePage() {
    }
    OtheruserProfilePage.prototype.ngOnInit = function () {
    };
    OtheruserProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-otheruser-profile',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./otheruser-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/otheruser-profile/otheruser-profile.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./otheruser-profile.page.scss */ "./src/app/home/otheruser-profile/otheruser-profile.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], OtheruserProfilePage);
    return OtheruserProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-otheruser-profile-otheruser-profile-module.js.map