(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gettingstart-gettingstart-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n    <h2><span>Profile <b>Picture</b></span></h2>\n    <img *ngIf=\"img\" src=\"{{img.path}}\" (click)=\"selectImage()\" class=\"round-button-create-profile\" style=\"height:100px;width:100px;\">\n    <a href=\"JavaScript:void(0)\" *ngIf=\"!img\" (click)=\"selectImage()\" class=\"round-button-create-profile Photo-background\" style=\"margin-top: 20%;\">\n        <ion-icon *ngIf=\"!img\" name=\"add-outline\" style=\"font-size: 50px;margin-top: 17%;color: darkgrey;\"></ion-icon>\n    </a>\n    <br>\n    <div style=\"width: 70%;margin-left: 15%;\">\n        <div class=\"formElement\">\n            <ion-checkbox slot=\"start\" color=\"light\" checked=\"true\">\n            </ion-checkbox>\n            <ion-label color=\"light\"> Would you like your photo to be blurry?<br/></ion-label>\n        </div>\n        <ion-label color=\"primary\"> You can change this setting later.<br/></ion-label>\n        <br>\n    </div>\n    <div class=\"form-div\">\n        <div class=\"formElement\">\n            <ion-select [(ngModel)]=\"preferredGender\" class=\"selectEvnBox myselect\" style=\"color: #fff;\" placeholder=\"Gender\">\n                <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n            </ion-select>\n        </div>\n        <div class=\"formElement\" style=\"display: none;\">\n            <ion-input type=\"text\" [(ngModel)]=\"username\" class=\"general-input ion-input-class\" placeholder=\"Name\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-datetime class=\"general-input ion-input-class\" [(ngModel)]=\"userBirthday\" displayFormat=\"DD-MMM-YYYY\" placeholder=\"BIRTHDAY\" min=\"1950-03-14\" max=\"2001-01-01\"></ion-datetime>\n        </div>\n\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"register_photo()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: GettingstartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPageRoutingModule", function() { return GettingstartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _gettingstart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gettingstart.page */ "./src/app/signup/gettingstart/gettingstart.page.ts");




var routes = [
    {
        path: '',
        component: _gettingstart_page__WEBPACK_IMPORTED_MODULE_3__["GettingstartPage"]
    }
];
var GettingstartPageRoutingModule = /** @class */ (function () {
    function GettingstartPageRoutingModule() {
    }
    GettingstartPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], GettingstartPageRoutingModule);
    return GettingstartPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.module.ts":
/*!************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.module.ts ***!
  \************************************************************/
/*! exports provided: GettingstartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPageModule", function() { return GettingstartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _gettingstart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gettingstart-routing.module */ "./src/app/signup/gettingstart/gettingstart-routing.module.ts");
/* harmony import */ var _gettingstart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gettingstart.page */ "./src/app/signup/gettingstart/gettingstart.page.ts");







var GettingstartPageModule = /** @class */ (function () {
    function GettingstartPageModule() {
    }
    GettingstartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _gettingstart_routing_module__WEBPACK_IMPORTED_MODULE_5__["GettingstartPageRoutingModule"]
            ],
            declarations: [_gettingstart_page__WEBPACK_IMPORTED_MODULE_6__["GettingstartPage"]]
        })
    ], GettingstartPageModule);
    return GettingstartPageModule;
}());



/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.page.scss":
/*!************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".Photo-background {\n  background-color: #fff;\n}\n\n.myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.selectEvnBox {\n  background-color: transparent;\n  border: 2px solid #fff;\n  border-radius: 35px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9nZXR0aW5nc3RhcnQvZ2V0dGluZ3N0YXJ0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2lnbnVwL2dldHRpbmdzdGFydC9nZXR0aW5nc3RhcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0JBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksNkJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9zaWdudXAvZ2V0dGluZ3N0YXJ0L2dldHRpbmdzdGFydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuUGhvdG8tYmFja2dyb3VuZCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLm15c2VsZWN0IHtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5zZWxlY3RFdm5Cb3gge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMzVweDtcbn0iLCIuUGhvdG8tYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG5cbi5teXNlbGVjdCB7XG4gIGhlaWdodDogNTBweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uc2VsZWN0RXZuQm94IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDM1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.page.ts ***!
  \**********************************************************/
/*! exports provided: GettingstartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPage", function() { return GettingstartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");










var GettingstartPage = /** @class */ (function () {
    function GettingstartPage(authSvc, platform, filePath, file, nav, camera, activatedroute, webview, gen, actionSheetController) {
        this.authSvc = authSvc;
        this.platform = platform;
        this.filePath = filePath;
        this.file = file;
        this.nav = nav;
        this.camera = camera;
        this.activatedroute = activatedroute;
        this.webview = webview;
        this.gen = gen;
        this.actionSheetController = actionSheetController;
        this.genderprefer = [];
    }
    GettingstartPage.prototype.updateMyDate = function ($event) {
        var day = $event.detail.value.day.value;
        var month = $event.detail.value.month.value;
        var year = $event.detail.value.year.value;
        var birthdate = year + '-' + month + '-' + day;
        this.gen.presentToast('Check Birthday' + birthdate);
    };
    GettingstartPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
        this.showgender();
    };
    GettingstartPage.prototype.ionViewWillLeave = function () {
        this.gen.closePresentLoading();
    };
    GettingstartPage.prototype.showgender = function () {
        this.genderprefer = [{ gendertype: 'Male' },
            { gendertype: 'Female' }, { gendertype: 'Undecided' }];
    };
    GettingstartPage.prototype.register_photo = function () {
        if (this.userPhoto === undefined || this.userPhoto === '') {
            this.gen.presentToast('Please select your profile image');
            this.selectImage();
        }
        else if (this.preferredGender === undefined || this.preferredGender === '') {
            this.gen.presentToast('Please select gender');
        }
        else if (this.userBirthday === undefined || this.userBirthday === '') {
            this.gen.presentToast('Please select your birthdate');
        }
        else {
            // this.gen.presentToast('Getting Birthday' + this.userBirthday);
            this.nav.navigateForward(['signup/registeraboutyou', {
                    NickName: this.NickName,
                    email: this.email,
                    Pass: this.Pass,
                    username: this.username,
                    userBirthday: this.userBirthday,
                    mobile: this.userMobile,
                    area: this.userArea,
                    pincode: this.userPincode,
                    state: this.userState,
                    userPhoto: this.userPhoto,
                    usercity: this.usercity,
                    latitude: this.latitude,
                    longitude: this.longitude,
                    preferredGender: this.preferredGender
                }]);
        }
    };
    GettingstartPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Select Photo From',
                            buttons: [{
                                    text: 'Load from Library',
                                    role: 'destructive',
                                    icon: 'images',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Use Camera',
                                    role: 'destructive',
                                    icon: 'camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GettingstartPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        });
    };
    GettingstartPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + '.jpg';
        return newFileName;
    };
    GettingstartPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.updateStoredImages(newFileName);
        }, function (error) {
            _this.gen.presentToast('Error while storing file.');
        });
    };
    GettingstartPage.prototype.updateStoredImages = function (name) {
        var filePath = this.file.dataDirectory + name;
        var resPath = this.pathForImage(filePath);
        var newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
        this.img = newEntry;
        this.startUpload(newEntry);
    };
    GettingstartPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    GettingstartPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.gen.presentLoading('Uploading the image');
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            _this.gen.presentToast('Error while reading file.');
        });
    };
    GettingstartPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    GettingstartPage.prototype.uploadImageData = function (formData) {
        var _this = this;
        this.authSvc.registerImage(formData).then(function (req) {
            console.log(req);
            _this.gen.closePresentLoading();
            var v = JSON.parse(req.data);
            console.log(v.data);
            if (v.success) {
                _this.userPhoto = v.data;
            }
        }).catch(function (err) { _this.gen.closePresentLoading(); });
    };
    GettingstartPage.ctorParameters = function () { return [
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__["File"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] }
    ]; };
    GettingstartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gettingstart',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gettingstart.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gettingstart.page.scss */ "./src/app/signup/gettingstart/gettingstart.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"],
            _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]])
    ], GettingstartPage);
    return GettingstartPage;
}());



/***/ })

}]);
//# sourceMappingURL=gettingstart-gettingstart-module.js.map