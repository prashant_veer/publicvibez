(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["postlogin-postlogin-module"],{

/***/ "./node_modules/gl-ionic-background-video/dist/esm/es5/index.js":
/*!**********************************************************************!*\
  !*** ./node_modules/gl-ionic-background-video/dist/esm/es5/index.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// GlBackgroundVideo: ES Module

/***/ }),

/***/ "./node_modules/gl-ionic-background-video/dist/esm/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/gl-ionic-background-video/dist/esm/index.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _es5_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./es5/index.js */ "./node_modules/gl-ionic-background-video/dist/esm/es5/index.js");
/* harmony import */ var _es5_index_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_es5_index_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _es5_index_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _es5_index_js__WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/postlogin/postlogin.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/postlogin/postlogin.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content force-overscroll=\"false\">\n    <gl-background-video class=\"video\" src=\"assets/video/IMG_1926.mp4\" poster=\"assets/your_video_screen.png\"></gl-background-video>\n    <div class=\"page-centered-element\">\n        <div class=\"content\">\n            <img class=\"logo\" src=\"assets/images/logo.png\">\n            <h3> Congrats! Your Profile was Successfully Created. <br/> Your account will be active once the Vibez Community is able to socialize <br/> ( See the medical communities\n                <ion-label style=\"color:red;\">Response</ion-label> we follow for Covid-19)</h3>\n            <h5>We will launch once we reach 25,000 downloads. <br/> Help us reach our goal by telling your family and friends about us.</h5>\n            <h5>Download Count:- 200</h5>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/postlogin/postlogin-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/postlogin/postlogin-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PostloginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostloginPageRoutingModule", function() { return PostloginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _postlogin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./postlogin.page */ "./src/app/postlogin/postlogin.page.ts");




var routes = [
    {
        path: '',
        component: _postlogin_page__WEBPACK_IMPORTED_MODULE_3__["PostloginPage"]
    }
];
var PostloginPageRoutingModule = /** @class */ (function () {
    function PostloginPageRoutingModule() {
    }
    PostloginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PostloginPageRoutingModule);
    return PostloginPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/postlogin/postlogin.module.ts":
/*!***********************************************!*\
  !*** ./src/app/postlogin/postlogin.module.ts ***!
  \***********************************************/
/*! exports provided: PostloginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostloginPageModule", function() { return PostloginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _postlogin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./postlogin-routing.module */ "./src/app/postlogin/postlogin-routing.module.ts");
/* harmony import */ var _postlogin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./postlogin.page */ "./src/app/postlogin/postlogin.page.ts");







var PostloginPageModule = /** @class */ (function () {
    function PostloginPageModule() {
    }
    PostloginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _postlogin_routing_module__WEBPACK_IMPORTED_MODULE_5__["PostloginPageRoutingModule"]
            ],
            declarations: [_postlogin_page__WEBPACK_IMPORTED_MODULE_6__["PostloginPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], PostloginPageModule);
    return PostloginPageModule;
}());



/***/ }),

/***/ "./src/app/postlogin/postlogin.page.scss":
/*!***********************************************!*\
  !*** ./src/app/postlogin/postlogin.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Bvc3Rsb2dpbi9wb3N0bG9naW4ucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/postlogin/postlogin.page.ts":
/*!*********************************************!*\
  !*** ./src/app/postlogin/postlogin.page.ts ***!
  \*********************************************/
/*! exports provided: PostloginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostloginPage", function() { return PostloginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var gl_ionic_background_video__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! gl-ionic-background-video */ "./node_modules/gl-ionic-background-video/dist/esm/index.js");



var PostloginPage = /** @class */ (function () {
    function PostloginPage() {
    }
    PostloginPage.prototype.ngOnInit = function () {
    };
    PostloginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-postlogin',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./postlogin.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/postlogin/postlogin.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./postlogin.page.scss */ "./src/app/postlogin/postlogin.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PostloginPage);
    return PostloginPage;
}());



/***/ })

}]);
//# sourceMappingURL=postlogin-postlogin-module.js.map