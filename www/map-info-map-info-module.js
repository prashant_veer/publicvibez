(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-info-map-info-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/map-info/map-info.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/map-info/map-info.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-title>Location</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n    <div #mapElement class=\"map\">\n        Maps\n    </div>\n</ion-content>\n<ion-footer style=\"margin-bottom: 16%;\">\n    <ion-label>Radar Range - {{radarrange}} m</ion-label>\n    <ion-item>\n        <ion-range [value]=\"radarrange\" (ionChange)=\"myrange($event)\" min=\"10\" max=\"1000\" step=\"5\" snaps=\"true\" color=\"secondary\">\n            <ion-label slot=\"start\">10</ion-label>\n            <ion-label slot=\"end\">1000</ion-label>\n        </ion-range>\n    </ion-item>\n</ion-footer>");

/***/ }),

/***/ "./src/app/home/map-info/map-info-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/home/map-info/map-info-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: MapInfoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapInfoPageRoutingModule", function() { return MapInfoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map-info.page */ "./src/app/home/map-info/map-info.page.ts");




var routes = [
    {
        path: '',
        component: _map_info_page__WEBPACK_IMPORTED_MODULE_3__["MapInfoPage"]
    }
];
var MapInfoPageRoutingModule = /** @class */ (function () {
    function MapInfoPageRoutingModule() {
    }
    MapInfoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MapInfoPageRoutingModule);
    return MapInfoPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/map-info/map-info.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home/map-info/map-info.module.ts ***!
  \**************************************************/
/*! exports provided: MapInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapInfoPageModule", function() { return MapInfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _map_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./map-info-routing.module */ "./src/app/home/map-info/map-info-routing.module.ts");
/* harmony import */ var _map_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map-info.page */ "./src/app/home/map-info/map-info.page.ts");







var MapInfoPageModule = /** @class */ (function () {
    function MapInfoPageModule() {
    }
    MapInfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _map_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["MapInfoPageRoutingModule"]
            ],
            declarations: [_map_info_page__WEBPACK_IMPORTED_MODULE_6__["MapInfoPage"]]
        })
    ], MapInfoPageModule);
    return MapInfoPageModule;
}());



/***/ }),

/***/ "./src/app/home/map-info/map-info.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home/map-info/map-info.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".map {\n  height: 100%;\n}\n\n.welcome-card img {\n  height: 100%;\n}\n\n.range {\n  position: absolute;\n  right: -30%;\n  bottom: 270px;\n  width: 290px;\n  /* IE 9 */\n  -webkit-transform: rotate(-90deg);\n  /* Chrome, Safari, Opera */\n  transform: rotate(-90deg);\n}\n\n.range i.icon {\n  /* IE 9 */\n  -webkit-transform: rotate(90deg);\n  /* Chrome, Safari, Opera */\n  transform: rotate(90deg);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL2hvbWUvbWFwLWluZm8vbWFwLWluZm8ucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL21hcC1pbmZvL21hcC1pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7QUNDSjs7QURFQTtFQUNJLFlBQUE7QUNDSjs7QURHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBRStCLFNBQUE7RUFDaEMsaUNBQUE7RUFBbUMsMEJBQUE7RUFDbkMseUJBQUE7QUNDSDs7QURBQztFQUNrQyxTQUFBO0VBQ2hDLGdDQUFBO0VBQWtDLDBCQUFBO0VBQ2xDLHdCQUFBO0FDSUgiLCJmaWxlIjoic3JjL2FwcC9ob21lL21hcC1pbmZvL21hcC1pbmZvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXAge1xuICAgIGhlaWdodDogMTAwJTtcbn1cblxuLndlbGNvbWUtY2FyZCBpbWcge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICAvLyBtYXgtaGVpZ2h0OiAzNXZoO1xuICAgIC8vIG92ZXJmbG93OiBoaWRkZW47XG59XG4ucmFuZ2V7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAtMzAlO1xuICAgIGJvdHRvbTogMjcwcHg7XG4gICAgd2lkdGg6IDI5MHB4O1xuXG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7IC8qIElFIDkgKi9cbiAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTsgLyogQ2hyb21lLCBTYWZhcmksIE9wZXJhICovXG4gICB0cmFuc2Zvcm06IHJvdGF0ZSgtOTBkZWcpO1xuIGkuaWNvbntcbiAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTsgLyogSUUgOSAqL1xuICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7IC8qIENocm9tZSwgU2FmYXJpLCBPcGVyYSAqL1xuICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuIH1cbn0iLCIubWFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ud2VsY29tZS1jYXJkIGltZyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnJhbmdlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogLTMwJTtcbiAgYm90dG9tOiAyNzBweDtcbiAgd2lkdGg6IDI5MHB4O1xuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTtcbiAgLyogSUUgOSAqL1xuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7XG4gIC8qIENocm9tZSwgU2FmYXJpLCBPcGVyYSAqL1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgtOTBkZWcpO1xufVxuLnJhbmdlIGkuaWNvbiB7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gIC8qIElFIDkgKi9cbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gIC8qIENocm9tZSwgU2FmYXJpLCBPcGVyYSAqL1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/map-info/map-info.page.ts":
/*!************************************************!*\
  !*** ./src/app/home/map-info/map-info.page.ts ***!
  \************************************************/
/*! exports provided: MapInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapInfoPage", function() { return MapInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var MapInfoPage = /** @class */ (function () {
    function MapInfoPage(router, gen, userService, geolocation, nativeGeocoder) {
        this.router = router;
        this.gen = gen;
        this.userService = userService;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.radarrange = 30;
        this.useList = [];
        this.markers = [];
    }
    MapInfoPage.prototype.ngOnInit = function () {
        var obj = this;
        obj.geolocation.getCurrentPosition().then(function (resp) {
            obj.latitude = resp.coords.latitude;
            obj.longitude = resp.coords.longitude;
            //  this.latitude =28.5821195;
            //  this.longitude = 77.3266991;
            obj.gen.presentLoading("Find user location..");
            obj.loadUserList(obj.radarrange);
        }).catch(function (error) {
            obj.latitude = obj.gen.getUserInfo().Latitude;
            obj.longitude = obj.gen.getUserInfo().Longitude;
            obj.gen.presentLoading("Find user location..");
            // obj.loadUserList(obj.radarrange);
        });
    };
    MapInfoPage.prototype.ngAfterViewInit = function () {
        var obj = this;
        var latlng = new google.maps.LatLng(28.7041, 77.1025);
        obj.map = new google.maps.Map(this.mapNativeElement.nativeElement, {
            zoom: 12,
            center: latlng,
            mapTypeId: 'terrain'
        });
    };
    MapInfoPage.prototype.loadUserList = function (range) {
        var _this = this;
        var obj = this;
        var latlng = new google.maps.LatLng(obj.latitude, obj.longitude);
        obj.map.setCenter(latlng);
        obj.userService.userList(range, obj.latitude, obj.longitude).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v, i;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                console.log(v);
                obj.gen.closePresentLoading();
                obj.deleteMarkers();
                if (v.success) {
                    obj.useList = v.data;
                    for (i = 0; i < obj.useList.length; i++) {
                        obj.addMarker(obj.useList[i]['Latitude'], obj.useList[i]['Longitude'], obj.useList[i]['zipcode']);
                    }
                }
                return [2 /*return*/];
            });
        }); });
    };
    MapInfoPage.prototype.addMarker = function (Longitude, Latitude, zipcode) {
        var obj = this;
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(Longitude, Latitude),
            map: obj.map,
            title: zipcode
        });
        obj.markers.push(marker);
        marker.addListener('click', function (mark, j) {
            console.log(marker);
            console.log(j);
            obj.getZipUserList(marker.title);
        });
    };
    MapInfoPage.prototype.deleteMarkers = function () {
        this.setMapOnAll(null);
        this.markers = [];
    };
    // Sets the map on all markers in the array.
    MapInfoPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    MapInfoPage.prototype.myrange = function (event) {
        console.log(event);
        this.radarrange = event.detail.value;
        console.log('range=' + event.detail.value);
        this.loadUserList(this.radarrange);
    };
    MapInfoPage.prototype.getZipUserList = function (zipCode) {
        this.router.navigate(['home/map-component', {
                mapZipIcon: zipCode,
            }]);
    };
    MapInfoPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"] },
        { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_3__["NativeGeocoder"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('mapElement', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], MapInfoPage.prototype, "mapNativeElement", void 0);
    MapInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-map-info',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./map-info.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/map-info/map-info.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./map-info.page.scss */ "./src/app/home/map-info/map-info.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"],
            _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_3__["NativeGeocoder"]])
    ], MapInfoPage);
    return MapInfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=map-info-map-info-module.js.map