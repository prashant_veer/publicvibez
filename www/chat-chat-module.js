(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar color=\"secondary\">\n        <ion-buttons slot=\"start\">\n            <ion-back-button defaultHref=\"home/tabs/chat\"></ion-back-button>\n        </ion-buttons>\n        <ion-title *ngIf=\"reqDetail\">{{reqDetail.name}}</ion-title>\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"sendInvitation()\">\n                <ion-icon name=\"add-circle-outline\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content fullscreen=\"true\">\n    <ion-list>\n        <div *ngFor=\"let chat of chats; let i = index;\">\n            <div class=\"coment-screen\">\n                <ion-card style=\"margin:0px;width:100%;padding:0px;\">\n                    <ion-card-content *ngIf=\"chat.user=='Requester' \">\n                        <span style=\"font-size: 15px;\">{{chat.sendDate | date:'d/M/y'}}</span> {{chat.message}}\n                    </ion-card-content>\n                    <ion-card-content style=\"float: right;\" *ngIf=\"chat.user!='Requester' \">\n                        <span style=\"font-size: 15px;\">{{chat.sendDate | date:'d/M/y'}}</span> {{chat.message}}\n                    </ion-card-content>\n                </ion-card>\n            </div>\n        </div>\n    </ion-list>\n</ion-content>\n<ion-footer translucent>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <ion-icon style=\"height: 50px;width: 10%;float: right;background:#981f40 ;color: #fff;\" (click)=\"sendMessage()\" name=\"paper-plane\"></ion-icon>\n\n                <ion-input type=\"text\" style=\"width: 90%;\" placeholder=\"Type a message\" [(ngModel)]=\"chat.message\" name=\"message\"></ion-input>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/photo-verification/photo-verification.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/photo-verification/photo-verification.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-title>Photo Verification</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n   <div>VERIFICATION</div>\n   <div>\n     A live and phone number required to proceed.\n   </div>\n   <ion-list>\n    <ion-item *ngFor=\"let img of images; index as pos\" text-wrap>\n        <ion-thumbnail  style=\"width: 200px;height: 200px;margin-left: 15%;\">\n            <ion-img [src]=\"img.path\" ></ion-img>\n        </ion-thumbnail>\n        <ion-button slot=\"end\" fill=\"clear\" (click)=\"deleteImage(img, pos)\">\n            <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\n        </ion-button>\n    </ion-item>\n  </ion-list>\n  <div class=\"formElement\">\n    <ion-button color=\"secondary\" *ngIf=\"images.length==0\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"selectImage()\">Upload</ion-button>\n    <ion-button color=\"secondary\" *ngIf=\"images.length==1\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"verifyNow()\">Verify now</ion-button>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/verification/verification.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/verification/verification.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button (click)=\"onclickClose()\" ></ion-back-button>\n    </ion-buttons>\n    <ion-title>Verification</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <img src=\"{{url}}thumbnail/{{userInfo.photoPath}}_thumb.jpg\" />\n      </ion-col>\n      <ion-col>\n        <img src=\"{{url}}thumbnail/{{userInfo.photo_name}}_thumb.jpg\" />\n      </ion-col>\n    </ion-row>\n  </ion-grid>  \n  <div  style=\"margin: 0 20px;\" >\n    <ion-button (click)=\"approveChat()\" style=\"width: 50%;\">\n        Approver\n    </ion-button>\n    <ion-button (click)=\"rejectChat()\" style=\"width: 50%;\">\n        Reject\n    </ion-button>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/chat/chat-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/chat/chat-routing.module.ts ***!
  \*********************************************/
/*! exports provided: ChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function() { return ChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");




var routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }
];
var ChatPageRoutingModule = /** @class */ (function () {
    function ChatPageRoutingModule() {
    }
    ChatPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ChatPageRoutingModule);
    return ChatPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/chat/chat.module.ts":
/*!*************************************!*\
  !*** ./src/app/chat/chat.module.ts ***!
  \*************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-routing.module */ "./src/app/chat/chat-routing.module.ts");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");
/* harmony import */ var _photo_verification_photo_verification_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./photo-verification/photo-verification.page */ "./src/app/chat/photo-verification/photo-verification.page.ts");
/* harmony import */ var _verification_verification_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./verification/verification.page */ "./src/app/chat/verification/verification.page.ts");









var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"]
            ],
            declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"], _photo_verification_photo_verification_page__WEBPACK_IMPORTED_MODULE_7__["PhotoVerificationPage"], _verification_verification_page__WEBPACK_IMPORTED_MODULE_8__["VerificationPage"]],
            entryComponents: [_photo_verification_photo_verification_page__WEBPACK_IMPORTED_MODULE_7__["PhotoVerificationPage"], _verification_verification_page__WEBPACK_IMPORTED_MODULE_8__["VerificationPage"]]
        })
    ], ChatPageModule);
    return ChatPageModule;
}());



/***/ }),

/***/ "./src/app/chat/chat.page.scss":
/*!*************************************!*\
  !*** ./src/app/chat/chat.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXQvY2hhdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/chat/chat.page.ts":
/*!***********************************!*\
  !*** ./src/app/chat/chat.page.ts ***!
  \***********************************/
/*! exports provided: snapshotToArray, ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "snapshotToArray", function() { return snapshotToArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _photo_verification_photo_verification_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./photo-verification/photo-verification.page */ "./src/app/chat/photo-verification/photo-verification.page.ts");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _verification_verification_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./verification/verification.page */ "./src/app/chat/verification/verification.page.ts");









var snapshotToArray = function (snapshot) {
    var returnArr = [];
    snapshot.forEach(function (childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });
    return returnArr;
};
var ChatPage = /** @class */ (function () {
    function ChatPage(alertController, general, modalCont, _userser, activatedroute) {
        var _this = this;
        this.alertController = alertController;
        this.general = general;
        this.modalCont = modalCont;
        this._userser = _userser;
        this.activatedroute = activatedroute;
        this.offStatus = false;
        this.pageType = "0";
        this.chat = { type: '', nickname: '', message: '' };
        this.chats = [];
        this.ref = firebase.database().ref('chatrooms/');
        this.reqDetail = JSON.parse(this.activatedroute.snapshot.paramMap.get('userProfile'));
        this.pageType = this.activatedroute.snapshot.paramMap.get('pageType');
        this.roomkey = this.reqDetail.chat_guid;
        this.nickname = this.reqDetail.name;
        this.chat.type = 'message';
        this.chat.nickname = this.nickname;
        this.chat.message = '';
        firebase.database().ref('chatrooms/' + this.roomkey + '/chats').on('value', function (resp) {
            _this.chats = [];
            _this.chats = snapshotToArray(resp);
            console.log(_this.chats);
            setTimeout(function () {
                if (_this.offStatus === false) {
                    _this.content.scrollToBottom(300);
                }
            }, 1000);
        });
    }
    ChatPage.prototype.ngOnInit = function () {
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        var obj = this;
        obj.setInterval();
    };
    ChatPage.prototype.ionViewWillLeave = function () {
        var obj = this;
        clearInterval(obj.chatInter);
    };
    ChatPage.prototype.setInterval = function () {
        var obj = this;
        obj.chatInter = setInterval(function () {
            var _this = this;
            console.log("setInterval");
            obj._userser.checkPhoto(obj.reqDetail).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    console.log(req);
                    if (req.data) {
                        v = JSON.parse(req.data);
                        if (v.success) {
                            if (v.data.status == 0 && v.data.user_id == obj.general.getUserInfo().user_id) {
                                obj.presentModal(_photo_verification_photo_verification_page__WEBPACK_IMPORTED_MODULE_5__["PhotoVerificationPage"], v.data);
                            }
                            else if (v.data.status == 1 && v.data.user_id != obj.general.getUserInfo().user_id) {
                                obj.presentModal(_verification_verification_page__WEBPACK_IMPORTED_MODULE_7__["VerificationPage"], v.data);
                            }
                            clearInterval(obj.chatInter);
                        }
                    }
                    return [2 /*return*/];
                });
            }); });
        }, 10000);
    };
    ChatPage.prototype.sendInvitation = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var obj, alert_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        obj = this;
                        if (!(obj.general.getUserInfo().isPad == 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Confirm!',
                                message: 'Photo varification cost: 0.99$',
                                buttons: [
                                    {
                                        text: 'Cancel',
                                        role: 'cancel',
                                        cssClass: 'secondary'
                                    }, {
                                        text: 'Okay',
                                        handler: function () {
                                            obj.sendAPIrequest();
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        obj.sendAPIrequest();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChatPage.prototype.sendAPIrequest = function () {
        var _this = this;
        var obj = this;
        obj._userser.sendPhotoVarifi(obj.reqDetail).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log(req);
                if (req.data) {
                    v = JSON.parse(req.data);
                    if (v.success) {
                        obj.general.presentToast('Send photo verification invitation!');
                    }
                }
                return [2 /*return*/];
            });
        }); });
    };
    ChatPage.prototype.presentModal = function (com, request) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var obj, modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        obj = this;
                        return [4 /*yield*/, this.modalCont.create({
                                component: com,
                                componentProps: {
                                    userInfo: request
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () { obj.setInterval(); });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ChatPage.prototype.sendMessage = function () {
        var newData = firebase
            .database()
            .ref('chatrooms/' + this.roomkey + '/chats')
            .push();
        newData.set({
            // type: type,
            type: this.chat.type,
            user: this.chat.nickname,
            message: this.chat.message,
            // user: this.data.nickname,
            // message: this.data.message,
            sendDate: Date()
        });
        // this.data.message = '';
        this.chat.message = '';
        this.scrollBottom();
    };
    ChatPage.prototype.scrollBottom = function () {
        var that = this;
        setTimeout(function () {
            that.content.scrollToBottom();
        }, 300);
    };
    ChatPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
    ], ChatPage.prototype, "content", void 0);
    ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat.page.scss */ "./src/app/chat/chat.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ChatPage);
    return ChatPage;
}());



/***/ }),

/***/ "./src/app/chat/photo-verification/photo-verification.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/chat/photo-verification/photo-verification.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXQvcGhvdG8tdmVyaWZpY2F0aW9uL3Bob3RvLXZlcmlmaWNhdGlvbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/chat/photo-verification/photo-verification.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/chat/photo-verification/photo-verification.page.ts ***!
  \********************************************************************/
/*! exports provided: PhotoVerificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoVerificationPage", function() { return PhotoVerificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");











var PhotoVerificationPage = /** @class */ (function () {
    function PhotoVerificationPage(webview, platform, camera, actionSheetController, toastCtrl, authSvc, _userser, file, filePath, gen, navParams, modalCtrl) {
        this.webview = webview;
        this.platform = platform;
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this._userser = _userser;
        this.file = file;
        this.filePath = filePath;
        this.gen = gen;
        this.modalCtrl = modalCtrl;
        this.images = [];
        this.userInfo = navParams.get('userInfo');
    }
    PhotoVerificationPage.prototype.onclickClose = function () {
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    };
    PhotoVerificationPage.prototype.ngOnInit = function () {
    };
    PhotoVerificationPage.prototype.deleteImage = function (imgEntry, position) {
        this.images = [];
    };
    PhotoVerificationPage.prototype.verifyNow = function () {
        var _this = this;
        this.gen.presentLoading('Uploading the image');
        var imgEntry = this.images[0];
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            _this.gen.presentToast('Error while reading file.');
        });
    };
    PhotoVerificationPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    PhotoVerificationPage.prototype.uploadImageData = function (formData) {
        var obj = this;
        obj.authSvc.registerImage(formData).then(function (req) {
            console.log(req);
            obj.gen.closePresentLoading();
            var v = JSON.parse(req.data);
            if (v.success) {
                obj._userser.addPhotos(obj.userInfo, v.data).then(function (req) {
                    obj.onclickClose();
                });
            }
            else {
                obj.images = [];
                obj.errorToast(v.message);
            }
            obj.gen.closePresentLoading();
        });
    };
    PhotoVerificationPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    PhotoVerificationPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Select Photo From',
                            buttons: [{
                                    text: 'Load from Library',
                                    role: 'destructive',
                                    icon: 'images',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Use Camera',
                                    role: 'destructive',
                                    icon: 'camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PhotoVerificationPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        });
    };
    PhotoVerificationPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + '.jpg';
        return newFileName;
    };
    PhotoVerificationPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.updateStoredImages(newFileName);
        }, function (error) {
            _this.gen.presentToast('Error while storing file.');
        });
    };
    PhotoVerificationPage.prototype.updateStoredImages = function (name) {
        var filePath = this.file.dataDirectory + name;
        var resPath = this.pathForImage(filePath);
        var newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
        this.images = [newEntry].concat(this.images);
    };
    PhotoVerificationPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    PhotoVerificationPage.ctorParameters = function () { return [
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_5__["FilePath"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    PhotoVerificationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-photo-verification',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./photo-verification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/photo-verification/photo-verification.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./photo-verification.page.scss */ "./src/app/chat/photo-verification/photo-verification.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_9__["Camera"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"], _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_5__["FilePath"], _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], PhotoVerificationPage);
    return PhotoVerificationPage;
}());



/***/ }),

/***/ "./src/app/chat/verification/verification.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/chat/verification/verification.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXQvdmVyaWZpY2F0aW9uL3ZlcmlmaWNhdGlvbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/chat/verification/verification.page.ts":
/*!********************************************************!*\
  !*** ./src/app/chat/verification/verification.page.ts ***!
  \********************************************************/
/*! exports provided: VerificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificationPage", function() { return VerificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");






var VerificationPage = /** @class */ (function () {
    function VerificationPage(navParams, _userser, modalCtrl) {
        this._userser = _userser;
        this.modalCtrl = modalCtrl;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].root_URL;
        this.userInfo = navParams.get('userInfo');
    }
    VerificationPage.prototype.onclickClose = function () {
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    };
    VerificationPage.prototype.ngOnInit = function () {
    };
    VerificationPage.prototype.approveChat = function () {
        var obj = this;
        obj._userser.setPhotoStatus(obj.userInfo.photo_verify_id, 2).then(function (req) { obj.onclickClose(); });
    };
    VerificationPage.prototype.rejectChat = function () {
        var obj = this;
        obj._userser.setPhotoStatus(obj.userInfo.photo_verify_id, 3).then(function (req) { obj.onclickClose(); });
    };
    VerificationPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    VerificationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-verification',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./verification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/verification/verification.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./verification.page.scss */ "./src/app/chat/verification/verification.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], VerificationPage);
    return VerificationPage;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");





var LoginService = /** @class */ (function () {
    function LoginService(http, file) {
        this.http = http;
        this.file = file;
        this.http.setDataSerializer('urlencoded');
    }
    LoginService.prototype.sendotptoEmail = function (userEmail, userMobile, userNickName) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "userEmail": userEmail,
            "userMobile": userMobile,
            "userNickName": userNickName
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.sendOtptoEmail(), formData, {});
    };
    LoginService.prototype.getuserCount = function (userEmail) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "userEmail": userEmail,
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.user.count(), formData, {});
    };
    LoginService.prototype.login = function (mobile, password, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "username": mobile, "password": password, "token": token };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.login(), formData, {});
    };
    LoginService.prototype.register = function (NickName, email, userMobile, Pass, usercity, userArea, userPincode, userState, username, userBirthday, userPhoto, preferredGender, userbodytype, userheight, userchild, usersmoke, userdrinks, userzodiac, userRace, userPreference, userAboutme, latitude, longitude, userweight) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "nickName": NickName, "userEmail": email,
            "userMobile": userMobile, "pass": Pass,
            "username": username, "userBirthday": userBirthday, "profileimg": userPhoto,
            "cityName": usercity, "addressName": userArea, "userPincode": userPincode, "userState": userState,
            "preferredGender": preferredGender, "userbodytype": userbodytype, "userheight": userheight, "userchild": userchild,
            "usersmoke": usersmoke, "userdrinks": userdrinks, "userzodiac": userzodiac, "userRace": userRace, "userPreference": userPreference, "userAboutme": userAboutme,
            "latitude": latitude, "longitude": longitude, "userweight": userweight
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.registerLong(), formData, {});
    };
    LoginService.prototype.getPhotoList = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.photoList(), formData, {});
    };
    LoginService.prototype.updateProfile = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
            "name": userInfo.name,
            "gender": userInfo.gender,
            "birthdate": userInfo.birthdate, "email_address": userInfo.email_address,
            "city": userInfo.city, "zipcode": userInfo.zipcode,
            "body_type": userInfo.body_type, "height": userInfo.height,
            "education": userInfo.education, "no_of_children": userInfo.no_of_children,
            "smoke": userInfo.smoke, "Interested": userInfo.Interested
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfile(), formData, {});
    };
    LoginService.prototype.registerImage = function (usersmoke) {
        this.http.setDataSerializer('multipart');
        this.http.setRequestTimeout(7000);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfileImg(), usersmoke, {});
    };
    LoginService.prototype.googleLogin = function (userEmail, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "googleID": "", "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.googleLogin(), formData, {});
    };
    LoginService.prototype.facebookLogin = function (userEmail, facebookId, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "facebookId": facebookId, "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.facebookLogin(), formData, {});
    };
    LoginService.ctorParameters = function () { return [
        { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"]])
    ], LoginService);
    return LoginService;
}());



/***/ })

}]);
//# sourceMappingURL=chat-chat-module.js.map