(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["membership-membership-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/membership/membership.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/membership/membership.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-title> Membership Setting</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"main\">\n      <h1>Select Your Plan</h1>\n      <div class=\"discountCard oneMonth\">\n          <div class=\"discountPer\">upto<br>30%</div>\n          <h4>Membership for 1 month plan</h4>\n          <h4>$10</h4>\n          <button>Try Now</button>\n      </div>\n      <div class=\"discountCard sixMonth\">\n          <div class=\"discountPer\">upto<br>30%</div>\n          <h4>Membership for 1 month plan</h4>\n          <h4>$10</h4>\n          <button>Try Now</button>\n      </div>\n      <div class=\"discountCard oneYear\">\n          <div class=\"discountPer\">upto<br>30%</div>\n          <h4>Membership for 1 month plan</h4>\n          <h4>$10</h4>\n          <button>Try Now</button>\n      </div>\n  </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/membership/membership-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/home/membership/membership-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: MembershipPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembershipPageRoutingModule", function() { return MembershipPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _membership_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./membership.page */ "./src/app/home/membership/membership.page.ts");




var routes = [
    {
        path: '',
        component: _membership_page__WEBPACK_IMPORTED_MODULE_3__["MembershipPage"]
    }
];
var MembershipPageRoutingModule = /** @class */ (function () {
    function MembershipPageRoutingModule() {
    }
    MembershipPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MembershipPageRoutingModule);
    return MembershipPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/membership/membership.module.ts":
/*!******************************************************!*\
  !*** ./src/app/home/membership/membership.module.ts ***!
  \******************************************************/
/*! exports provided: MembershipPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembershipPageModule", function() { return MembershipPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _membership_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./membership-routing.module */ "./src/app/home/membership/membership-routing.module.ts");
/* harmony import */ var _membership_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./membership.page */ "./src/app/home/membership/membership.page.ts");







var MembershipPageModule = /** @class */ (function () {
    function MembershipPageModule() {
    }
    MembershipPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _membership_routing_module__WEBPACK_IMPORTED_MODULE_5__["MembershipPageRoutingModule"]
            ],
            declarations: [_membership_page__WEBPACK_IMPORTED_MODULE_6__["MembershipPage"]]
        })
    ], MembershipPageModule);
    return MembershipPageModule;
}());



/***/ }),

/***/ "./src/app/home/membership/membership.page.scss":
/*!******************************************************!*\
  !*** ./src/app/home/membership/membership.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvbWVtYmVyc2hpcC9tZW1iZXJzaGlwLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/home/membership/membership.page.ts":
/*!****************************************************!*\
  !*** ./src/app/home/membership/membership.page.ts ***!
  \****************************************************/
/*! exports provided: MembershipPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MembershipPage", function() { return MembershipPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MembershipPage = /** @class */ (function () {
    function MembershipPage() {
    }
    MembershipPage.prototype.ngOnInit = function () {
    };
    MembershipPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-membership',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./membership.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/membership/membership.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./membership.page.scss */ "./src/app/home/membership/membership.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MembershipPage);
    return MembershipPage;
}());



/***/ })

}]);
//# sourceMappingURL=membership-membership-module.js.map