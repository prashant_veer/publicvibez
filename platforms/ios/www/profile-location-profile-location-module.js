(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-location-profile-location-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/profile-location/profile-location.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/profile-location/profile-location.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n\n    <a href=\"\" class=\"round-button-create-profile gradient-background\" style=\"margin-top: 20%;\">\n        <ion-icon name=\"person-outline\" style=\"font-size: 50px;margin-top: 10%;\"></ion-icon>\n    </a>\n    <br>\n    <h2><span>CREATE <b>PROFILE</b></span></h2>\n    <!-- <p>Address - {{geoAddress}}</p> -->\n    <div class=\"form-div\">\n        <div class=\"formElement\">\n            <ion-input type=\"text\" [(ngModel)]=\"userArea\" class=\"general-input ion-input-class\" placeholder=\"Area Name\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"text\" [(ngModel)]=\"usercity\" class=\"general-input ion-input-class\" placeholder=\"Area city\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"email\" [(ngModel)]=\"userPincode\" class=\"general-input ion-input-class\" placeholder=\"Area Pincode\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"email\" [(ngModel)]=\"userState\" class=\"general-input ion-input-class\" placeholder=\"State\"></ion-input>\n        </div>\n\n        <div class=\"formElement\">\n\n\n\n        </div>\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_getstart()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/profile-location/profile-location-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/signup/profile-location/profile-location-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ProfileLocationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLocationPageRoutingModule", function() { return ProfileLocationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_location_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile-location.page */ "./src/app/signup/profile-location/profile-location.page.ts");




var routes = [
    {
        path: '',
        component: _profile_location_page__WEBPACK_IMPORTED_MODULE_3__["ProfileLocationPage"]
    }
];
var ProfileLocationPageRoutingModule = /** @class */ (function () {
    function ProfileLocationPageRoutingModule() {
    }
    ProfileLocationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ProfileLocationPageRoutingModule);
    return ProfileLocationPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/profile-location/profile-location.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/profile-location/profile-location.module.ts ***!
  \********************************************************************/
/*! exports provided: ProfileLocationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLocationPageModule", function() { return ProfileLocationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _profile_location_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-location-routing.module */ "./src/app/signup/profile-location/profile-location-routing.module.ts");
/* harmony import */ var _profile_location_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile-location.page */ "./src/app/signup/profile-location/profile-location.page.ts");







var ProfileLocationPageModule = /** @class */ (function () {
    function ProfileLocationPageModule() {
    }
    ProfileLocationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _profile_location_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfileLocationPageRoutingModule"]
            ],
            declarations: [_profile_location_page__WEBPACK_IMPORTED_MODULE_6__["ProfileLocationPage"]]
        })
    ], ProfileLocationPageModule);
    return ProfileLocationPageModule;
}());



/***/ }),

/***/ "./src/app/signup/profile-location/profile-location.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/signup/profile-location/profile-location.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9wcm9maWxlLWxvY2F0aW9uL3Byb2ZpbGUtbG9jYXRpb24ucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/signup/profile-location/profile-location.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/profile-location/profile-location.page.ts ***!
  \******************************************************************/
/*! exports provided: ProfileLocationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLocationPage", function() { return ProfileLocationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");







var ProfileLocationPage = /** @class */ (function () {
    function ProfileLocationPage(nav, gen, activatedroute, geolocation, nativeGeocoder) {
        this.nav = nav;
        this.gen = gen;
        this.activatedroute = activatedroute;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.pincodepattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
        this.statepattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
    }
    ProfileLocationPage.prototype.ngOnInit = function () {
        var _this = this;
        this.userMobile = window.localStorage.getItem('Mobile');
        this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
        this.userNickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.userPass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        var options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
            .then(function (result) {
            console.log('address ' + JSON.stringify(result[0]));
            _this.geoAddress = _this.generateAddress(result[0]);
        })
            .catch(function (error) { return console.log(error); });
    };
    // Return Comma saperated address
    ProfileLocationPage.prototype.generateAddress = function (addressObj) {
        var obj = [];
        var address = '';
        // tslint:disable-next-line:forin
        for (var key in addressObj) {
            obj.push(addressObj[key]);
        }
        obj.reverse();
        for (var val in obj) {
            if (obj[val].length) {
                address += obj[val] + ', ';
            }
        }
        return address.slice(0, -2);
    };
    ProfileLocationPage.prototype.goto_getstart = function () {
        this.nav.navigateForward(['signup/gettingstart', {
                email: this.userEmail,
                NickName: this.userNickName,
                mobile: this.userMobile,
                Pass: this.userPass,
                area: this.userArea,
                pincode: this.userPincode,
                state: this.userState,
                usercity: this.usercity,
                latitude: this.latitude,
                longitude: this.longitude
            }]);
    };
    ProfileLocationPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
        { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_6__["NativeGeocoder"] }
    ]; };
    ProfileLocationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-location',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profile-location.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/profile-location/profile-location.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profile-location.page.scss */ "./src/app/signup/profile-location/profile-location.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
            _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_6__["NativeGeocoder"]])
    ], ProfileLocationPage);
    return ProfileLocationPage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-location-profile-location-module.js.map