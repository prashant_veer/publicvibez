(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registersummary-registersummary-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n    <div class=\"other\">\n        <div class=\"mainDiv uploadInfo\">\n            <div class=\"tableCell\">\n                <h2><span>Upload <b>Information</b></span></h2>\n                <h2><span>Profile <b>Picture</b></span></h2>\n                <a href=\"JavaScript:void(0)\" class=\"round-button-create-profile Photo-background\" style=\"margin-top: 20%;\">\n                    <ion-img *ngIf=\"userPhoto\" [src]=\"userPhoto.path\" style=\"height:100%;width:100%;\"></ion-img>\n                    <ion-icon *ngIf=\"!userPhoto\" name=\"add-outline\" style=\"font-size: 50px;margin-top: 17%;color: darkgrey;\"></ion-icon>\n                </a>\n                <div class=\"form-div\">\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Nickname</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"text\" [(ngModel)]=\"NickName\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">email</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"email\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">username</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"username\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Birthday</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userBirthday\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Mobile</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userMobile\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Area</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userArea\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Pincode</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userPincode\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">State</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"userState\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">City</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <input type=\"email\" [(ngModel)]=\"usercity\" class=\"myselect\">\n\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Gender</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"preferredGender\" class=\"myselect\" [selectedText]=\"genderSelectedtext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">BodyType</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userbodytype\" class=\"myselect\" [selectedText]=\"bodytypetext\">\n\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Height</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userheight\" class=\"myselect\" [selectedText]=\"userheighttext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Child</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userchild\" class=\"myselect\" [selectedText]=\"userchildtext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Smoke</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"usersmoke\" class=\"myselect\" [selectedText]=\"usersmoketext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Drinks</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userdrinks\" class=\"myselect\" [selectedText]=\"userdrinkstext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Zodiac</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userzodiac\" class=\"myselect\" [selectedText]=\"zodiactext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Race</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userRace\" placeholder=\"Select Race\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Prefecen</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userPreference\" placeholder=\"Select Prefecen\" class=\"myselect\">\n\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"ion-textarea-Box\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"12\">\n                                        <ion-textarea [(ngModel)]=\"userAboutme\" placeholder=\"About Me\" class=\"box-label label-padding\"></ion-textarea>\n                                    </ion-col>\n\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <ion-grid>\n                        <ion-row class=\"ion-text-center\">\n                            <ion-col>\n                                <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_registerfinish()\">\n                                    <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                                </a>\n\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/registersummary/registersummary-routing.module.ts":
/*!**************************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: RegistersummaryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPageRoutingModule", function() { return RegistersummaryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registersummary_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registersummary.page */ "./src/app/signup/registersummary/registersummary.page.ts");




var routes = [
    {
        path: '',
        component: _registersummary_page__WEBPACK_IMPORTED_MODULE_3__["RegistersummaryPage"]
    }
];
var RegistersummaryPageRoutingModule = /** @class */ (function () {
    function RegistersummaryPageRoutingModule() {
    }
    RegistersummaryPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegistersummaryPageRoutingModule);
    return RegistersummaryPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.module.ts ***!
  \******************************************************************/
/*! exports provided: RegistersummaryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPageModule", function() { return RegistersummaryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registersummary_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registersummary-routing.module */ "./src/app/signup/registersummary/registersummary-routing.module.ts");
/* harmony import */ var _registersummary_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registersummary.page */ "./src/app/signup/registersummary/registersummary.page.ts");







var RegistersummaryPageModule = /** @class */ (function () {
    function RegistersummaryPageModule() {
    }
    RegistersummaryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _registersummary_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistersummaryPageRoutingModule"]
            ],
            declarations: [_registersummary_page__WEBPACK_IMPORTED_MODULE_6__["RegistersummaryPage"]]
        })
    ], RegistersummaryPageModule);
    return RegistersummaryPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.page.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.box-left {\n  background: #37A4DC;\n  padding-right: -10px;\n}\n\n.box-label {\n  background: #CCD1D1;\n  font-size: small;\n}\n\n.label-padding {\n  padding-left: 15px;\n}\n\n.myselect-label {\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyc3VtbWFyeS9yZWdpc3RlcnN1bW1hcnkucGFnZS5zY3NzIiwic3JjL2FwcC9zaWdudXAvcmVnaXN0ZXJzdW1tYXJ5L3JlZ2lzdGVyc3VtbWFyeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0Esb0JBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFLSSxnQkFBQTtBQ0hKIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyc3VtbWFyeS9yZWdpc3RlcnN1bW1hcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15c2VsZWN0IHtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3gtbGVmdCB7XG4gICAgYmFja2dyb3VuZDogIzM3QTREQztcbiAgICBwYWRkaW5nLXJpZ2h0OiAtMTBweDtcbn1cblxuLmJveC1sYWJlbCB7XG4gICAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgICBmb250LXNpemU6IHNtYWxsO1xufVxuXG4ubGFiZWwtcGFkZGluZyB7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4ubXlzZWxlY3QtbGFiZWwge1xuICAgIC8vICB3aWR0aDogMTAwJTtcbiAgICAvLyAgaGVpZ2h0OiA0MHB4O1xuICAgIC8vICBwYWRkaW5nOiAxOHB4IDE3cHggMThweCAzMHB4O1xuICAgIC8vICBiYWNrZ3JvdW5kOiAjQ0NEMUQxO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG59IiwiLm15c2VsZWN0IHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBvdXRsaW5lOiBub25lO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBwYWRkaW5nOiAwIDE1cHggMCAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3gtbGVmdCB7XG4gIGJhY2tncm91bmQ6ICMzN0E0REM7XG4gIHBhZGRpbmctcmlnaHQ6IC0xMHB4O1xufVxuXG4uYm94LWxhYmVsIHtcbiAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmxhYmVsLXBhZGRpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbi5teXNlbGVjdC1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/registersummary/registersummary.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/signup/registersummary/registersummary.page.ts ***!
  \****************************************************************/
/*! exports provided: RegistersummaryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistersummaryPage", function() { return RegistersummaryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var RegistersummaryPage = /** @class */ (function () {
    function RegistersummaryPage(general, toastCtrl, router, activatedroute, authSvc) {
        this.general = general;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.activatedroute = activatedroute;
        this.authSvc = authSvc;
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userchildarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.genderprefer = [];
        this.userDrinksarray = [];
    }
    RegistersummaryPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.username = this.activatedroute.snapshot.paramMap.get('username');
        this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
        this.bodytypetext = this.activatedroute.snapshot.paramMap.get('userbodytype');
        this.userheighttext = this.activatedroute.snapshot.paramMap.get('userheight');
        this.userchildtext = this.activatedroute.snapshot.paramMap.get('userchild');
        this.usereducationtext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.usersmoketext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.genderSelectedtext = this.activatedroute.snapshot.paramMap.get('preferredGender');
        this.userdrinkstext = this.activatedroute.snapshot.paramMap.get('usersmoke');
        this.zodiactext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.petpeevestext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.racetext = this.activatedroute.snapshot.paramMap.get('userRace');
        this.preferencetext = this.activatedroute.snapshot.paramMap.get('userPreference');
    };
    RegistersummaryPage.prototype.goto_registerfinish = function () {
        var _this = this;
        if (this.bodytypetext === 'What\'s your body type?') {
            this.general.presentToast('Please select your body type.');
        }
        else if (this.userheighttext === 'What\'s your height?') {
            this.general.presentToast('Please select height.');
        }
        else if (this.userchildtext === 'Do you have children?') {
            this.general.presentToast('Please select details about children.');
        }
        else if (this.usersmoketext === 'do you smoke?') {
            this.general.presentToast('Please select details about smoking.');
        }
        else {
            this.general.presentLoading("Please wait... ");
            this.authSvc.register(this.NickName, this.email, this.userMobile, this.Pass, this.usercity, this.userArea, this.userPincode, this.userState, this.username, this.userBirthday, this.userPhoto, this.preferredGender, this.userbodytype, this.userheight, this.userchild, this.usersmoke, this.userdrinks, this.userzodiac, this.userRace, this.userPreference, this.userAboutme, this.latitude, this.longitude).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.general.closePresentLoading();
                    console.log(req.data);
                    v = JSON.parse(req.data);
                    console.log(v);
                    if (v.success) {
                        this.router.navigate(['signup/registerfinished', {
                                "NickName": this.NickName, "email": this.email, "userMobile": this.userMobile, "Pass": this.Pass,
                                "usercity": this.usercity, "userArea": this.userArea, "userPincode": this.userPincode, "userState": this.userState,
                                "username": this.username, "userBirthday": this.userBirthday, "userPhoto": this.userPhoto,
                                "preferredGender": this.preferredGender, "userbodytype": this.userbodytype, "userheight": this.userheight, "userchild": this.userchild,
                                "usersmoke": this.usersmoke, "userdrinks": this.userdrinks, "userzodiac": this.userzodiac, "userRace": this.userRace, "userPreference": this.userPreference, "userAboutme": this.userAboutme,
                                "latitude": this.latitude, "longitude": this.longitude
                            }]);
                    }
                    else {
                        this.errorToast(v.message);
                    }
                    return [2 /*return*/];
                });
            }); });
        }
    };
    RegistersummaryPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    RegistersummaryPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] }
    ]; };
    RegistersummaryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registersummary',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registersummary.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registersummary/registersummary.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registersummary.page.scss */ "./src/app/signup/registersummary/registersummary.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], RegistersummaryPage);
    return RegistersummaryPage;
}());



/***/ })

}]);
//# sourceMappingURL=registersummary-registersummary-module.js.map