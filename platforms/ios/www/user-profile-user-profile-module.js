(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-profile-user-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/user-profile/user-profile.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/user-profile/user-profile.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n        <ion-back-button (click)=\"onclickBack()\" ></ion-back-button>\n      </ion-buttons>\n    <ion-title>Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"userInfo\">\n    <div class=\"profileDesc\">\n        <table>\n            <tr>\n                <td colspan=\"2\">\n                    <img style=\"max-height: 200px;width: 100%;\" src=\"{{url}}thumbnail/{{userInfo.photoList[0]['photo_name']}}_thumb.jpg\" alt=\"\" title=\"\">\n                </td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Passion Dating Name</span>\n                    <h4> {{userInfo.name}}</h4>\n                </td>\n                <td>\n                    <span>Birthdate</span>\n                    <h4>{{userInfo.birthdate}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td colspan=\"2\">\n                    <span>Location</span>\n                    <h4>{{userInfo.city}} {{userInfo.zipcode}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td colspan=\"2\">\n                    <span>Email</span>\n                    <h4>{{userInfo.email_address}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <th colspan=\"2\">\n                    <h4>Basic</h4>\n                </th>\n            </tr>\n            <tr>\n                <td>\n                    <span>Sign</span>\n                    <h4>Aquaries</h4>\n                </td>\n                <td>\n                    <span>Height</span>\n                    <h4>{{userInfo.height}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Gender</span>\n                    <h4>{{userInfo.gender}}</h4>\n                </td>\n                <td>\n                    <span>Interested In</span>\n                    <h4>{{userInfo.Interested}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td>\n                    <span>Ethenticity </span>\n                    <h4>{{userInfo.city}}</h4>\n                </td>\n                <td>\n                    <span>Body Type</span>\n                    <h4>{{userInfo.body_type}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <th colspan=\"2\">\n                    <span>Relationship History</span>\n                    <h4>Add your relationship history</h4>\n                </th>\n            </tr>\n            <tr>\n                <td colspan=\"2\">\n                    <span>Children</span>\n                    <h4>{{userInfo.no_of_children}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td colspan=\"2\">\n                    <span>Education</span>\n                    <h4>{{userInfo.education}}</h4>\n                </td>\n            </tr>\n            <tr>\n                <td colspan=\"2\">\n                    <span>Smoking</span>\n                    <h4>{{userInfo.smoke}}</h4>\n                </td>\n            </tr>\n        </table>\n    </div>\n    <div *ngIf=\"pageType=='0' && userInfo.chat_guid==null\" style=\"margin: 0 20px;text-align: center;\">\n      <ion-button (click)=\"sendInvitaion()\">\n          Invtation\n      </ion-button>\n    </div>\n    <div *ngIf=\"pageType=='1' && userInfo.chat_guid!=null && userInfo.requesterId == gen.getUserInfo().user_id\" style=\"margin: 0 20px;\" >\n        <ion-button (click)=\"approveChat()\" style=\"width: 50%;\">\n            Approver\n        </ion-button>\n        <ion-button (click)=\"rejectChat()\" style=\"width: 50%;\">\n            Reject\n        </ion-button>\n      </div>\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/user-profile/user-profile.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/user-profile/user-profile.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>user-profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/map-component/user-profile/user-profile-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/map-component/user-profile/user-profile-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: UserProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageRoutingModule", function() { return UserProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-profile.page */ "./src/app/map-component/user-profile/user-profile.page.ts");




var routes = [
    {
        path: '',
        component: _user_profile_page__WEBPACK_IMPORTED_MODULE_3__["UserProfilePage"]
    }
];
var UserProfilePageRoutingModule = /** @class */ (function () {
    function UserProfilePageRoutingModule() {
    }
    UserProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], UserProfilePageRoutingModule);
    return UserProfilePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/map-component/user-profile/user-profile.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/map-component/user-profile/user-profile.module.ts ***!
  \*******************************************************************/
/*! exports provided: UserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageModule", function() { return UserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-profile-routing.module */ "./src/app/map-component/user-profile/user-profile-routing.module.ts");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-profile.page */ "./src/app/map-component/user-profile/user-profile.page.ts");







var UserProfilePageModule = /** @class */ (function () {
    function UserProfilePageModule() {
    }
    UserProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserProfilePageRoutingModule"]
            ],
            declarations: [_user_profile_page__WEBPACK_IMPORTED_MODULE_6__["UserProfilePage"]]
        })
    ], UserProfilePageModule);
    return UserProfilePageModule;
}());



/***/ }),

/***/ "./src/app/map-component/user-profile/user-profile.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/map-component/user-profile/user-profile.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 40px;\n  width: 100%;\n  border: solid 1px #981F40;\n  text-align: center;\n  outline: none;\n  text-transform: uppercase;\n  padding-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvbWFwLWNvbXBvbmVudC91c2VyLXByb2ZpbGUvdXNlci1wcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbWFwLWNvbXBvbmVudC91c2VyLXByb2ZpbGUvdXNlci1wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9tYXAtY29tcG9uZW50L3VzZXItcHJvZmlsZS91c2VyLXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15c2VsZWN0IHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiBzb2xpZCAxcHggIzk4MUY0MDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xufSIsIi5teXNlbGVjdCB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogc29saWQgMXB4ICM5ODFGNDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgb3V0bGluZTogbm9uZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/map-component/user-profile/user-profile.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/map-component/user-profile/user-profile.page.ts ***!
  \*****************************************************************/
/*! exports provided: UserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePage", function() { return UserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");







var UserProfilePage = /** @class */ (function () {
    function UserProfilePage(gen, navCtrl, userService, router, activatedroute) {
        this.gen = gen;
        this.navCtrl = navCtrl;
        this.userService = userService;
        this.router = router;
        this.activatedroute = activatedroute;
        this.imgPath = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].root_URL + "/Users/";
        this.pageType = "";
        var user = this.activatedroute.snapshot.paramMap.get('userProfile');
        this.pageType = this.activatedroute.snapshot.paramMap.get('pageType');
        this.userInfo = JSON.parse(user);
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].root_URL;
    }
    UserProfilePage.prototype.onclickBack = function () {
        if (this.pageType == '0') {
            this.navCtrl.navigateBack('home/map-component');
        }
        else {
            this.navCtrl.navigateBack('home/tabs/chat');
        }
    };
    UserProfilePage.prototype.ngOnInit = function () {
    };
    UserProfilePage.prototype.sendInvitaion = function () {
        var _this = this;
        this.gen.presentLoading("please wait..");
        this.userService.SendInvitation(this.userInfo.user_id).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v, Count;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                this.gen.closePresentLoading();
                console.log(v);
                if (v.success) {
                    Count = this.gen.getInvitaionCount();
                    this.gen.setInvitaionCount(Count + 1);
                    //this.navCtrl.back();
                    this.pageType = "2";
                }
                return [2 /*return*/];
            });
        }); });
    };
    UserProfilePage.prototype.approveChat = function () {
        var _this = this;
        this.gen.presentLoading("please wait..");
        this.userService.setChatStatus(this.userInfo.chat_guid, 1).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                this.gen.closePresentLoading();
                console.log(v);
                if (v.success) {
                    this.navCtrl.back();
                }
                return [2 /*return*/];
            });
        }); });
    };
    UserProfilePage.prototype.rejectChat = function () {
        var _this = this;
        this.gen.presentLoading("please wait..");
        this.userService.setChatStatus(this.userInfo.chat_guid, 2).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                this.gen.closePresentLoading();
                if (v.success) {
                    this.pageType = "2";
                    // this.navCtrl.back();
                }
                return [2 /*return*/];
            });
        }); });
    };
    UserProfilePage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    UserProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-profile',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/map-component/user-profile/user-profile.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user-profile.page.scss */ "./src/app/map-component/user-profile/user-profile.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], UserProfilePage);
    return UserProfilePage;
}());



/***/ }),

/***/ "./src/app/signup/user-profile/user-profile-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/user-profile/user-profile-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: UserProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageRoutingModule", function() { return UserProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-profile.page */ "./src/app/signup/user-profile/user-profile.page.ts");




var routes = [
    {
        path: '',
        component: _user_profile_page__WEBPACK_IMPORTED_MODULE_3__["UserProfilePage"]
    }
];
var UserProfilePageRoutingModule = /** @class */ (function () {
    function UserProfilePageRoutingModule() {
    }
    UserProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], UserProfilePageRoutingModule);
    return UserProfilePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/user-profile/user-profile.module.ts":
/*!************************************************************!*\
  !*** ./src/app/signup/user-profile/user-profile.module.ts ***!
  \************************************************************/
/*! exports provided: UserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageModule", function() { return UserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-profile-routing.module */ "./src/app/signup/user-profile/user-profile-routing.module.ts");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-profile.page */ "./src/app/signup/user-profile/user-profile.page.ts");







var UserProfilePageModule = /** @class */ (function () {
    function UserProfilePageModule() {
    }
    UserProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserProfilePageRoutingModule"]
            ],
            declarations: [_user_profile_page__WEBPACK_IMPORTED_MODULE_6__["UserProfilePage"]]
        })
    ], UserProfilePageModule);
    return UserProfilePageModule;
}());



/***/ }),

/***/ "./src/app/signup/user-profile/user-profile.page.scss":
/*!************************************************************!*\
  !*** ./src/app/signup/user-profile/user-profile.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC91c2VyLXByb2ZpbGUvdXNlci1wcm9maWxlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/signup/user-profile/user-profile.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/signup/user-profile/user-profile.page.ts ***!
  \**********************************************************/
/*! exports provided: UserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePage", function() { return UserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserProfilePage = /** @class */ (function () {
    function UserProfilePage() {
    }
    UserProfilePage.prototype.ngOnInit = function () {
    };
    UserProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-profile',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/user-profile/user-profile.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user-profile.page.scss */ "./src/app/signup/user-profile/user-profile.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserProfilePage);
    return UserProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=user-profile-user-profile-module.js.map