(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registerfinished-registerfinished-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content force-overscroll=\"false\">\n    <gl-background-video src=\"assets/video/IMG_1926.mp4\" poster=\"assets/your_video_screen.png\"></gl-background-video>\n    <div class=\"page-centered-element\">\n        <div class=\"content\">\n            <img class=\"logo\" src=\"assets/images/logo.png\">\n            <h3> Congrats! Your Profile was Successfully Created. <br/> Your account will be active once the Vibez Community is able to socialize <br/> ( See the medical communities\n                <ion-label style=\"color:red;\">Response</ion-label> we follow for Covid-19)</h3>\n            <!-- (click)=\"openWebpage('https://www.who.int/emergencies/diseases/novel-coronavirus-2019')\" -->\n            <h5>We will launch once we reach 25,000 downloads. <br/> Help us reach our goal by telling your family and friends about us.</h5>\n            <h5>Download Count:- 200</h5>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: RegisterfinishedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPageRoutingModule", function() { return RegisterfinishedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registerfinished_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registerfinished.page */ "./src/app/signup/registerfinished/registerfinished.page.ts");




var routes = [
    {
        path: '',
        component: _registerfinished_page__WEBPACK_IMPORTED_MODULE_3__["RegisterfinishedPage"]
    }
];
var RegisterfinishedPageRoutingModule = /** @class */ (function () {
    function RegisterfinishedPageRoutingModule() {
    }
    RegisterfinishedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegisterfinishedPageRoutingModule);
    return RegisterfinishedPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.module.ts ***!
  \********************************************************************/
/*! exports provided: RegisterfinishedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPageModule", function() { return RegisterfinishedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registerfinished_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registerfinished-routing.module */ "./src/app/signup/registerfinished/registerfinished-routing.module.ts");
/* harmony import */ var _registerfinished_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registerfinished.page */ "./src/app/signup/registerfinished/registerfinished.page.ts");







var RegisterfinishedPageModule = /** @class */ (function () {
    function RegisterfinishedPageModule() {
    }
    RegisterfinishedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _registerfinished_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterfinishedPageRoutingModule"]
            ], providers: [],
            declarations: [_registerfinished_page__WEBPACK_IMPORTED_MODULE_6__["RegisterfinishedPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], RegisterfinishedPageModule);
    return RegisterfinishedPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".logo {\n  max-width: 50%;\n}\n\n.logoTapCls {\n  text-align: center;\n  margin-top: 15%;\n}\n\n.socialLogin ul {\n  margin-top: 15px !important;\n}\n\nion-content {\n  --background: #53abd9;\n  --background: -moz-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: -webkit-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: linear-gradient(to right, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  display: table;\n}\n\n.page-centered-element {\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  color: #ffffff;\n  font-size: 15px;\n}\n\n.content {\n  text-align: center;\n}\n\n.content .buttons-container {\n  margin-top: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyZmluaXNoZWQvcmVnaXN0ZXJmaW5pc2hlZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3NpZ251cC9yZWdpc3RlcmZpbmlzaGVkL3JlZ2lzdGVyZmluaXNoZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSwyQkFBQTtBQ0NKOztBREVBO0VBQ0kscUJBQUE7RUFDQSwrRUFBQTtFQUNBLGtGQUFBO0VBQ0EsOEVBQUE7RUFDQSxjQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREFJO0VBQ0ksZ0JBQUE7QUNFUiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9yZWdpc3RlcmZpbmlzaGVkL3JlZ2lzdGVyZmluaXNoZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ28ge1xuICAgIG1heC13aWR0aDogNTAlO1xufVxuXG4ubG9nb1RhcENscyB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDE1JTtcbn1cblxuLnNvY2lhbExvZ2luIHVsIHtcbiAgICBtYXJnaW4tdG9wOiAxNXB4IWltcG9ydGFudDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogIzUzYWJkOTtcbiAgICAtLWJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KGxlZnQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICAgIC0tYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgIzUzYWJkOSAwJSwgIzM5NWE2YiA1MCUsICMzMjRhNTcgMTAwJSk7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICAgIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4ucGFnZS1jZW50ZXJlZC1lbGVtZW50IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5jb250ZW50IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLmJ1dHRvbnMtY29udGFpbmVyIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMzBweDtcbiAgICB9XG59IiwiLmxvZ28ge1xuICBtYXgtd2lkdGg6IDUwJTtcbn1cblxuLmxvZ29UYXBDbHMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDE1JTtcbn1cblxuLnNvY2lhbExvZ2luIHVsIHtcbiAgbWFyZ2luLXRvcDogMTVweCAhaW1wb3J0YW50O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogIzUzYWJkOTtcbiAgLS1iYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgLS1iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnBhZ2UtY2VudGVyZWQtZWxlbWVudCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uY29udGVudCAuYnV0dG9ucy1jb250YWluZXIge1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.page.ts ***!
  \******************************************************************/
/*! exports provided: RegisterfinishedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPage", function() { return RegisterfinishedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var gl_ionic_background_video__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! gl-ionic-background-video */ "./node_modules/gl-ionic-background-video/dist/esm/index.js");






// import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

var RegisterfinishedPage = /** @class */ (function () {
    function RegisterfinishedPage(gen, toastCtrl, authSvc, router, activatedroute
    // private iab: InAppBrowser
    ) {
        this.gen = gen;
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this.router = router;
        this.activatedroute = activatedroute;
    }
    RegisterfinishedPage.prototype.ngOnInit = function () {
        this.preferredGender = this.activatedroute.snapshot.paramMap.get('gender');
        this.userBirthday = this.activatedroute.snapshot.paramMap.get('birthdate');
        this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
        this.cityzip = this.activatedroute.snapshot.paramMap.get('cityzip');
        this.profileimg = this.activatedroute.snapshot.paramMap.get('profileimg');
        this.userbodytype = this.activatedroute.snapshot.paramMap.get('bodytype');
        this.userheight = this.activatedroute.snapshot.paramMap.get('height');
        this.userchild = this.activatedroute.snapshot.paramMap.get('noofchild');
        this.usereducation = this.activatedroute.snapshot.paramMap.get('education');
        this.usersmoke = this.activatedroute.snapshot.paramMap.get('smoke');
    };
    RegisterfinishedPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    RegisterfinishedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-registerfinished',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registerfinished.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registerfinished.page.scss */ "./src/app/signup/registerfinished/registerfinished.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]
            // private iab: InAppBrowser
        ])
    ], RegisterfinishedPage);
    return RegisterfinishedPage;
}());



/***/ })

}]);
//# sourceMappingURL=registerfinished-registerfinished-module.js.map