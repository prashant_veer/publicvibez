(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gettingstart-gettingstart-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n    <h2><span>Profile <b>Picture</b></span></h2>\n    <a href=\"JavaScript:void(0)\" (click)=\"selectImage()\" class=\"round-button-create-profile Photo-background\" style=\"margin-top: 20%;\">\n        <ion-img *ngIf=\"img\" [src]=\"img.path\" style=\"height:100%;width:100%;\"></ion-img>\n        <ion-icon *ngIf=\"!img\" name=\"add-outline\" style=\"font-size: 50px;margin-top: 17%;color: darkgrey;\"></ion-icon>\n    </a>\n    <br>\n    <div style=\"width: 70%;margin-left: 15%;\">\n        <div class=\"formElement\">\n            <ion-checkbox slot=\"start\" color=\"light\" checked=\"true\">\n            </ion-checkbox>\n            <ion-label color=\"light\"> Want there photo to be blurry?<br/></ion-label>\n        </div>\n        <ion-label color=\"primary\"> If not it will be visiable to change it go to profile - edit<br/></ion-label>\n        <br>\n        <ion-label color=\"light\"> When your out around people who use the app live you will be in a pin of a 200 foot radias</ion-label>\n        <br/>\n    </div>\n    <div class=\"form-div\">\n        <div class=\"formElement\">\n            <ion-input type=\"text\" [(ngModel)]=\"username\" class=\"general-input ion-input-class\" placeholder=\"Name\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-datetime class=\"general-input ion-input-class\" [(ngModel)]=\"userBirthday\" displayFormat=\"DD-MMM-YYYY\" placeholder=\"BIRTHDAY\" min=\"1950-03-14\" max=\"2001-01-01\"></ion-datetime>\n        </div>\n\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"register_photo()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n\n    </div>\n    <!--\n  <div class=\"wrapper\">\n      <div class=\"loginForm\">\n          <div class=\"tableCell\">\n              <div class=\"steps\">\n                  <div class=\"stepsIn leftAlign selected\">\n                      <span>1</span>\n                      <p>GET STARTED</p>\n                  </div>\n                  <div class=\"stepsIn centerAlign\">\n                      <span>2</span>\n                      <p>Your Photo</p>\n                  </div>\n                  <div class=\"stepsIn rightAlign\">\n                      <span>3</span>\n                      <p>About You</p>\n                  </div>\n              </div>\n              <h1>Get Started</h1>\n              <div class=\"formElement\">\n                  <ion-select [(ngModel)]=\"preferredGender\" class=\"myselect\" (ionChange)=\"genderValue($event)\" [selectedText]=\"genderSelectedtext\">\n                      <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.Gender}}</ion-select-option>\n                  </ion-select>\n              </div>\n              <div class=\"formElement\">\n                  <ion-datetime class=\"myselect\" [(ngModel)]=\"userBirthday\" displayFormat=\"DD-MMM-YYYY\" placeholder=\"BIRTHDAY\" min=\"1950-03-14\" max=\"2001-01-01\"></ion-datetime>\n              </div>\n              <div class=\"formElement\">\n                <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"addressName\" placeholder=\"address\"></ion-input>\n            </div>\n              <div class=\"formElement\">\n                  <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"cityName\" placeholder=\"City\"></ion-input>\n              </div>\n              <div class=\"formElement\">\n                  <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"cityzip\" placeholder=\"zip code\"></ion-input>\n              </div>\n              <div class=\"formElement\">\n                  <ion-button color=\"secondary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"register_photo()\">CONTINUE</ion-button>\n\n              </div>\n\n          </div>\n\n      </div>\n  </div>-->\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart-routing.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart-routing.module.ts ***!
  \********************************************************************/
/*! exports provided: GettingstartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPageRoutingModule", function() { return GettingstartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _gettingstart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gettingstart.page */ "./src/app/signup/gettingstart/gettingstart.page.ts");




var routes = [
    {
        path: '',
        component: _gettingstart_page__WEBPACK_IMPORTED_MODULE_3__["GettingstartPage"]
    }
];
var GettingstartPageRoutingModule = /** @class */ (function () {
    function GettingstartPageRoutingModule() {
    }
    GettingstartPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], GettingstartPageRoutingModule);
    return GettingstartPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.module.ts":
/*!************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.module.ts ***!
  \************************************************************/
/*! exports provided: GettingstartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPageModule", function() { return GettingstartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _gettingstart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gettingstart-routing.module */ "./src/app/signup/gettingstart/gettingstart-routing.module.ts");
/* harmony import */ var _gettingstart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gettingstart.page */ "./src/app/signup/gettingstart/gettingstart.page.ts");







var GettingstartPageModule = /** @class */ (function () {
    function GettingstartPageModule() {
    }
    GettingstartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _gettingstart_routing_module__WEBPACK_IMPORTED_MODULE_5__["GettingstartPageRoutingModule"]
            ],
            declarations: [_gettingstart_page__WEBPACK_IMPORTED_MODULE_6__["GettingstartPage"]]
        })
    ], GettingstartPageModule);
    return GettingstartPageModule;
}());



/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.page.scss":
/*!************************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.page.scss ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".Photo-background {\n  background-color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL2dldHRpbmdzdGFydC9nZXR0aW5nc3RhcnQucGFnZS5zY3NzIiwic3JjL2FwcC9zaWdudXAvZ2V0dGluZ3N0YXJ0L2dldHRpbmdzdGFydC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL2dldHRpbmdzdGFydC9nZXR0aW5nc3RhcnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLlBob3RvLWJhY2tncm91bmR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cbiIsIi5QaG90by1iYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/signup/gettingstart/gettingstart.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/signup/gettingstart/gettingstart.page.ts ***!
  \**********************************************************/
/*! exports provided: GettingstartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GettingstartPage", function() { return GettingstartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");










var GettingstartPage = /** @class */ (function () {
    function GettingstartPage(authSvc, platform, filePath, file, nav, camera, activatedroute, webview, gen, actionSheetController) {
        this.authSvc = authSvc;
        this.platform = platform;
        this.filePath = filePath;
        this.file = file;
        this.nav = nav;
        this.camera = camera;
        this.activatedroute = activatedroute;
        this.webview = webview;
        this.gen = gen;
        this.actionSheetController = actionSheetController;
    }
    GettingstartPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    };
    GettingstartPage.prototype.register_photo = function () {
        if (this.username === undefined || this.username === '') {
            this.gen.presentToast('Please select user name');
        }
        else if (this.userBirthday === undefined || this.userBirthday === '') {
            this.gen.presentToast('Please select your birthdate');
        }
        else {
            this.nav.navigateForward(['signup/registeraboutyou', {
                    NickName: this.NickName,
                    email: this.email,
                    Pass: this.Pass,
                    username: this.username,
                    userBirthday: this.userBirthday,
                    mobile: this.userMobile,
                    area: this.userArea,
                    pincode: this.userPincode,
                    state: this.userState,
                    userPhoto: this.userPhoto,
                    usercity: this.usercity,
                    latitude: this.latitude,
                    longitude: this.longitude
                }]);
        }
    };
    GettingstartPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Select Photo From',
                            buttons: [{
                                    text: 'Load from Library',
                                    role: 'destructive',
                                    icon: 'images',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Use Camera',
                                    role: 'destructive',
                                    icon: 'camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GettingstartPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        });
    };
    GettingstartPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + '.jpg';
        return newFileName;
    };
    GettingstartPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.updateStoredImages(newFileName);
        }, function (error) {
            _this.gen.presentToast('Error while storing file.');
        });
    };
    GettingstartPage.prototype.updateStoredImages = function (name) {
        var filePath = this.file.dataDirectory + name;
        var resPath = this.pathForImage(filePath);
        var newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
        this.img = newEntry;
        this.startUpload(newEntry);
    };
    GettingstartPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    GettingstartPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.gen.presentLoading('Uploading the image');
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            _this.gen.presentToast('Error while reading file.');
        });
    };
    GettingstartPage.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    GettingstartPage.prototype.uploadImageData = function (formData) {
        var _this = this;
        console.log("uploadImageData");
        console.log(formData);
        this.authSvc.registerImage(formData).then(function (req) {
            console.log(req);
            _this.gen.closePresentLoading();
            var v = JSON.parse(req.data);
            if (v.success) {
                console.log(v.data);
                _this.userPhoto = v.data;
            }
            _this.gen.closePresentLoading();
        });
    };
    GettingstartPage.ctorParameters = function () { return [
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__["File"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"] }
    ]; };
    GettingstartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gettingstart',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gettingstart.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/gettingstart/gettingstart.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gettingstart.page.scss */ "./src/app/signup/gettingstart/gettingstart.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_8__["FilePath"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_7__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_5__["Camera"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_9__["WebView"],
            _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]])
    ], GettingstartPage);
    return GettingstartPage;
}());



/***/ })

}]);
//# sourceMappingURL=gettingstart-gettingstart-module.js.map