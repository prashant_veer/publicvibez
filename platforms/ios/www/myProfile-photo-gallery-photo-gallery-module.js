(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["myProfile-photo-gallery-photo-gallery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/photo-gallery/photo-gallery.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/photo-gallery/photo-gallery.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home/tabs/myprofile\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Gallery</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"selectImage()\">\n        <ion-icon name=\"add-circle-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"3\" *ngFor='let photo of userPhotos'> \n        <img src=\"{{url}}thumbnail/{{photo.photo_name}}_thumb.jpg\" />\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/myProfile/photo-gallery/photo-gallery-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/myProfile/photo-gallery/photo-gallery-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: PhotoGalleryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoGalleryPageRoutingModule", function() { return PhotoGalleryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _photo_gallery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./photo-gallery.page */ "./src/app/myProfile/photo-gallery/photo-gallery.page.ts");




var routes = [
    {
        path: '',
        component: _photo_gallery_page__WEBPACK_IMPORTED_MODULE_3__["PhotoGalleryPage"]
    }
];
var PhotoGalleryPageRoutingModule = /** @class */ (function () {
    function PhotoGalleryPageRoutingModule() {
    }
    PhotoGalleryPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PhotoGalleryPageRoutingModule);
    return PhotoGalleryPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/myProfile/photo-gallery/photo-gallery.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/myProfile/photo-gallery/photo-gallery.module.ts ***!
  \*****************************************************************/
/*! exports provided: PhotoGalleryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoGalleryPageModule", function() { return PhotoGalleryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _photo_gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./photo-gallery-routing.module */ "./src/app/myProfile/photo-gallery/photo-gallery-routing.module.ts");
/* harmony import */ var _photo_gallery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./photo-gallery.page */ "./src/app/myProfile/photo-gallery/photo-gallery.page.ts");







var PhotoGalleryPageModule = /** @class */ (function () {
    function PhotoGalleryPageModule() {
    }
    PhotoGalleryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _photo_gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__["PhotoGalleryPageRoutingModule"]
            ],
            declarations: [_photo_gallery_page__WEBPACK_IMPORTED_MODULE_6__["PhotoGalleryPage"]]
        })
    ], PhotoGalleryPageModule);
    return PhotoGalleryPageModule;
}());



/***/ }),

/***/ "./src/app/myProfile/photo-gallery/photo-gallery.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/myProfile/photo-gallery/photo-gallery.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL215UHJvZmlsZS9waG90by1nYWxsZXJ5L3Bob3RvLWdhbGxlcnkucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/myProfile/photo-gallery/photo-gallery.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/myProfile/photo-gallery/photo-gallery.page.ts ***!
  \***************************************************************/
/*! exports provided: PhotoGalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoGalleryPage", function() { return PhotoGalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");












var PhotoGalleryPage = /** @class */ (function () {
    function PhotoGalleryPage(webview, ref, file, filePath, platform, camera, actionSheetController, activatedroute, toastCtrl, general, authSvc, userSvc) {
        this.webview = webview;
        this.ref = ref;
        this.file = file;
        this.filePath = filePath;
        this.platform = platform;
        this.camera = camera;
        this.actionSheetController = actionSheetController;
        this.activatedroute = activatedroute;
        this.toastCtrl = toastCtrl;
        this.general = general;
        this.authSvc = authSvc;
        this.userSvc = userSvc;
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].root_URL;
        this.images = [];
    }
    PhotoGalleryPage.prototype.ngOnInit = function () {
    };
    PhotoGalleryPage.prototype.ionViewWillEnter = function () {
        this.userInfo = JSON.parse(this.activatedroute.snapshot.paramMap.get('user'));
        this.goto_profilePhoto();
    };
    PhotoGalleryPage.prototype.goto_profilePhoto = function () {
        var _this = this;
        this.general.presentLoading("Please wait... ");
        this.authSvc.getPhotoList(this.userInfo).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.general.closePresentLoading();
                v = JSON.parse(req.data);
                console.log(v);
                if (v.success) {
                    this.userPhotos = v.data;
                }
                return [2 /*return*/];
            });
        }); });
    };
    PhotoGalleryPage.prototype.selectImage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Select Photo From',
                            buttons: [{
                                    text: 'Load from Library',
                                    role: 'destructive',
                                    icon: 'images',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Use Camera',
                                    role: 'destructive',
                                    icon: 'camera',
                                    handler: function () {
                                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PhotoGalleryPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        });
    };
    PhotoGalleryPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + '.jpg';
        return newFileName;
    };
    PhotoGalleryPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(function (success) {
            _this.updateStoredImages(newFileName);
        }, function (error) {
            console.log(error);
            _this.general.presentToast('Error while storing file.');
        });
    };
    PhotoGalleryPage.prototype.updateStoredImages = function (name) {
        var filePath = this.file.dataDirectory + name;
        var resPath = this.pathForImage(filePath);
        var newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
        this.images = [newEntry].concat(this.images);
        console.log(this.images);
        this.ref.detectChanges(); // trigger change detection cycle
        this.startUpload(this.images[0]);
    };
    PhotoGalleryPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            var converted = this.webview.convertFileSrc(img);
            return converted;
        }
    };
    PhotoGalleryPage.prototype.startUpload = function (imgEntry) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
            .then(function (entry) {
            entry.file(function (file) { return _this.readFile(file); });
        })
            .catch(function (err) {
            _this.general.presentToast('Error while reading file.');
        });
    };
    PhotoGalleryPage.prototype.readFile = function (file) {
        var _this = this;
        console.log(file);
        var reader = new FileReader();
        reader.onload = function () {
            _this.general.presentLoading('Uploading the image');
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            console.log(imgBlob);
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        reader.readAsArrayBuffer(file);
    };
    PhotoGalleryPage.prototype.uploadImageData = function (formData) {
        var _this = this;
        // this.loadingController.present();
        var obj = this;
        console.log("uploadImageData");
        obj.authSvc.registerImage(formData).then(function (req) {
            console.log(req);
            var v = JSON.parse(req.data);
            if (v.success) {
                obj.userSvc.uploaduserImg(v.data).then(function (req) {
                    console.log("uploaduserImg");
                    _this.images = [];
                    console.log(req);
                    var imageReq = JSON.parse(req.data);
                    obj.userPhotos.push({
                        "photo_name": v.data,
                        "photo_id": imageReq.photosId,
                        "user_id": obj.general.getUserInfo().user_id
                    });
                    obj.general.closePresentLoading();
                });
            }
            else {
                obj.general.closePresentLoading();
                obj.general.presentToast(v.message);
            }
        });
    };
    PhotoGalleryPage.ctorParameters = function () { return [
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_11__["WebView"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"] },
        { type: _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] }
    ]; };
    PhotoGalleryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-photo-gallery',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./photo-gallery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/photo-gallery/photo-gallery.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./photo-gallery.page.scss */ "./src/app/myProfile/photo-gallery/photo-gallery.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_11__["WebView"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_9__["FilePath"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _services_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"], _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], PhotoGalleryPage);
    return PhotoGalleryPage;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");





var LoginService = /** @class */ (function () {
    function LoginService(http, file) {
        this.http = http;
        this.file = file;
        this.http.setDataSerializer('urlencoded');
    }
    LoginService.prototype.sendotptoEmail = function (userEmail) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "userEmail": userEmail
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.sendOtptoEmail(), formData, {});
    };
    LoginService.prototype.login = function (mobile, password, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "username": mobile, "password": password, "token": token };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.login(), formData, {});
    };
    LoginService.prototype.register = function (NickName, email, userMobile, Pass, usercity, userArea, userPincode, userState, username, userBirthday, userPhoto, preferredGender, userbodytype, userheight, userchild, usersmoke, userdrinks, userzodiac, userRace, userPreference, userAboutme, latitude, longitude) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "nickName": NickName, "userEmail": email,
            "userMobile": userMobile, "pass": Pass,
            "username": username, "userBirthday": userBirthday, "profileimg": userPhoto,
            "cityName": usercity, "addressName": userArea, "userPincode": userPincode, "userState": userState,
            "preferredGender": preferredGender, "userbodytype": userbodytype, "userheight": userheight, "userchild": userchild,
            "usersmoke": usersmoke, "userdrinks": userdrinks, "userzodiac": userzodiac, "userRace": userRace, "userPreference": userPreference, "userAboutme": userAboutme,
            "latitude": latitude, "longitude": longitude
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.registerLong(), formData, {});
    };
    LoginService.prototype.getPhotoList = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.photoList(), formData, {});
    };
    LoginService.prototype.updateProfile = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
            "name": userInfo.name,
            "gender": userInfo.gender,
            "birthdate": userInfo.birthdate, "email_address": userInfo.email_address,
            "city": userInfo.city, "zipcode": userInfo.zipcode,
            "body_type": userInfo.body_type, "height": userInfo.height,
            "education": userInfo.education, "no_of_children": userInfo.no_of_children,
            "smoke": userInfo.smoke, "Interested": userInfo.Interested
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfile(), formData, {});
    };
    LoginService.prototype.registerImage = function (usersmoke) {
        console.log(usersmoke);
        this.http.setDataSerializer('multipart');
        this.http.setRequestTimeout(7000);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfileImg(), usersmoke, {});
    };
    LoginService.prototype.googleLogin = function (userEmail, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "googleID": "", "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.googleLogin(), formData, {});
    };
    LoginService.prototype.facebookLogin = function (userEmail, facebookId, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "facebookId": facebookId, "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.facebookLogin(), formData, {});
    };
    LoginService.ctorParameters = function () { return [
        { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"],
            _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"]])
    ], LoginService);
    return LoginService;
}());



/***/ })

}]);
//# sourceMappingURL=myProfile-photo-gallery-photo-gallery-module.js.map