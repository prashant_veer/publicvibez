(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"tableCell\">\n        <div class=\"logoTapCls\">\n            <img class=\"logo\" src=\"assets/images/logo.png\">\n        </div>\n        <p class=\"welcomeMsg\"><span>Welcome to <b>VIBEZ</b></span></p>\n        <div class=\"form-div\">\n            <div class=\"formElement\">\n                <ion-input type=\"email\" [(ngModel)]=\"useremail\" class=\"username ion-input-class\" placeholder=\"Username\"></ion-input>\n            </div>\n            <div class=\"formElement\">\n                <ion-input type=\"password\" [(ngModel)]=\"userpwd\" class=\"password ion-input-class\" placeholder=\"password\"></ion-input>\n            </div>\n            <ion-grid>\n                <ion-row class=\"ion-text-center\">\n                    <ion-col>\n                        <ion-button (click)=\"goto_tab()\" color=\"light\" expand=\"full\" shape=\"round\" class=\"ion-button-class\">SIGN IN</ion-button>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n\n\n        </div>\n        <div class=\"socialLogin\">\n            <h4>OR</h4>\n            <br/>\n            <div class=\"form-div\">\n                <ion-grid>\n                    <ion-row class=\"ion-text-center\">\n                        <ion-col>\n                            <ion-button (click)=\"goto_SignUp()\" color=\"light\" expand=\"full\" shape=\"round\">SIGN UP</ion-button>\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n            </div>\n            <br/>\n            <p>Login With</p>\n            <ul>\n                <li>\n                    <img (click)=\"goto_Facebook()\" src=\"assets/images/facebook.svg\" alt=\"Facebook\" title=\"Facebook\">\n                </li>\n                <li>\n                    <img (click)=\"goto_google()\" src=\"assets/images/google.svg\" alt=\"google\" title=\"google\">\n                </li>\n            </ul>\n\n        </div>\n    </div>\n    <!--\n          <div class=\"wrapper\">\n      <div class=\"loginForm\">\n          <div class=\"tableCell\">\n              <h1>Login</h1>\n              <p class=\"headDesc\">Login to see who's new in your area!</p>\n\n              <div class=\"formElement\">\n                  <ion-input type=\"email\" class=\"myselect\"  placeholder=\"EMAIL ADDRESS\"></ion-input>\n              </div>\n              <div class=\"formElement\">\n                  <ion-input type=\"password\" class=\"myprassswordselect\"  placeholder=\"password\"></ion-input>\n              </div>\n              <br>\n              <div class=\"formElement\">\n                  <ion-grid>\n                      <ion-row>\n                          <ion-col text-center>\n                              <ion-label color=\"secondary\" text-lg-capitalize>Forgot Password?</ion-label>\n                          </ion-col>\n                      </ion-row>\n                  </ion-grid>\n              </div>\n              <br>\n              <div class=\"formElement\">\n                  <ion-button color=\"secondary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"goto_tab()\">LOGIN</ion-button>\n              </div>\n\n              <p class=\"or\">OR LOGIN WITH</p>\n              <div class=\"socialLogin\">\n                  <img src=\"assets/images/facebook.svg\" (click)=\"goto_Facebook()\" alt=\"Facebook\" title=\"Facebook\">\n                  <img src=\"assets/images/google-plus.svg\" (click)=\"goto_google()\" alt=\"google\" title=\"google\">\n              </div>\n          </div>\n          <p class=\"note\">*By selecting \"Log in with Facebook\" or \"Log in with Google\", you agree to our <b>Terms of User</b><br>(including the mandatory arbitration of bispute)</p>\n         \n      </div>\n    </div>\n    -->\n</ion-content>");

/***/ }),

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");




var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
var LoginPageRoutingModule = /** @class */ (function () {
    function LoginPageRoutingModule() {
    }
    LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], LoginPageRoutingModule);
    return LoginPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");









var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
            providers: [
                _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__["Facebook"]
            ]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".logo {\n  max-width: 50%;\n}\n\n.logoTapCls {\n  text-align: center;\n  margin-top: 15%;\n}\n\n.socialLogin ul {\n  margin-top: 15px !important;\n}\n\nion-content {\n  --background: #53abd9;\n  --background: -moz-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: -webkit-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: linear-gradient(to right, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  display: table;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0FDQ0o7O0FEQ0E7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUNFSjs7QURBQTtFQUNJLDJCQUFBO0FDR0o7O0FEREE7RUFFSSxxQkFBQTtFQUNBLCtFQUFBO0VBQ0Esa0ZBQUE7RUFDQSw4RUFBQTtFQUNBLGNBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dve1xuICAgIG1heC13aWR0aDogNTAlO1xufVxuLmxvZ29UYXBDbHN7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDE1JTtcbn1cbi5zb2NpYWxMb2dpbiB1bHtcbiAgICBtYXJnaW4tdG9wOiAxNXB4IWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50e1xuXG4gICAgLS1iYWNrZ3JvdW5kOiAjNTNhYmQ5O1xuICAgIC0tYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQobGVmdCwgIzUzYWJkOSAwJSwgIzM5NWE2YiA1MCUsICMzMjRhNTcgMTAwJSk7XG4gICAgLS1iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzUzYWJkOSAwJSwgIzM5NWE2YiA1MCUsICMzMjRhNTcgMTAwJSk7XG4gICAgZGlzcGxheTogdGFibGU7XG59IiwiLmxvZ28ge1xuICBtYXgtd2lkdGg6IDUwJTtcbn1cblxuLmxvZ29UYXBDbHMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDE1JTtcbn1cblxuLnNvY2lhbExvZ2luIHVsIHtcbiAgbWFyZ2luLXRvcDogMTVweCAhaW1wb3J0YW50O1xufVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogIzUzYWJkOTtcbiAgLS1iYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgLS1iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICBkaXNwbGF5OiB0YWJsZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/fcm/ngx */ "./node_modules/@ionic-native/fcm/ngx/index.js");









var LoginPage = /** @class */ (function () {
    function LoginPage(nav, fcm, general, toastCtrl, authSvc, googlePlus, fb, router) {
        this.nav = nav;
        this.fcm = fcm;
        this.general = general;
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this.googlePlus = googlePlus;
        this.fb = fb;
        this.router = router;
    }
    LoginPage.prototype.ngOnInit = function () {
        var obj = this;
        if (localStorage.getItem("userInfo") != null) {
            //obj.general.presentLoading("Please wait... ");
            setTimeout(function () {
                console.log("Please");
                obj.general.restoreUserInfo();
                obj.router.navigateByUrl('postlogin');
                // obj.router.navigateByUrl("/home/tabs/trending");
            }, 1000);
        }
        obj.fcm.getToken().then(function (token) {
            console.log(token);
            obj.token = token;
        });
        obj.fcm.onNotification().subscribe(function (data) {
            if (data.wasTapped) {
                console.log("Received in background");
            }
        });
    };
    LoginPage.prototype.goto_tab = function () {
        this.router.navigateByUrl('postlogin');
        // this.router.navigateByUrl('/home/tabs/trending');
        // // /this.sessionUseremail
        // console.log(this.useremail);
        // console.log(this.userpwd);
        // if (!this.useremail || this.useremail == "") {
        //   this.general.presentToast('Please enter registred email id.');
        // } else if (!this.userpwd || this.userpwd == "") {
        //   this.general.presentToast('Please enter password');
        // } else {
        //   this.general.presentLoading("Please wait... ");
        //   this.router.navigateByUrl('/home/tabs/map');
        //   // this.authSvc.login(this.useremail, this.userpwd, this.token).then(async req => {
        //   //   console.log(req.data);
        //   //   var v = JSON.parse(req.data);
        //   //   console.log(v);
        //   //   this.general.closePresentLoading();
        //   //   if (v.success) {
        //   //     this.general.setUserInfo(v.data);
        //   //     this.general.setUserPhotos(v.Photos);
        //   //     this.router.navigateByUrl('/home/tabs/map');
        //   //   } else {
        //   //     this.errorToast(v.message);
        //   //   }
        //   // });
        // }
    };
    LoginPage.prototype.goto_social = function (res, socialType) {
        var _this = this;
        /* if(socialType==1){
           console.log(res);
           this.general.presentLoading("Please wait... ");
           this.authSvc.facebookLogin(res.email, this.token).then(async req => {
             console.log(req);
             if(req.data){
               var v= JSON.parse(req.data);
               this.general.closePresentLoading();
               if (v.success) {
                 this.general.setUserInfo(v.data);
                 this.general.setUserPhotos(v.Photos);
                 this.router.navigateByUrl('/home/tabs/map');
               } else {
                   this.errorToast(v.message);
               }
             }
           });
         }else{*/
        this.general.presentLoading("Please wait... ");
        this.authSvc.googleLogin(res.email, this.token).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log(req);
                if (req.data) {
                    v = JSON.parse(req.data);
                    this.general.closePresentLoading();
                    if (v.success) {
                        this.general.setUserInfo(v.data);
                        this.general.setUserPhotos(v.Photos);
                        this.router.navigateByUrl('/home/tabs/map');
                    }
                    else {
                        this.errorToast(v.message);
                    }
                }
                return [2 /*return*/];
            });
        }); });
        // }
    };
    LoginPage.prototype.goto_google = function () {
        var _this = this;
        this.googlePlus.login({})
            .then(function (res) {
            console.log(JSON.stringify(res));
            _this.goto_social(res, 2);
        })
            .catch(function (err) {
            console.error(err);
        });
    };
    LoginPage.prototype.goto_Facebook = function () {
        var _this = this;
        this.fb.getLoginStatus().then(function (res) {
            console.log(res);
            if (res.status === 'connected') {
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.fb.login(['email'])
                    .then(function (res) {
                    if (res.status === 'connected') {
                        _this.getUserDetail(res.authResponse.userID);
                    }
                    else {
                    }
                })
                    .catch(function (e) { return console.log('Error logging into Facebook', e); });
            }
        });
    };
    LoginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
            .then(function (res) {
            console.log(res);
            _this.goto_social(res, 1);
            //this.users = res;
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    LoginPage.prototype.goto_SignUp = function () {
        this.general.isSignup = true;
        this.nav.navigateForward("signup");
    };
    LoginPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
        { type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
        { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_5__["GooglePlus"] },
        { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
    ]; };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"],
            _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_5__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map