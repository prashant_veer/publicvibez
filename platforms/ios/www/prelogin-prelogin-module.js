(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["prelogin-prelogin-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/prelogin/prelogin.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/prelogin/prelogin.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"wrapper\">\n      <div class=\"loginForm\">\n          <div class=\"tableCell\">\n              <img class=\"logo\" src=\"assets/images/logo.jpg\" alt=\"Public Vibez\" title=\"Public Vibez\">\n\n              <br><br>\n              <p class=\"dream_para\">DREAM OF FINDING LOVE</p>\n              <br><br><br>\n              <p class=\"single_p\"><b>More singles who</b></p>\n\n              <p class=\"single_p\"><b>are more your style</b></p>\n              <br><br><br>\n              <p class=\"last_p\">40,000,000 singles worldwide and</p>\n\n              <p class=\"last_p\">3 million messages sent daily</p>\n              <br><br><br>\n              <div class=\"formElement\">\n                  <ion-button color=\"secondary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"goto_login()\">LOGIN</ion-button>\n              </div>\n              <div class=\"formElement\">\n                  <ion-button color=\"tertiary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"goto_signup()\">SIGN UP</ion-button>\n              </div>\n\n          </div>\n\n      </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/prelogin/prelogin-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/prelogin/prelogin-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: PreloginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreloginPageRoutingModule", function() { return PreloginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _prelogin_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./prelogin.page */ "./src/app/prelogin/prelogin.page.ts");




var routes = [
    {
        path: '',
        component: _prelogin_page__WEBPACK_IMPORTED_MODULE_3__["PreloginPage"]
    }
];
var PreloginPageRoutingModule = /** @class */ (function () {
    function PreloginPageRoutingModule() {
    }
    PreloginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PreloginPageRoutingModule);
    return PreloginPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/prelogin/prelogin.module.ts":
/*!*********************************************!*\
  !*** ./src/app/prelogin/prelogin.module.ts ***!
  \*********************************************/
/*! exports provided: PreloginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreloginPageModule", function() { return PreloginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _prelogin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./prelogin-routing.module */ "./src/app/prelogin/prelogin-routing.module.ts");
/* harmony import */ var _prelogin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./prelogin.page */ "./src/app/prelogin/prelogin.page.ts");







var PreloginPageModule = /** @class */ (function () {
    function PreloginPageModule() {
    }
    PreloginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _prelogin_routing_module__WEBPACK_IMPORTED_MODULE_5__["PreloginPageRoutingModule"]
            ],
            declarations: [_prelogin_page__WEBPACK_IMPORTED_MODULE_6__["PreloginPage"]]
        })
    ], PreloginPageModule);
    return PreloginPageModule;
}());



/***/ }),

/***/ "./src/app/prelogin/prelogin.page.scss":
/*!*********************************************!*\
  !*** ./src/app/prelogin/prelogin.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".dream_para {\n  color: grey;\n  font-stretch: expanded;\n  font-size: medium;\n}\n\n.single_p {\n  color: #000000;\n  font-stretch: expanded;\n  font-size: 24px;\n  text-align: center;\n  padding-bottom: 10px;\n}\n\n.last_p {\n  color: #981F40;\n  font-stretch: expanded;\n  font-size: medium;\n  padding-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvcHJlbG9naW4vcHJlbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wcmVsb2dpbi9wcmVsb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3ByZWxvZ2luL3ByZWxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kcmVhbV9wYXJhIHtcbiAgICBjb2xvcjogZ3JleTtcbiAgICBmb250LXN0cmV0Y2g6IGV4cGFuZGVkO1xuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xufVxuXG4uc2luZ2xlX3Age1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIGZvbnQtc3RyZXRjaDogZXhwYW5kZWQ7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cblxuLmxhc3RfcCB7XG4gICAgY29sb3I6ICM5ODFGNDA7XG4gICAgZm9udC1zdHJldGNoOiBleHBhbmRlZDtcbiAgICBmb250LXNpemU6IG1lZGl1bTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn0iLCIuZHJlYW1fcGFyYSB7XG4gIGNvbG9yOiBncmV5O1xuICBmb250LXN0cmV0Y2g6IGV4cGFuZGVkO1xuICBmb250LXNpemU6IG1lZGl1bTtcbn1cblxuLnNpbmdsZV9wIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtc3RyZXRjaDogZXhwYW5kZWQ7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cblxuLmxhc3RfcCB7XG4gIGNvbG9yOiAjOTgxRjQwO1xuICBmb250LXN0cmV0Y2g6IGV4cGFuZGVkO1xuICBmb250LXNpemU6IG1lZGl1bTtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/prelogin/prelogin.page.ts":
/*!*******************************************!*\
  !*** ./src/app/prelogin/prelogin.page.ts ***!
  \*******************************************/
/*! exports provided: PreloginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreloginPage", function() { return PreloginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");




var PreloginPage = /** @class */ (function () {
    function PreloginPage(nav, gen) {
        this.nav = nav;
        this.gen = gen;
    }
    PreloginPage.prototype.ngOnInit = function () {
    };
    PreloginPage.prototype.goto_login = function () {
        this.nav.navigateForward('login');
    };
    PreloginPage.prototype.goto_signup = function () {
        this.gen.setIsSignup(true);
        this.nav.navigateForward('signup');
    };
    PreloginPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] }
    ]; };
    PreloginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-prelogin',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./prelogin.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/prelogin/prelogin.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./prelogin.page.scss */ "./src/app/prelogin/prelogin.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"]])
    ], PreloginPage);
    return PreloginPage;
}());



/***/ })

}]);
//# sourceMappingURL=prelogin-prelogin-module.js.map