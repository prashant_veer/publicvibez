(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profileother-profileother-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/profileother/profileother.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/profileother/profileother.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar>\n        <ion-title>User </ion-title>\n        <ion-buttons slot=\"start\" (click)='back_livelist()'>\n            <ion-button>\n                <ion-icon name=\"arrow-back-outline\" style=\"font-size: 15px; padding-left: 15px;color: skyblue;\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"mainDiv uploadInfo\">\n\n    <a href=\"JavaScript:void(0)\" class=\"round-button-create-profile Photo-background\" style=\"margin-top: 20%;\">\n        <ion-img *ngIf=\"img\" [src]=\"img.path\" style=\"height:50%;width:50%;\"></ion-img>\n        <ion-icon *ngIf=\"!img\" name=\"add-outline\" style=\"font-size: 50px;margin-top: 17%;color: darkgrey;\"></ion-icon>\n    </a>\n\n    <h2>\n        <ion-icon name=\"person-outline\" style=\"color: skyblue;\"></ion-icon>Zami Designer</h2>\n\n    <div class=\"other\">\n        <div class=\"mainDiv uploadInfo\">\n            <div class=\"tableCell\">\n                <!-- <h2><span>Upload <b>Information</b></span></h2> -->\n                <div class=\"form-div\">\n                    <div class=\"formElement\">\n                        <div class=\"ion-textarea-Box\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"12\">\n                                        <ion-textarea placeholder=\"About Other User\" class=\"box-label label-padding\"></ion-textarea>\n                                    </ion-col>\n\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Gender</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"preferredGender\" class=\"myselect\" (ionChange)=\"genderValue($event)\" [selectedText]=\"genderSelectedtext\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">BodyType</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userbodytype\" class=\"myselect\" (ionChange)=\"bodytypeValue($event)\" [selectedText]=\"bodytypetext\">\n                                            <ion-select-option *ngFor=\"let bodyt of bodytypearray\">{{bodyt.bodytype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Height</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userheight\" class=\"myselect\" (ionChange)=\"heightValue($event)\" [selectedText]=\"userheighttext\">\n                                            <ion-select-option *ngFor=\"let ht of userheightarray\">{{ht.height}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Child</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userchild\" class=\"myselect\" (ionChange)=\"childValue($event)\" [selectedText]=\"userchildtext\">\n                                            <ion-select-option *ngFor=\"let ch of userchildarray\">{{ch.child}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Smoke</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"usersmoke\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" [selectedText]=\"usersmoketext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Drinks</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userdrinks\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" [selectedText]=\"userdrinkstext\">\n                                            <ion-select-option *ngFor=\"let ch of userDrinksarray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Zodiac</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userzodiac\" class=\"myselect\" [selectedText]=\"zodiactext\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Race</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select placeholder=\"Select Race\" class=\"myselect\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Prefecen</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select placeholder=\"Select Prefecen\" class=\"myselect\">\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n\n                    <ion-grid>\n                        <ion-row class=\"ion-text-center\">\n                            <ion-col>\n                                <a href=\"\" class=\"round-button gradient-background\">\n                                    <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                                </a>\n\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/profileother/profileother-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/home/profileother/profileother-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: ProfileotherPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileotherPageRoutingModule", function() { return ProfileotherPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profileother_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profileother.page */ "./src/app/home/profileother/profileother.page.ts");




var routes = [
    {
        path: '',
        component: _profileother_page__WEBPACK_IMPORTED_MODULE_3__["ProfileotherPage"]
    }
];
var ProfileotherPageRoutingModule = /** @class */ (function () {
    function ProfileotherPageRoutingModule() {
    }
    ProfileotherPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ProfileotherPageRoutingModule);
    return ProfileotherPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/profileother/profileother.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/home/profileother/profileother.module.ts ***!
  \**********************************************************/
/*! exports provided: ProfileotherPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileotherPageModule", function() { return ProfileotherPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _profileother_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profileother-routing.module */ "./src/app/home/profileother/profileother-routing.module.ts");
/* harmony import */ var _profileother_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profileother.page */ "./src/app/home/profileother/profileother.page.ts");







var ProfileotherPageModule = /** @class */ (function () {
    function ProfileotherPageModule() {
    }
    ProfileotherPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _profileother_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfileotherPageRoutingModule"]
            ],
            declarations: [_profileother_page__WEBPACK_IMPORTED_MODULE_6__["ProfileotherPage"]]
        })
    ], ProfileotherPageModule);
    return ProfileotherPageModule;
}());



/***/ }),

/***/ "./src/app/home/profileother/profileother.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/home/profileother/profileother.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.box-left {\n  background: #37A4DC;\n  padding-right: -10px;\n}\n\n.box-label {\n  background: #CCD1D1;\n  font-size: small;\n}\n\n.label-padding {\n  padding-left: 15px;\n}\n\n.myselect-label {\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvaG9tZS9wcm9maWxlb3RoZXIvcHJvZmlsZW90aGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9wcm9maWxlb3RoZXIvcHJvZmlsZW90aGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxvQkFBQTtBQ0NKOztBREVBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUtJLGdCQUFBO0FDSEoiLCJmaWxlIjoic3JjL2FwcC9ob21lL3Byb2ZpbGVvdGhlci9wcm9maWxlb3RoZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15c2VsZWN0IHtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3gtbGVmdCB7XG4gICAgYmFja2dyb3VuZDogIzM3QTREQztcbiAgICBwYWRkaW5nLXJpZ2h0OiAtMTBweDtcbn1cblxuLmJveC1sYWJlbCB7XG4gICAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgICBmb250LXNpemU6IHNtYWxsO1xufVxuXG4ubGFiZWwtcGFkZGluZyB7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4ubXlzZWxlY3QtbGFiZWwge1xuICAgIC8vICB3aWR0aDogMTAwJTtcbiAgICAvLyAgaGVpZ2h0OiA0MHB4O1xuICAgIC8vICBwYWRkaW5nOiAxOHB4IDE3cHggMThweCAzMHB4O1xuICAgIC8vICBiYWNrZ3JvdW5kOiAjQ0NEMUQxO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG59IiwiLm15c2VsZWN0IHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBvdXRsaW5lOiBub25lO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBwYWRkaW5nOiAwIDE1cHggMCAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3gtbGVmdCB7XG4gIGJhY2tncm91bmQ6ICMzN0E0REM7XG4gIHBhZGRpbmctcmlnaHQ6IC0xMHB4O1xufVxuXG4uYm94LWxhYmVsIHtcbiAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmxhYmVsLXBhZGRpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbi5teXNlbGVjdC1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/profileother/profileother.page.ts":
/*!********************************************************!*\
  !*** ./src/app/home/profileother/profileother.page.ts ***!
  \********************************************************/
/*! exports provided: ProfileotherPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileotherPage", function() { return ProfileotherPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var ProfileotherPage = /** @class */ (function () {
    function ProfileotherPage(nav) {
        this.nav = nav;
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userchildarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.genderprefer = [];
        this.userDrinksarray = [];
    }
    ProfileotherPage.prototype.ngOnInit = function () {
        this.genderSelectedtext = 'Gender';
        this.bodytypetext = 'What\'s your body type?';
        this.userheighttext = 'What\'s your height?';
        this.userchildtext = 'Do you have children?';
        this.usereducationtext = 'What\'s your education?';
        this.usersmoketext = 'do you smoke?';
        this.userdrinkstext = 'do you drink?';
        this.zodiactext = 'zodiac';
        this.petpeevestext = 'Pet Peeves';
        this.showgender();
        this.showbodytype();
        this.showchild();
        this.showeducation();
        this.showheight();
        this.showsmoke();
    };
    ProfileotherPage.prototype.showbodytype = function () {
        this.bodytypearray = [{ bodytype: 'Muscles' },
            { bodytype: 'Athletic' },
            { bodytype: 'Curvy' },
            { bodytype: 'Overweight' },
            { bodytype: 'Underweight' }
        ];
    };
    ProfileotherPage.prototype.bodytypeValue = function (event) {
        console.log(event.detail.value);
        this.bodytypetext = event.detail.value;
    };
    ProfileotherPage.prototype.showgender = function () {
        this.genderprefer = [{ gendertype: 'Male' },
            { gendertype: 'Female' }
        ];
    };
    ProfileotherPage.prototype.genderValue = function (event) {
        console.log(event.detail.value);
        this.genderSelectedtext = event.detail.value;
    };
    ProfileotherPage.prototype.showheight = function () {
        this.userheightarray = [{ height: '<5 Foot' },
            { height: '>5 Foot' },
            { height: '>6 Foot' },
            { height: '<6 Foot' }
        ];
    };
    ProfileotherPage.prototype.heightValue = function (event) {
        console.log(event.detail.value);
        this.userheighttext = event.detail.value;
    };
    ProfileotherPage.prototype.showchild = function () {
        this.userchildarray = [{ child: 'YES' },
            { child: 'NO' }];
    };
    ProfileotherPage.prototype.childValue = function (event) {
        console.log(event.detail.value);
        this.userchildtext = event.detail.value;
    };
    ProfileotherPage.prototype.showeducation = function () {
        this.usereducationarray = [{ education: 'HS GED' },
            { education: 'HS Graduate' },
            { education: 'Attending College' },
            { education: 'College Graduate' },
            { education: 'Attending Advance Degree' },
            { education: 'Graduate Degree' }
        ];
    };
    ProfileotherPage.prototype.educationValue = function (event) {
        console.log(event.detail.value);
        this.usereducationtext = event.detail.value;
    };
    ProfileotherPage.prototype.showsmoke = function () {
        this.usersmokearray = [{ smoke: 'YES' },
            { smoke: 'NO' }
        ];
    };
    ProfileotherPage.prototype.smokeValue = function (event) {
        console.log(event.detail.value);
        this.usersmoketext = event.detail.value;
    };
    ProfileotherPage.prototype.back_livelist = function () {
        this.nav.navigateForward('home/tabs/live');
    };
    ProfileotherPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"] }
    ]; };
    ProfileotherPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-profileother',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profileother.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/profileother/profileother.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profileother.page.scss */ "./src/app/home/profileother/profileother.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], ProfileotherPage);
    return ProfileotherPage;
}());



/***/ })

}]);
//# sourceMappingURL=profileother-profileother-module.js.map