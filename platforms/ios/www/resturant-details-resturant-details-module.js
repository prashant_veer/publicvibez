(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resturant-details-resturant-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-details/resturant-details.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-details/resturant-details.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\n    <ion-toolbar class=\"new-background-color\">\n        <ion-buttons slot=\"start\" color=\"danger\">\n            <ion-icon name=\"arrow-back-outline\" style=\"font-size: medium;\" (click)=\"back_trending()\"></ion-icon>\n        </ion-buttons>\n        <ion-title class=\"ion-text-center\">\n            <b> Restuarant Details</b>\n        </ion-title>\n\n    </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div class=\"myrestaurant\">\n        <div class=\"imageSub\" style=\"width: 100%;\">\n            <img src=\"assets/images/rest0.jpg\" alt=\"Something\" />\n            <div class=\"blackbg\"></div>\n            <div class=\"label\">\n                <!-- <ion-icon name=\"document-text-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\">\n                </ion-icon> -->\n                <ion-icon name=\"create-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\"></ion-icon>\n                <ion-icon name=\"heart-circle-outline\" style=\"float: right;font-size: 30px;color:red\"></ion-icon>\n            </div>\n        </div>\n    </div>\n    <br>\n    <ion-card>\n\n        <ion-card-content>\n            <h2>Restaurant Name </h2>\n            <h4>Location :- Address</h4><br> Keep close to Nature's heart... and break clear away, once in awhile, and climb a mountain or spend a week in the woods. Wash your spirit clean.\n            <br>\n            <ion-item>\n                <div slot=\"end\">\n                    <ion-button color=\"dark\" fill=\"outline\" expand=\"full\" shape=\"round\">Take me there</ion-button>\n                </div>\n            </ion-item>\n        </ion-card-content>\n    </ion-card>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/resturant-details/resturant-details-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/home/resturant-details/resturant-details-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ResturantDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantDetailsPageRoutingModule", function() { return ResturantDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _resturant_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resturant-details.page */ "./src/app/home/resturant-details/resturant-details.page.ts");




var routes = [
    {
        path: '',
        component: _resturant_details_page__WEBPACK_IMPORTED_MODULE_3__["ResturantDetailsPage"]
    }
];
var ResturantDetailsPageRoutingModule = /** @class */ (function () {
    function ResturantDetailsPageRoutingModule() {
    }
    ResturantDetailsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ResturantDetailsPageRoutingModule);
    return ResturantDetailsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/resturant-details/resturant-details.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/home/resturant-details/resturant-details.module.ts ***!
  \********************************************************************/
/*! exports provided: ResturantDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantDetailsPageModule", function() { return ResturantDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _resturant_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./resturant-details-routing.module */ "./src/app/home/resturant-details/resturant-details-routing.module.ts");
/* harmony import */ var _resturant_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./resturant-details.page */ "./src/app/home/resturant-details/resturant-details.page.ts");







var ResturantDetailsPageModule = /** @class */ (function () {
    function ResturantDetailsPageModule() {
    }
    ResturantDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _resturant_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResturantDetailsPageRoutingModule"]
            ],
            declarations: [_resturant_details_page__WEBPACK_IMPORTED_MODULE_6__["ResturantDetailsPage"]]
        })
    ], ResturantDetailsPageModule);
    return ResturantDetailsPageModule;
}());



/***/ }),

/***/ "./src/app/home/resturant-details/resturant-details.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/home/resturant-details/resturant-details.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".new-background-color {\n  --background: skyblue;\n}\n\ndiv.imageSub {\n  position: relative;\n}\n\ndiv.imageSub img {\n  z-index: 1;\n}\n\ndiv.imageSub div {\n  position: absolute;\n  left: 1%;\n  right: 1%;\n  bottom: 0;\n  padding: 4px;\n  height: 50px;\n  text-align: center;\n  overflow: hidden;\n}\n\ndiv.imageSub div.blackbg {\n  z-index: 2;\n  background-color: #000;\n  -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\";\n  filter: alpha(opacity=50);\n  opacity: 0.5;\n}\n\ndiv.imageSub div.label {\n  z-index: 3;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvaG9tZS9yZXN0dXJhbnQtZGV0YWlscy9yZXN0dXJhbnQtZGV0YWlscy5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvcmVzdHVyYW50LWRldGFpbHMvcmVzdHVyYW50LWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUVBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9ob21lL3Jlc3R1cmFudC1kZXRhaWxzL3Jlc3R1cmFudC1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gICAgLS1iYWNrZ3JvdW5kOiBza3libHVlO1xufVxuXG5kaXYuaW1hZ2VTdWIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuZGl2LmltYWdlU3ViIGltZyB7XG4gICAgei1pbmRleDogMTtcbn1cblxuZGl2LmltYWdlU3ViIGRpdiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDElO1xuICAgIHJpZ2h0OiAxJTtcbiAgICBib3R0b206IDA7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICAvLyBsaW5lLWhlaWdodDogMTZweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuZGl2LmltYWdlU3ViIGRpdi5ibGFja2JnIHtcbiAgICB6LWluZGV4OiAyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gICAgLW1zLWZpbHRlcjogXCJwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuQWxwaGEoT3BhY2l0eT01MClcIjtcbiAgICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9NTApO1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cblxuZGl2LmltYWdlU3ViIGRpdi5sYWJlbCB7XG4gICAgei1pbmRleDogMztcbiAgICBjb2xvcjogd2hpdGU7XG59IiwiLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgLS1iYWNrZ3JvdW5kOiBza3libHVlO1xufVxuXG5kaXYuaW1hZ2VTdWIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmRpdi5pbWFnZVN1YiBpbWcge1xuICB6LWluZGV4OiAxO1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxJTtcbiAgcmlnaHQ6IDElO1xuICBib3R0b206IDA7XG4gIHBhZGRpbmc6IDRweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYuYmxhY2tiZyB7XG4gIHotaW5kZXg6IDI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkFscGhhKE9wYWNpdHk9NTApXCI7XG4gIGZpbHRlcjogYWxwaGEob3BhY2l0eT01MCk7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuZGl2LmltYWdlU3ViIGRpdi5sYWJlbCB7XG4gIHotaW5kZXg6IDM7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/resturant-details/resturant-details.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/home/resturant-details/resturant-details.page.ts ***!
  \******************************************************************/
/*! exports provided: ResturantDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantDetailsPage", function() { return ResturantDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var ResturantDetailsPage = /** @class */ (function () {
    function ResturantDetailsPage(nav) {
        this.nav = nav;
    }
    ResturantDetailsPage.prototype.ngOnInit = function () {
    };
    ResturantDetailsPage.prototype.back_trending = function () {
        this.nav.navigateForward('home/tabs/trending');
    };
    ResturantDetailsPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
    ]; };
    ResturantDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-resturant-details',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./resturant-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-details/resturant-details.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./resturant-details.page.scss */ "./src/app/home/resturant-details/resturant-details.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], ResturantDetailsPage);
    return ResturantDetailsPage;
}());



/***/ })

}]);
//# sourceMappingURL=resturant-details-resturant-details-module.js.map