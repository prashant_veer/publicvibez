(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["otpcheck-otpcheck-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n\n    <div class=\"verifyotp\">\n        <a href=\"\" class=\"round-button-create-profile gradient-background\" style=\"margin-top: 20%;\">\n            <ion-icon name=\"person-outline\" style=\"font-size: 50px;margin-top: 10%;\"></ion-icon>\n        </a>\n        <br>\n        <h2><span>CREATE <b>PROFILE</b></span></h2>\n        <h5 style=\"color: #ffffff;\">Please check your mobile or email, you got the passcode <br> Verify Your mobile number and email id.\n        </h5>\n\n\n        <ion-grid>\n            <ion-row>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp1\" pattern=\"[0-9]{6}\" #otp1 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp2)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp2\" pattern=\"[0-9]{6}\" #otp2 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp3)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp3\" pattern=\"[0-9]{6}\" #otp3 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp4)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp4\" pattern=\"[0-9]{6}\" #otp4 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp5)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp5\" pattern=\"[0-9]{6}\" #otp5 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp6)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp6\" pattern=\"[0-9]{6}\" #otp6 class=\"ion-text-center ion-input-class\" maxlength=\"1\"></ion-input>\n                </ion-col>\n\n            </ion-row>\n        </ion-grid>\n\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_getstart()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n        <!-- <ion-grid>\n            <ion-row ion-text-center>\n                <ion-col size=\"6\">\n                    <ion-button expand=\"block\" color=\"danger\" (click)='back_passcode()'>Back</ion-button>\n                </ion-col>\n                <ion-col size=\"6\">\n                    <ion-button expand=\"block\" color=\"primary\" (click)='confirm_passcode()'>Save</ion-button>\n                </ion-col>\n\n            </ion-row>\n\n        </ion-grid> -->\n\n    </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/services/loading.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/loading.service.ts ***!
  \*********************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var LoadingService = /** @class */ (function () {
    function LoadingService(loadingController) {
        this.loadingController = loadingController;
        this.isLoading = false;
    }
    LoadingService.prototype.present = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingController.create({
                                duration: 5000,
                                spinner: 'circles',
                            }).then(function (a) {
                                a.present().then(function () {
                                    // console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoadingService.prototype.dismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingController.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoadingService.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
    ]; };
    LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoadingService);
    return LoadingService;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck-routing.module.ts ***!
  \************************************************************/
/*! exports provided: OtpcheckPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPageRoutingModule", function() { return OtpcheckPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _otpcheck_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otpcheck.page */ "./src/app/signup/otpcheck/otpcheck.page.ts");




var routes = [
    {
        path: '',
        component: _otpcheck_page__WEBPACK_IMPORTED_MODULE_3__["OtpcheckPage"]
    }
];
var OtpcheckPageRoutingModule = /** @class */ (function () {
    function OtpcheckPageRoutingModule() {
    }
    OtpcheckPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], OtpcheckPageRoutingModule);
    return OtpcheckPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.module.ts":
/*!****************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.module.ts ***!
  \****************************************************/
/*! exports provided: OtpcheckPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPageModule", function() { return OtpcheckPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _otpcheck_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otpcheck-routing.module */ "./src/app/signup/otpcheck/otpcheck-routing.module.ts");
/* harmony import */ var _otpcheck_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./otpcheck.page */ "./src/app/signup/otpcheck/otpcheck.page.ts");







var OtpcheckPageModule = /** @class */ (function () {
    function OtpcheckPageModule() {
    }
    OtpcheckPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _otpcheck_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtpcheckPageRoutingModule"]
            ],
            declarations: [_otpcheck_page__WEBPACK_IMPORTED_MODULE_6__["OtpcheckPage"]]
        })
    ], OtpcheckPageModule);
    return OtpcheckPageModule;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.page.scss":
/*!****************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".verifyotp {\n  margin: 40px 20px;\n}\n\n.verifyotp h5 {\n  text-align: center;\n}\n\n.verifyotp p {\n  text-align: center;\n  font-size: 12px;\n}\n\n.verifyotpcol {\n  border-radius: 40px;\n  margin: 2px 2px 20px 2px;\n}\n\n.btn {\n  --background: rgb(212, 9, 9);\n  border-radius: 5px;\n  --color: #fff;\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n}\n\n.btn1 {\n  border: 2px solid #f1c732;\n  border-radius: 5px;\n  color: #d40909;\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n  margin-top: 10px;\n}\n\n.btn2 {\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n  margin-top: 10px;\n  --background: rgb(241, 199, 50);\n  color: #d40909;\n}\n\n#time {\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL290cGNoZWNrL290cGNoZWNrLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2lnbnVwL290cGNoZWNrL290cGNoZWNrLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFFSSxtQkFBQTtFQUNBLHdCQUFBO0FDQUo7O0FER0E7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDQUo7O0FER0E7RUFFSSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNESjs7QURJQTtFQUNJLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtFQUNBLGNBQUE7QUNESjs7QURJQTtFQUNJLGVBQUE7QUNESiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9vdHBjaGVjay9vdHBjaGVjay5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudmVyaWZ5b3RwIHtcbiAgICBtYXJnaW46IDQwcHggMjBweDtcbn1cblxuLnZlcmlmeW90cCBoNSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udmVyaWZ5b3RwIHAge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi52ZXJpZnlvdHBjb2wge1xuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIHJnYigyMTIsIDksIDkpO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgbWFyZ2luOiAycHggMnB4IDIwcHggMnB4O1xufVxuXG4uYnRuIHtcbiAgICAtLWJhY2tncm91bmQ6IHJnYigyMTIsIDksIDkpO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWNvbG9yOiAjZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5idG4xIHtcbiAgICAvLyAtLWJhY2tncm91bmQ6IHJnYigyMTIsIDksIDkpO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYigyNDEsIDE5OSwgNTApO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogcmdiKDIxMiwgOSwgOSk7XG4gICAgZm9udC1mYW1pbHk6IEhlbHZldGljYTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uYnRuMiB7XG4gICAgZm9udC1mYW1pbHk6IEhlbHZldGljYTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIC0tYmFja2dyb3VuZDogcmdiKDI0MSwgMTk5LCA1MCk7XG4gICAgY29sb3I6IHJnYigyMTIsIDksIDkpO1xufVxuXG4jdGltZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufSIsIi52ZXJpZnlvdHAge1xuICBtYXJnaW46IDQwcHggMjBweDtcbn1cblxuLnZlcmlmeW90cCBoNSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnZlcmlmeW90cCBwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi52ZXJpZnlvdHBjb2wge1xuICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICBtYXJnaW46IDJweCAycHggMjBweCAycHg7XG59XG5cbi5idG4ge1xuICAtLWJhY2tncm91bmQ6IHJnYigyMTIsIDksIDkpO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIC0tY29sb3I6ICNmZmY7XG4gIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmJ0bjEge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZjFjNzMyO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGNvbG9yOiAjZDQwOTA5O1xuICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5idG4yIHtcbiAgZm9udC1mYW1pbHk6IEhlbHZldGljYTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICAtLWJhY2tncm91bmQ6IHJnYigyNDEsIDE5OSwgNTApO1xuICBjb2xvcjogI2Q0MDkwOTtcbn1cblxuI3RpbWUge1xuICBmb250LXNpemU6IDE0cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.page.ts":
/*!**************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.page.ts ***!
  \**************************************************/
/*! exports provided: OtpcheckPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPage", function() { return OtpcheckPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/loading.service */ "./src/app/services/loading.service.ts");
/* harmony import */ var _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/present-toast.service */ "./src/app/services/present-toast.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");






var OtpcheckPage = /** @class */ (function () {
    function OtpcheckPage(router, activatedroute, nav, presentToast, loadingservice) {
        this.router = router;
        this.activatedroute = activatedroute;
        this.nav = nav;
        this.presentToast = presentToast;
        this.loadingservice = loadingservice;
    }
    OtpcheckPage.prototype.next = function (el) {
        el.setFocus();
    };
    OtpcheckPage.prototype.ngOnInit = function () {
        // this.myotp1.focus();
        this.smobilenumber = window.localStorage.getItem('mobile');
        this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
        this.userNickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.userPass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.newotp = this.activatedroute.snapshot.paramMap.get('otp');
    };
    OtpcheckPage.prototype.goto_getstart = function () {
        this.confirmOtp = this.myotp1 + '' + this.myotp2 + '' + this.myotp3 + '' + this.myotp4 + '' + this.myotp5 + '' + this.myotp6;
        if (this.confirmOtp !== this.newotp) {
            this.presentToast.presentToast('Please enter correct passcode');
            this.myotp1 = '';
            this.myotp2 = '';
            this.myotp3 = '';
            this.myotp4 = '';
            this.myotp5 = '';
            this.myotp6 = '';
        }
        else {
            this.nav.navigateForward(['signup/profile-location', {
                    email: this.userEmail,
                    NickName: this.userNickName,
                    mobile: this.smobilenumber,
                    Pass: this.userPass
                }]);
        }
    };
    OtpcheckPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__["PresentToastService"] },
        { type: _services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"] }
    ]; };
    OtpcheckPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-otpcheck',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./otpcheck.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./otpcheck.page.scss */ "./src/app/signup/otpcheck/otpcheck.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
            _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__["PresentToastService"], _services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"]])
    ], OtpcheckPage);
    return OtpcheckPage;
}());



/***/ })

}]);
//# sourceMappingURL=otpcheck-otpcheck-module.js.map