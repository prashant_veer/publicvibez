(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registeraboutyou-registeraboutyou-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"other\">\n        <div class=\"mainDiv uploadInfo\">\n            <div class=\"tableCell\">\n                <h2><span>Upload <b>Information</b></span></h2>\n                <div class=\"form-div\">\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Gender</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"preferredGender\" class=\"myselect\" (ionChange)=\"genderValue($event)\" [selectedText]=\"genderSelectedtext\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">BodyType</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userbodytype\" class=\"myselect\" (ionChange)=\"bodytypeValue($event)\" [selectedText]=\"bodytypetext\">\n                                            <ion-select-option *ngFor=\"let bodyt of bodytypearray\">{{bodyt.bodytype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n \n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Height</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userheight\" class=\"myselect\" (ionChange)=\"heightValue($event)\" [selectedText]=\"userheighttext\">\n                                            <ion-select-option *ngFor=\"let ht of userheightarray\">{{ht.height}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Child</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userchild\" class=\"myselect\" (ionChange)=\"childValue($event)\" [selectedText]=\"userchildtext\">\n                                            <ion-select-option *ngFor=\"let ch of userchildarray\">{{ch.child}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Smoke</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"usersmoke\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" [selectedText]=\"usersmoketext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Drinks</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userdrinks\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" [selectedText]=\"userdrinkstext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Zodiac</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userzodiac\" class=\"myselect\" [selectedText]=\"zodiactext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Race</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userRace\" placeholder=\"Select Race\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Prefecen</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userPreference\" placeholder=\"Select Prefecen\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"ion-textarea-Box\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"12\">\n                                        <ion-textarea [(ngModel)]=\"userAboutme\" placeholder=\"About Me\" class=\"box-label label-padding\"></ion-textarea>\n                                    </ion-col>\n\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n                    <ion-grid>\n                        <ion-row class=\"ion-text-center\">\n                            <ion-col>\n                                <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_registerfinish()\">\n                                    <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                                </a>\n\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: RegisteraboutyouPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPageRoutingModule", function() { return RegisteraboutyouPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registeraboutyou.page */ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts");




var routes = [
    {
        path: '',
        component: _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_3__["RegisteraboutyouPage"]
    }
];
var RegisteraboutyouPageRoutingModule = /** @class */ (function () {
    function RegisteraboutyouPageRoutingModule() {
    }
    RegisteraboutyouPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegisteraboutyouPageRoutingModule);
    return RegisteraboutyouPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.module.ts ***!
  \********************************************************************/
/*! exports provided: RegisteraboutyouPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPageModule", function() { return RegisteraboutyouPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registeraboutyou_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registeraboutyou-routing.module */ "./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts");
/* harmony import */ var _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registeraboutyou.page */ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts");







var RegisteraboutyouPageModule = /** @class */ (function () {
    function RegisteraboutyouPageModule() {
    }
    RegisteraboutyouPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _registeraboutyou_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisteraboutyouPageRoutingModule"]
            ],
            declarations: [_registeraboutyou_page__WEBPACK_IMPORTED_MODULE_6__["RegisteraboutyouPage"]]
        })
    ], RegisteraboutyouPageModule);
    return RegisteraboutyouPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.box-left {\n  background: #37A4DC;\n  padding-right: -10px;\n}\n\n.box-label {\n  background: #CCD1D1;\n  font-size: small;\n}\n\n.label-padding {\n  padding-left: 15px;\n}\n\n.myselect-label {\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyYWJvdXR5b3UvcmVnaXN0ZXJhYm91dHlvdS5wYWdlLnNjc3MiLCJzcmMvYXBwL3NpZ251cC9yZWdpc3RlcmFib3V0eW91L3JlZ2lzdGVyYWJvdXR5b3UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFDO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDQ0w7O0FERUM7RUFDSSxtQkFBQTtFQUNBLG9CQUFBO0FDQ0w7O0FERUM7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0FDQ0w7O0FERUM7RUFDSSxrQkFBQTtBQ0NMOztBREVDO0VBS0ksZ0JBQUE7QUNITCIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9yZWdpc3RlcmFib3V0eW91L3JlZ2lzdGVyYWJvdXR5b3UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC5teXNlbGVjdCB7XG4gICAgIGhlaWdodDogNTBweDtcbiAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgd2lkdGg6IDEwMCU7XG4gICAgIGJvcmRlcjogbm9uZTtcbiAgICAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgei1pbmRleDogMTtcbiAgICAgZm9udC1zaXplOiAxNHB4O1xuIH1cbiBcbiAuYm94LWxlZnQge1xuICAgICBiYWNrZ3JvdW5kOiAjMzdBNERDO1xuICAgICBwYWRkaW5nLXJpZ2h0OiAtMTBweDtcbiB9XG4gXG4gLmJveC1sYWJlbCB7XG4gICAgIGJhY2tncm91bmQ6ICNDQ0QxRDE7XG4gICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gfVxuIFxuIC5sYWJlbC1wYWRkaW5nIHtcbiAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuIH1cbiBcbiAubXlzZWxlY3QtbGFiZWwge1xuICAgICAvLyAgd2lkdGg6IDEwMCU7XG4gICAgIC8vICBoZWlnaHQ6IDQwcHg7XG4gICAgIC8vICBwYWRkaW5nOiAxOHB4IDE3cHggMThweCAzMHB4O1xuICAgICAvLyAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgICAgZm9udC1zaXplOiBzbWFsbDtcbiB9IiwiLm15c2VsZWN0IHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBvdXRsaW5lOiBub25lO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBwYWRkaW5nOiAwIDE1cHggMCAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3gtbGVmdCB7XG4gIGJhY2tncm91bmQ6ICMzN0E0REM7XG4gIHBhZGRpbmctcmlnaHQ6IC0xMHB4O1xufVxuXG4uYm94LWxhYmVsIHtcbiAgYmFja2dyb3VuZDogI0NDRDFEMTtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmxhYmVsLXBhZGRpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG5cbi5teXNlbGVjdC1sYWJlbCB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.page.ts ***!
  \******************************************************************/
/*! exports provided: RegisteraboutyouPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPage", function() { return RegisteraboutyouPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var RegisteraboutyouPage = /** @class */ (function () {
    function RegisteraboutyouPage(general, toastCtrl, router, activatedroute, authSvc) {
        this.general = general;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.activatedroute = activatedroute;
        this.authSvc = authSvc;
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userchildarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.genderprefer = [];
        this.userDrinksarray = [];
    }
    RegisteraboutyouPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.username = this.activatedroute.snapshot.paramMap.get('username');
        this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
        this.genderSelectedtext = 'Gender';
        this.bodytypetext = 'What\'s your body type?';
        this.userheighttext = 'What\'s your height?';
        this.userchildtext = 'Do you have children?';
        this.usersmoketext = 'do you smoke?';
        this.userdrinkstext = 'do you drink?';
        this.zodiactext = 'zodiac';
        this.showgender();
        this.showbodytype();
        this.showchild();
        this.showheight();
        this.showsmoke();
    };
    RegisteraboutyouPage.prototype.showbodytype = function () {
        this.bodytypearray = [{ bodytype: 'Muscles' },
            { bodytype: 'Athletic' },
            { bodytype: 'Curvy' },
            { bodytype: 'Overweight' },
            { bodytype: 'Underweight' }
        ];
    };
    RegisteraboutyouPage.prototype.bodytypeValue = function (event) {
        console.log(event.detail.value);
        this.bodytypetext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showgender = function () {
        this.genderprefer = [{ gendertype: 'Male' },
            { gendertype: 'Female' }
        ];
    };
    RegisteraboutyouPage.prototype.genderValue = function (event) {
        console.log(event.detail.value);
        this.genderSelectedtext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showheight = function () {
        this.userheightarray = [{ height: '<5 Foot' },
            { height: '>5 Foot' },
            { height: '>6 Foot' },
            { height: '<6 Foot' }
        ];
    };
    RegisteraboutyouPage.prototype.heightValue = function (event) {
        console.log(event.detail.value);
        this.userheighttext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showchild = function () {
        this.userchildarray = [{ child: 'YES' },
            { child: 'NO' }];
    };
    RegisteraboutyouPage.prototype.childValue = function (event) {
        console.log(event.detail.value);
        this.userchildtext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showsmoke = function () {
        this.usersmokearray = [{ smoke: 'YES' },
            { smoke: 'NO' }
        ];
    };
    RegisteraboutyouPage.prototype.smokeValue = function (event) {
        console.log(event.detail.value);
        this.usersmoketext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.goto_registerfinish = function () {
        if (this.bodytypetext === 'What\'s your body type?') {
            this.general.presentToast('Please select your body type.');
        }
        else if (this.userheighttext === 'What\'s your height?') {
            this.general.presentToast('Please select height.');
        }
        else if (this.userchildtext === 'Do you have children?') {
            this.general.presentToast('Please select details about children.');
        }
        else if (this.usersmoketext === 'do you smoke?') {
            this.general.presentToast('Please select details about smoking.');
        }
        else {
            // this.general.presentLoading("Please wait... ");
            this.router.navigate(['signup/registersummary', {
                    "NickName": this.NickName, "email": this.email, "userMobile": this.userMobile, "Pass": this.Pass,
                    "usercity": this.usercity, "userArea": this.userArea, "userPincode": this.userPincode, "userState": this.userState,
                    "username": this.username, "userBirthday": this.userBirthday, "userPhoto": this.userPhoto,
                    "preferredGender": this.preferredGender, "userbodytype": this.userbodytype, "userheight": this.userheight, "userchild": this.userchild,
                    "usersmoke": this.usersmoke, "userdrinks": this.userdrinks, "userzodiac": this.userzodiac, "userRace": this.userRace, "userPreference": this.userPreference, "userAboutme": this.userAboutme,
                    "latitude": this.latitude, "longitude": this.longitude
                }]);
            // this.authSvc.register( this.NickName, this.email, this.userMobile, this.Pass,
            //   this.usercity, this.userArea, this.userPincode,this.userState,
            //   this.username, this.userBirthday, this.userPhoto,
            //   this.preferredGender, this.userbodytype,this.userheight,this.userchild,
            //   this.usersmoke,this.userdrinks,this.userzodiac,this.userRace,this.userPreference,this.userAboutme,
            //   this.latitude, this.longitude).then(async req => {
            //     this.general.closePresentLoading();
            //     console.log(req.data);
            //     var v = JSON.parse(req.data);
            //     console.log(v);
            //     if (v.success) {
            //       this.router.navigate(['signup/registerfinished', {
            //         "NickName":this.NickName, "email":this.email, "userMobile":this.userMobile, "Pass":this.Pass,
            //         "usercity":this.usercity, "userArea":this.userArea, "userPincode":this.userPincode,"userState":this.userState,
            //         "username":this.username, "userBirthday":this.userBirthday, "userPhoto":this.userPhoto,
            //         "preferredGender":this.preferredGender, "userbodytype":this.userbodytype,"userheight":this.userheight,
            // "userchild":this.userchild ,
            //         "usersmoke":this.usersmoke,"userdrinks":this.userdrinks,"userzodiac":this.userzodiac,"userRace":this.userRace,
            // "userPreference":this.userPreference,"userAboutme":this.userAboutme,
            //         "latitude":this.latitude, "longitude":this.longitude
            //       }]);
            //     } else {
            //       this.errorToast(v.message);
            //     }
            //   });
        }
    };
    RegisteraboutyouPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisteraboutyouPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] }
    ]; };
    RegisteraboutyouPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registeraboutyou',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registeraboutyou.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registeraboutyou.page.scss */ "./src/app/signup/registeraboutyou/registeraboutyou.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], RegisteraboutyouPage);
    return RegisteraboutyouPage;
}());



/***/ })

}]);
//# sourceMappingURL=registeraboutyou-registeraboutyou-module.js.map