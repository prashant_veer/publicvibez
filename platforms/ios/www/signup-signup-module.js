(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signup-signup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n\n    <a href=\"\" class=\"round-button-create-profile gradient-background\" style=\"margin-top: 20%;\">\n        <ion-icon name=\"person-outline\" style=\"font-size: 50px;margin-top: 10%;\"></ion-icon>\n    </a>\n    <br>\n    <h2><span>CREATE <b>PROFILE</b></span></h2>\n    <div class=\"form-div\">\n        <div class=\"formElement\">\n            <ion-input type=\"text\" [(ngModel)]=\"userNickName\" class=\"general-input ion-input-class\" placeholder=\"Your nickname\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"email\" [(ngModel)]=\"userEmail\" class=\"general-input ion-input-class\" placeholder=\"Your Email\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"tel\" [(ngModel)]=\"userMobile\" class=\"general-input ion-input-class\" placeholder=\"Your Mobile Number\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"password\" [(ngModel)]=\"userPass\" class=\"general-input ion-input-class\" placeholder=\"Your password\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n\n\n            <ion-checkbox slot=\"start\" color=\"light\" checked=\"true\">\n            </ion-checkbox>\n            <ion-label color=\"light\"> Let App know location to continue</ion-label>\n\n        </div>\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_getstart()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n\n    </div>\n\n    <!--\n          <div class=\"wrapper\">\n      <div class=\"loginForm\">\n          <div class=\"tableCell\">\n              <h1>Sign Up</h1>\n              <p class=\"headDesc\" style=\"text-align: center;\">Sign up to see who's new in your area!</p>\n              <div class=\"formElement\">\n                  <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"userName\" placeholder=\"User Name\"></ion-input>\n              </div>\n              <div class=\"formElement\">\n                  <ion-input type=\"email\" class=\"myselect\" [(ngModel)]=\"userEmail\" placeholder=\"EMAIL ADDRESS\"></ion-input>\n              </div>\n\n              <div class=\"formElement\">\n                  <ion-button color=\"tertiary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"goto_getstart()\">SIGN UP</ion-button>\n              </div>\n\n              <p class=\"or\">or login with</p>\n              <div class=\"socialLogin\">\n                  <img src=\"assets/images/facebook.svg\" (click)=\"goto_Facebook()\" alt=\"Facebook\" title=\"Facebook\">\n                  <img src=\"assets/images/google-plus.svg\" (click)=\"goto_google()\" alt=\"Facebook\" title=\"Facebook\">\n              </div>\n          </div>\n          <p class=\"note\">*By selecting \"Log in with Facebook\" or \"Log in with Google\", you agree to our <b>Terms of User</b><br>(including the mandatory arbitration of bispute)</p>\n      </div>\n  </div>\n\n    -->\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/signup-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/signup/signup-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageRoutingModule", function() { return SignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup.page */ "./src/app/signup/signup.page.ts");




var routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_3__["SignupPage"]
    },
    {
        path: 'gettingstart',
        loadChildren: function () { return __webpack_require__.e(/*! import() | gettingstart-gettingstart-module */ "gettingstart-gettingstart-module").then(__webpack_require__.bind(null, /*! ./gettingstart/gettingstart.module */ "./src/app/signup/gettingstart/gettingstart.module.ts")).then(function (m) { return m.GettingstartPageModule; }); }
    },
    {
        path: 'registerphoto',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registerphoto-registerphoto-module */ "registerphoto-registerphoto-module").then(__webpack_require__.bind(null, /*! ./registerphoto/registerphoto.module */ "./src/app/signup/registerphoto/registerphoto.module.ts")).then(function (m) { return m.RegisterphotoPageModule; }); }
    },
    {
        path: 'registeraboutyou',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registeraboutyou-registeraboutyou-module */ "registeraboutyou-registeraboutyou-module").then(__webpack_require__.bind(null, /*! ./registeraboutyou/registeraboutyou.module */ "./src/app/signup/registeraboutyou/registeraboutyou.module.ts")).then(function (m) { return m.RegisteraboutyouPageModule; }); }
    },
    {
        path: 'registerfinished',
        loadChildren: function () { return Promise.all(/*! import() | registerfinished-registerfinished-module */[__webpack_require__.e("common"), __webpack_require__.e("registerfinished-registerfinished-module")]).then(__webpack_require__.bind(null, /*! ./registerfinished/registerfinished.module */ "./src/app/signup/registerfinished/registerfinished.module.ts")).then(function (m) { return m.RegisterfinishedPageModule; }); }
    },
    {
        path: 'otpcheck',
        loadChildren: function () { return Promise.all(/*! import() | otpcheck-otpcheck-module */[__webpack_require__.e("common"), __webpack_require__.e("otpcheck-otpcheck-module")]).then(__webpack_require__.bind(null, /*! ./otpcheck/otpcheck.module */ "./src/app/signup/otpcheck/otpcheck.module.ts")).then(function (m) { return m.OtpcheckPageModule; }); }
    },
    {
        path: 'profile-location',
        loadChildren: function () { return __webpack_require__.e(/*! import() | profile-location-profile-location-module */ "profile-location-profile-location-module").then(__webpack_require__.bind(null, /*! ./profile-location/profile-location.module */ "./src/app/signup/profile-location/profile-location.module.ts")).then(function (m) { return m.ProfileLocationPageModule; }); }
    },
    {
        path: 'user-profile',
        loadChildren: function () { return Promise.all(/*! import() | user-profile-user-profile-module */[__webpack_require__.e("common"), __webpack_require__.e("user-profile-user-profile-module")]).then(__webpack_require__.bind(null, /*! ./user-profile/user-profile.module */ "./src/app/signup/user-profile/user-profile.module.ts")).then(function (m) { return m.UserProfilePageModule; }); }
    },
    {
        path: 'registersummary',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registersummary-registersummary-module */ "registersummary-registersummary-module").then(__webpack_require__.bind(null, /*! ./registersummary/registersummary.module */ "./src/app/signup/registersummary/registersummary.module.ts")).then(function (m) { return m.RegistersummaryPageModule; }); }
    }
];
var SignupPageRoutingModule = /** @class */ (function () {
    function SignupPageRoutingModule() {
    }
    SignupPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], SignupPageRoutingModule);
    return SignupPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.module.ts":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.module.ts ***!
  \*****************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup-routing.module */ "./src/app/signup/signup-routing.module.ts");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "./src/app/signup/signup.page.ts");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");









var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignupPageRoutingModule"]
            ],
            declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]],
            providers: [
                _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__["Facebook"]
            ]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.page.scss":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 40px;\n  width: 100%;\n  border: solid 1px #981F40;\n  text-align: center;\n  outline: none;\n  text-transform: uppercase;\n  padding-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6L3NyYy9hcHAvc2lnbnVwL3NpZ251cC5wYWdlLnNjc3MiLCJzcmMvYXBwL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15c2VsZWN0IHtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiBzb2xpZCAxcHggIzk4MUY0MDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xufSIsIi5teXNlbGVjdCB7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogc29saWQgMXB4ICM5ODFGNDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgb3V0bGluZTogbm9uZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/signup.page.ts":
/*!***************************************!*\
  !*** ./src/app/signup/signup.page.ts ***!
  \***************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/login.service */ "./src/app/services/login.service.ts");








var SignupPage = /** @class */ (function () {
    function SignupPage(toastCtrl, authSvc, nav, googlePlus, fb, gen) {
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this.nav = nav;
        this.googlePlus = googlePlus;
        this.fb = fb;
        this.gen = gen;
        this.emailpattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
        this.mobilePattern = '/^([+]\d{2})?\d{10}$/';
    }
    SignupPage.prototype.ngOnInit = function () {
    };
    SignupPage.prototype.goto_getstart = function () {
        var _this = this;
        if (this.userNickName === undefined || this.userNickName === '') {
            this.gen.presentToast('Please enter Nickname');
        }
        else if (this.userEmail === undefined || this.userEmail === '') {
            this.gen.presentToast('Please enter Email id');
        }
        else if (!this.userEmail.match(this.emailpattern)) {
            this.gen.presentToast('Please enter proper Email id');
        }
        else if (this.userMobile === undefined || this.userMobile === '') {
            this.gen.presentToast('Please enter mobile number with country code.');
        }
        else if (this.userPass === undefined || this.userPass === '') {
            this.gen.presentToast('Please enter password');
        }
        else {
            this.gen.presentLoading("Please wait... ");
            this.authSvc.sendotptoEmail(this.userEmail).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.gen.closePresentLoading();
                    v = JSON.parse(req.data);
                    if (v.success) {
                        this.nav.navigateForward(['signup/otpcheck', {
                                email: this.userEmail,
                                NickName: this.userNickName,
                                mobile: this.userMobile,
                                Pass: this.userPass,
                                otp: v.passcode
                            }]);
                    }
                    else {
                        this.errorToast(v.message);
                    }
                    return [2 /*return*/];
                });
            }); });
        }
    };
    SignupPage.prototype.goto_google = function () {
        var _this = this;
        this.googlePlus.login({})
            .then(function (res) {
            console.log(res),
                _this.nav.navigateForward(['signup/gettingstart', {
                        email: res.email, name: res.displayName,
                        googleID: res.userId, facebookId: ""
                    }]);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    SignupPage.prototype.goto_Facebook = function () {
        var _this = this;
        this.fb.getLoginStatus().then(function (res) {
            console.log(res);
            if (res.status === 'connected') {
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.fb.login(['email'])
                    .then(function (res) {
                    if (res.status === 'connected') {
                        _this.getUserDetail(res.authResponse.userID);
                    }
                    else {
                    }
                })
                    .catch(function (e) { return console.log('Error logging into Facebook', e); });
            }
        });
    };
    SignupPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api('/' + userid + '/?fields=id,email,name', ['public_profile'])
            .then(function (res) {
            console.log(res);
            _this.nav.navigateForward(['signup/gettingstart', {
                    name: res.name, email: res.email,
                    googleID: "", facebookId: res.id
                }]);
            //this.users = res;
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    SignupPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    SignupPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__["GooglePlus"] },
        { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__["Facebook"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_4__["GeneralService"] }
    ]; };
    SignupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signup.page.scss */ "./src/app/signup/signup.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__["Facebook"], _services_general__WEBPACK_IMPORTED_MODULE_4__["GeneralService"]])
    ], SignupPage);
    return SignupPage;
}());



/***/ })

}]);
//# sourceMappingURL=signup-signup-module.js.map