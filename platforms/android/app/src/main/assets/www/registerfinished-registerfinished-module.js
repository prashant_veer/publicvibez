(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registerfinished-registerfinished-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modalwin/modalwin.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modalwin/modalwin.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header no-border no-shadow>\n    <ion-toolbar color=\"light\">\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-circle-outline\" color=\"dark\" (click)=\"close()\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header> -->\n<ion-content>\n    <div class=\"backMain\">\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon name=\"close-circle-outline\" color=\"dark\" (click)=\"close()\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </div>\n\n    <div class=\"container\">\n\n        You are now eligible for <br/><br/> half off the first 2 months <br/><br/> when upgrading to <br/><br/>\n        <strong>Vibez Premium</strong> just by <br/> <br/>Pre Registering with us.<br/><br/>\n\n        <ion-button (click)=\"close()\" color=\"light\" style=\"border:3px solid #000000; border-radius: 10px;\">Close</ion-button>\n\n\n\n    </div>\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content force-overscroll=\"false\">\n\n\n    <div class=\"page-centered-element\">\n        <div class=\"content\" style=\"font-family:'CaviarDreams'; \">\n\n            <img class=\"logo\" src=\"assets/images/logo.png\">\n            <ion-icon name=\"information-circle-outline\" (click)=\"presentPopover($event)\" class=\"info-icon\"></ion-icon>\n            <p class=\"bottom_para\"><i>Next big social dating app</i></p>\n            <ion-row>\n                <ion-col>\n                    <div class=\"subscribed_user\">\n                        <span style=\"font-size: 45px;\">{{mycount}}</span><br/>\n                        <i>users so far</i>\n                    </div>\n                </ion-col>\n            </ion-row>\n\n            <ion-row>\n                <ion-col>\n                    <div class=\"reach_target\">\n                        <i>help us to reach</i><span style=\"font-size: 30px;\"> 50,000</span> <br/><i> users to go live</i>\n\n                    </div>\n\n                    <div class=\"hr_mydiv\">\n\n                        <hr style=\"border:1px solid #ffffff; width: 30%; margin: auto;  \">\n                    </div>\n                </ion-col>\n            </ion-row>\n\n\n\n\n            <div class=\"its_dating_div\">\n                ITS NOT DATING <br/> ITS VIBING\n            </div>\n            <div class=\"bottom_footer\">\n                Follow us to stay connected @publicvibez\n            </div>\n        </div>\n\n    </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/modalwin/modalwin.component.scss":
/*!**************************************************!*\
  !*** ./src/app/modalwin/modalwin.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #FBFCFC;\n}\n\n.backMain {\n  height: 50px;\n  color: #000;\n  font-size: 30px;\n  line-height: 50px;\n  text-align: right;\n  z-index: 1;\n  float: right;\n  margin-top: 10px;\n}\n\n.container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 55%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n\n.container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n.container p {\n  font-size: 16px;\n  line-height: 19px;\n  color: #000000;\n  margin: 0;\n}\n\n.container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL21vZGFsd2luL21vZGFsd2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2RhbHdpbi9tb2RhbHdpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7QUNDSjs7QURFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7QUNDSjs7QURFQTtFQUNJLHFCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9tb2RhbHdpbi9tb2RhbHdpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6ICNGQkZDRkM7XG59XG5cbi5iYWNrTWFpbiB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB6LWluZGV4OiAxO1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uY29udGFpbmVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiA1NSU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4uY29udGFpbmVyIHN0cm9uZyB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4uY29udGFpbmVyIHAge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMTlweDtcbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5jb250YWluZXIgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufSIsImlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRkJGQ0ZDO1xufVxuXG4uYmFja01haW4ge1xuICBoZWlnaHQ6IDUwcHg7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDMwcHg7XG4gIGxpbmUtaGVpZ2h0OiA1MHB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgei1pbmRleDogMTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDU1JTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4uY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbi5jb250YWluZXIgcCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBtYXJnaW46IDA7XG59XG5cbi5jb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/modalwin/modalwin.component.ts":
/*!************************************************!*\
  !*** ./src/app/modalwin/modalwin.component.ts ***!
  \************************************************/
/*! exports provided: ModalwinComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalwinComponent", function() { return ModalwinComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var ModalwinComponent = /** @class */ (function () {
    function ModalwinComponent(modalController, navParams, popoverController) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.popoverController = popoverController;
    }
    ModalwinComponent.prototype.ngOnInit = function () { };
    ModalwinComponent.prototype.close = function () {
        this.modalController.dismiss({
            'dismissed': true
        });
        // this.popoverController.dismiss();
        // const onClosedData = 'Wrapped Up!';
        // await this.modalController.dismiss(onClosedData);
    };
    ModalwinComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] }
    ]; };
    ModalwinComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modalwin',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./modalwin.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modalwin/modalwin.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./modalwin.component.scss */ "./src/app/modalwin/modalwin.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]])
    ], ModalwinComponent);
    return ModalwinComponent;
}());



/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: RegisterfinishedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPageRoutingModule", function() { return RegisterfinishedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registerfinished_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registerfinished.page */ "./src/app/signup/registerfinished/registerfinished.page.ts");




var routes = [
    {
        path: '',
        component: _registerfinished_page__WEBPACK_IMPORTED_MODULE_3__["RegisterfinishedPage"]
    }
];
var RegisterfinishedPageRoutingModule = /** @class */ (function () {
    function RegisterfinishedPageRoutingModule() {
    }
    RegisterfinishedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegisterfinishedPageRoutingModule);
    return RegisterfinishedPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.module.ts ***!
  \********************************************************************/
/*! exports provided: RegisterfinishedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPageModule", function() { return RegisterfinishedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _modalwin_modalwin_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../modalwin/modalwin.component */ "./src/app/modalwin/modalwin.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registerfinished_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registerfinished-routing.module */ "./src/app/signup/registerfinished/registerfinished-routing.module.ts");
/* harmony import */ var _registerfinished_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./registerfinished.page */ "./src/app/signup/registerfinished/registerfinished.page.ts");






// import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


var RegisterfinishedPageModule = /** @class */ (function () {
    function RegisterfinishedPageModule() {
    }
    RegisterfinishedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _registerfinished_routing_module__WEBPACK_IMPORTED_MODULE_6__["RegisterfinishedPageRoutingModule"]
            ], providers: [],
            entryComponents: [_modalwin_modalwin_component__WEBPACK_IMPORTED_MODULE_1__["ModalwinComponent"]],
            declarations: [_registerfinished_page__WEBPACK_IMPORTED_MODULE_7__["RegisterfinishedPage"], _modalwin_modalwin_component__WEBPACK_IMPORTED_MODULE_1__["ModalwinComponent"]],
        })
    ], RegisterfinishedPageModule);
    return RegisterfinishedPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".logo {\n  max-width: 40%;\n  margin-top: 10px;\n}\n\n.logoTapCls {\n  text-align: center;\n  margin-top: 1%;\n}\n\n.bottom_para {\n  font-size: 11px;\n  margin-top: 10px;\n  letter-spacing: 2px;\n}\n\n.subscribed_user {\n  position: relative;\n  left: 0px;\n  top: 55px;\n  font-size: 14px;\n  width: 100%;\n  text-align: center;\n  padding: 10px;\n  word-spacing: 5px;\n  letter-spacing: 2px;\n}\n\n.reach_target {\n  position: relative;\n  top: 55%;\n  left: 0px;\n  line-height: 30px;\n  font-size: 14px;\n  width: 100%;\n  text-align: center;\n  padding: 10px;\n  word-spacing: 5px;\n  letter-spacing: 2px;\n}\n\n.hr_mydiv {\n  position: relative;\n  left: 0px;\n  top: 50%;\n  font-size: 17px;\n  width: 100%;\n  text-align: center;\n  padding: 10px;\n  word-spacing: 5px;\n  letter-spacing: 2px;\n}\n\n.its_dating_div {\n  width: 100%;\n  position: fixed;\n  left: 0px;\n  bottom: 50px;\n  text-align: center;\n  font-size: 20px;\n  padding: 10px;\n  line-height: 30px;\n}\n\n.bottom_footer {\n  position: fixed;\n  left: 0px;\n  bottom: 0px;\n  border-top: 2px solid #ffffff;\n  width: 100%;\n  text-align: center;\n  padding: 10px;\n}\n\n.socialLogin ul {\n  margin-top: 15px !important;\n}\n\nion-content {\n  --background: #53abd9;\n  --background: -moz-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: -webkit-linear-gradient(left, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  --background: linear-gradient(to right, #53abd9 0%, #395a6b 50%, #324a57 100%);\n  display: table;\n}\n\n.page-centered-element {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  color: #ffffff;\n  font-size: 15px;\n  font-family: \"CaviarDreams\";\n}\n\n.content {\n  text-align: center;\n}\n\n.info-icon {\n  position: fixed;\n  margin-top: 10px;\n  margin-left: 18%;\n  font-size: 32px;\n}\n\n.modal-wrapper {\n  --width: 60%;\n  --height: 70%;\n}\n\n.my-custom-modal-css .modal-wrapper {\n  height: 20%;\n  top: 80%;\n  position: absolute;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9yZWdpc3RlcmZpbmlzaGVkL3JlZ2lzdGVyZmluaXNoZWQucGFnZS5zY3NzIiwic3JjL2FwcC9zaWdudXAvcmVnaXN0ZXJmaW5pc2hlZC9yZWdpc3RlcmZpbmlzaGVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0FDQ0o7O0FERUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUVBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDQUo7O0FER0E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBRUEsZUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDREo7O0FESUE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0FDREo7O0FESUE7RUFDSSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNESjs7QURJQTtFQUNJLDJCQUFBO0FDREo7O0FESUE7RUFLSSxxQkFBQTtFQUNBLCtFQUFBO0VBQ0Esa0ZBQUE7RUFDQSw4RUFBQTtFQUNBLGNBQUE7QUNMSjs7QURRQTtFQUVJLE9BQUE7RUFDQSxRQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxvQkFBQTtFQUFBLGFBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsMkJBQUE7QUNOSjs7QURVQTtFQUNJLGtCQUFBO0FDUEo7O0FEYUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNYSjs7QURjQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FDWEo7O0FEY0E7RUFDSSxXQUFBO0VBQ0EsUUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ1hKIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL3JlZ2lzdGVyZmluaXNoZWQvcmVnaXN0ZXJmaW5pc2hlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nbyB7XG4gICAgbWF4LXdpZHRoOiA0MCU7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuLmxvZ29UYXBDbHMge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAxJTtcbn1cblxuLmJvdHRvbV9wYXJhIHtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xufVxuXG4uc3Vic2NyaWJlZF91c2VyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogNTVweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd29yZC1zcGFjaW5nOiA1cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbn1cblxuLnJlYWNoX3RhcmdldCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogNTUlO1xuICAgIGxlZnQ6IDBweDtcbiAgICAvLyBib3R0b206IDI1MHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB3b3JkLXNwYWNpbmc6IDVweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xufVxuXG4uaHJfbXlkaXYge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBsZWZ0OiAwcHg7XG4gICAgdG9wOiA1MCU7XG4gICAgLy8gYm90dG9tOiAxOTBweDtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgd29yZC1zcGFjaW5nOiA1cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbn1cblxuLml0c19kYXRpbmdfZGl2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbGVmdDogMHB4O1xuICAgIGJvdHRvbTogNTBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG59XG5cbi5ib3R0b21fZm9vdGVyIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbGVmdDogMHB4O1xuICAgIGJvdHRvbTogMHB4O1xuICAgIGJvcmRlci10b3A6IDJweCBzb2xpZCAjZmZmZmZmO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4O1xufVxuXG4uc29jaWFsTG9naW4gdWwge1xuICAgIG1hcmdpbi10b3A6IDE1cHghaW1wb3J0YW50O1xufVxuXG5pb24tY29udGVudCB7XG4gICAgLy8gIGJhY2tncm91bmQ6ICM1NWI0ZTA7XG4gICAgLy8gYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQobGVmdCwgIzU1YjRlMCAwJSwgIzNhNWQ3MCA1MCUsICMzMjQ5NTcgMTAwJSk7XG4gICAgLy8gYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCwgIzU1YjRlMCAwJSwgIzNhNWQ3MCA1MCUsICMzMjQ5NTcgMTAwJSk7XG4gICAgLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjNTViNGUwIDAlLCAjM2E1ZDcwIDUwJSwgIzMyNDk1NyAxMDAlKTtcbiAgICAtLWJhY2tncm91bmQ6ICM1M2FiZDk7XG4gICAgLS1iYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudChsZWZ0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgICAtLWJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGxlZnQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjNTNhYmQ5IDAlLCAjMzk1YTZiIDUwJSwgIzMyNGE1NyAxMDAlKTtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnBhZ2UtY2VudGVyZWQtZWxlbWVudCB7XG4gICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGZvbnQtZmFtaWx5OiAnQ2F2aWFyRHJlYW1zJztcbiAgICAvLyBmb250LWZhbWlseTogJ0NhdmlhckRyZWFtcyBDYXZpYXJEcmVhbXNfQm9sZCBDbGVtZW50ZVBEYW8tSGVhdnknO1xufVxuXG4uY29udGVudCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC5idXR0b25zLWNvbnRhaW5lciB7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDMwcHg7XG4gICAgfVxufVxuXG4uaW5mby1pY29uIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBtYXJnaW4tbGVmdDogMTglO1xuICAgIGZvbnQtc2l6ZTogMzJweDtcbn1cblxuLm1vZGFsLXdyYXBwZXIge1xuICAgIC0td2lkdGg6IDYwJTtcbiAgICAtLWhlaWdodDogNzAlO1xufVxuXG4ubXktY3VzdG9tLW1vZGFsLWNzcyAubW9kYWwtd3JhcHBlciB7XG4gICAgaGVpZ2h0OiAyMCU7XG4gICAgdG9wOiA4MCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xufSIsIi5sb2dvIHtcbiAgbWF4LXdpZHRoOiA0MCU7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5sb2dvVGFwQ2xzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxJTtcbn1cblxuLmJvdHRvbV9wYXJhIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xufVxuXG4uc3Vic2NyaWJlZF91c2VyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogNTVweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxMHB4O1xuICB3b3JkLXNwYWNpbmc6IDVweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbn1cblxuLnJlYWNoX3RhcmdldCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiA1NSU7XG4gIGxlZnQ6IDBweDtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogMTBweDtcbiAgd29yZC1zcGFjaW5nOiA1cHg7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG59XG5cbi5ocl9teWRpdiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMHB4O1xuICB0b3A6IDUwJTtcbiAgZm9udC1zaXplOiAxN3B4O1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxMHB4O1xuICB3b3JkLXNwYWNpbmc6IDVweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbn1cblxuLml0c19kYXRpbmdfZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbGVmdDogMHB4O1xuICBib3R0b206IDUwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICBsaW5lLWhlaWdodDogMzBweDtcbn1cblxuLmJvdHRvbV9mb290ZXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGxlZnQ6IDBweDtcbiAgYm90dG9tOiAwcHg7XG4gIGJvcmRlci10b3A6IDJweCBzb2xpZCAjZmZmZmZmO1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4uc29jaWFsTG9naW4gdWwge1xuICBtYXJnaW4tdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjNTNhYmQ5O1xuICAtLWJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KGxlZnQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICAtLWJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KGxlZnQsICM1M2FiZDkgMCUsICMzOTVhNmIgNTAlLCAjMzI0YTU3IDEwMCUpO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzUzYWJkOSAwJSwgIzM5NWE2YiA1MCUsICMzMjRhNTcgMTAwJSk7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4ucGFnZS1jZW50ZXJlZC1lbGVtZW50IHtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC1mYW1pbHk6IFwiQ2F2aWFyRHJlYW1zXCI7XG59XG5cbi5jb250ZW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmluZm8taWNvbiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDE4JTtcbiAgZm9udC1zaXplOiAzMnB4O1xufVxuXG4ubW9kYWwtd3JhcHBlciB7XG4gIC0td2lkdGg6IDYwJTtcbiAgLS1oZWlnaHQ6IDcwJTtcbn1cblxuLm15LWN1c3RvbS1tb2RhbC1jc3MgLm1vZGFsLXdyYXBwZXIge1xuICBoZWlnaHQ6IDIwJTtcbiAgdG9wOiA4MCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogYmxvY2s7XG59Il19 */");

/***/ }),

/***/ "./src/app/signup/registerfinished/registerfinished.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registerfinished/registerfinished.page.ts ***!
  \******************************************************************/
/*! exports provided: RegisterfinishedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterfinishedPage", function() { return RegisterfinishedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _modalwin_modalwin_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../modalwin/modalwin.component */ "./src/app/modalwin/modalwin.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");









//  import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
var RegisterfinishedPage = /** @class */ (function () {
    function RegisterfinishedPage(gen, toastCtrl, authSvc, router, activatedroute, modalController, navCtrl, popoverController, alertController, platform) {
        this.gen = gen;
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this.router = router;
        this.activatedroute = activatedroute;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.popoverController = popoverController;
        this.alertController = alertController;
        this.platform = platform;
        this.mycount = 10000;
        this.usercount = 0;
        this.showcount = 10000;
    }
    RegisterfinishedPage.prototype.ngOnInit = function () {
        this.userAccount = this.activatedroute.snapshot.paramMap.get('userAccount');
        //  this.run_count();
        this.stop_back();
        this.get_user_count();
        this.mycount = this.mycount + this.usercount;
    };
    RegisterfinishedPage.prototype.get_user_count = function () {
        var _this = this;
        this.authSvc.getuserCount('bbclarkws@gmail.com').then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.gen.closePresentLoading();
                v = JSON.parse(req.data);
                if (v.success) {
                    this.usercount = v.data;
                }
                else {
                    this.usercount = v.data;
                }
                return [2 /*return*/];
            });
        }); });
    };
    RegisterfinishedPage.prototype.stop_back = function () {
        this.platform.backButton.subscribeWithPriority(9999, function () {
            document.addEventListener('backbutton', function (event) {
                event.preventDefault();
                event.stopPropagation();
                this.gen.presentToast('Back Button disabled');
                console.log('hello');
            }, false);
        });
    };
    RegisterfinishedPage.prototype.run_count = function () {
        var _this = this;
        this.timer = setInterval(function () {
            //run code
            if (_this.mycount !== 30000) {
                _this.mycount = _this.mycount + 1;
            }
        }, 3000);
    };
    RegisterfinishedPage.prototype.presentPopover = function (ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modalwin_modalwin_component__WEBPACK_IMPORTED_MODULE_1__["ModalwinComponent"],
                            cssClass: 'my-custom-modal-css'
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    RegisterfinishedPage.prototype.presentAlert = function (ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Alert',
                            buttons: [
                                {
                                    text: 'Okay',
                                    handler: function () {
                                        _this.presentPopover(ev);
                                    }
                                }, {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterfinishedPage.prototype.openWebpage = function (url) {
        //  const options: InAppBrowserOptions = {
        //    zoom: 'no',
        //    fullscreen: 'yes',
        //    hidenavigationbuttons: 'no',
        //    toolbar: 'no',
        //    hideurlbar: 'yes',
        //  };
        //  // Opening a URL and returning an InAppBrowserObject
        //  const browser = this.iab.create(url, '_system', {
        //    toolbar: 'no', hideurlbar: 'yes', location: 'no', options
        //  });
        //  browser.on('loadstop').subscribe(event => {
        //    browser.insertCSS({ code: 'toolbar{display: none;' });
        //  });
        //   // Inject scripts, css and more with browser.X
    };
    RegisterfinishedPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] }
    ]; };
    RegisterfinishedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-registerfinished',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registerfinished.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registerfinished/registerfinished.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registerfinished.page.scss */ "./src/app/signup/registerfinished/registerfinished.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]])
    ], RegisterfinishedPage);
    return RegisterfinishedPage;
}());



/***/ })

}]);
//# sourceMappingURL=registerfinished-registerfinished-module.js.map