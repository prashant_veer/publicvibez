(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/tabs/tabs.page.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/tabs/tabs.page.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"other list\">\n    <div class=\"myheader\">\n        <div class=\"menuClick\">\n        </div>\n        <ion-title>\n            <h1>Page {{ pageTitle }}</h1>\n        </ion-title>\n    </div>\n</ion-header>\n<ion-tabs class=\"other list\">\n    <!-- <div class=\"footerMenu\"> -->\n    <!-- <div class=\"mainDiv\"> -->\n    <ion-tab-bar slot=\"top\" class=\"tabOpt\" #tabs (ionSelect)=\"setTitle()\">\n        <ion-tab-button tab=\"trending\" class=\"mytab-button\" (click)=\"myevent($event)\">\n            Trending\n        </ion-tab-button>\n        <ion-tab-button tab=\"map\" class=\"mytab-button\">\n            Map\n        </ion-tab-button>\n        <ion-tab-button tab=\"live\" class=\"mytab-button\">\n            Live\n        </ion-tab-button>\n        <ion-tab-button tab=\"vibez\" class=\"mytab-button\">\n            Vibez\n        </ion-tab-button>\n        <ion-tab-button tab=\"chat\" class=\"mytab-button\">\n            Chat\n        </ion-tab-button>\n        <!-- <ul>\n            <li>\n                Trending\n            </li>\n            <li>\n                Map\n\n            </li>\n            <li class=\"selected\">\n                Live\n            </li>\n            <li>\n                Vibez\n            </li>\n            <li>\n                Chat\n            </li>\n        </ul> -->\n\n    </ion-tab-bar>\n    <ion-tab-bar slot=\"top\" class=\"login\">\n        <div class=\"searchTop\">\n            <div class=\"searchText\">\n                <input type=\"text\">\n            </div>\n            <div class=\"wishlist\">\n\n            </div>\n            <div class=\"add\">\n\n            </div>\n        </div>\n    </ion-tab-bar>\n\n    <!-- </div> -->\n    <!-- </div> -->\n    <!-- <ion-tab-bar slot=\"bottom\">\n            <ion-tab-button tab=\"map\">\n                <img src=\"assets/images/footer1.png\" alt=\"\" title=\"\" style=\"height: 80% !important;\">\n            </ion-tab-button>\n\n            <ion-tab-button tab=\"myprofile\">\n                <img src=\"assets/images/footer2.png\" alt=\"\" title=\"\" style=\"height: 80% !important;\">\n            </ion-tab-button>\n\n            <ion-tab-button tab=\"membership\">\n                <img src=\"assets/images/footer3.png\" alt=\"\" title=\"\" style=\"height: 80% !important;\">\n            </ion-tab-button>\n\n            <ion-tab-button tab=\"chat\">\n                <img src=\"assets/images/footer4.png\" alt=\"\" title=\"\" style=\"height: 80% !important;\">\n            </ion-tab-button>\n        </ion-tab-bar> -->\n\n    <!-- </div> -->\n</ion-tabs>");

/***/ }),

/***/ "./src/app/home/tabs/tabs-routing.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home/tabs/tabs-routing.module.ts ***!
  \**************************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "./src/app/home/tabs/tabs.page.ts");




var routes = [
    {
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'map',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | map-info-map-info-module */[__webpack_require__.e("common"), __webpack_require__.e("map-info-map-info-module")]).then(__webpack_require__.bind(null, /*! ../map-info/map-info.module */ "./src/app/home/map-info/map-info.module.ts")).then(function (m) { return m.MapInfoPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'myprofile',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | my-profile-my-profile-module */ "my-profile-my-profile-module").then(__webpack_require__.bind(null, /*! ../my-profile/my-profile.module */ "./src/app/home/my-profile/my-profile.module.ts")).then(function (m) { return m.MyProfilePageModule; });
                        }
                    }
                ]
            },
            {
                path: 'membership',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | membership-membership-module */ "membership-membership-module").then(__webpack_require__.bind(null, /*! ../membership/membership.module */ "./src/app/home/membership/membership.module.ts")).then(function (m) { return m.MembershipPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'chat',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | chat-list-chat-list-module */[__webpack_require__.e("common"), __webpack_require__.e("chat-list-chat-list-module")]).then(__webpack_require__.bind(null, /*! ../chat-list/chat-list.module */ "./src/app/home/chat-list/chat-list.module.ts")).then(function (m) { return m.ChatListPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'trending',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | trending-trending-module */ "trending-trending-module").then(__webpack_require__.bind(null, /*! ../trending/trending.module */ "./src/app/home/trending/trending.module.ts")).then(function (m) { return m.TrendingPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'live',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return Promise.all(/*! import() | livelist-livelist-module */[__webpack_require__.e("common"), __webpack_require__.e("livelist-livelist-module")]).then(__webpack_require__.bind(null, /*! ../livelist/livelist.module */ "./src/app/home/livelist/livelist.module.ts")).then(function (m) { return m.LivelistPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'vibez',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | vibezlist-vibezlist-module */ "vibezlist-vibezlist-module").then(__webpack_require__.bind(null, /*! ../vibezlist/vibezlist.module */ "./src/app/home/vibezlist/vibezlist.module.ts")).then(function (m) { return m.VibezlistPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'resturant-details',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | resturant-details-resturant-details-module */ "resturant-details-resturant-details-module").then(__webpack_require__.bind(null, /*! ../resturant-details/resturant-details.module */ "./src/app/home/resturant-details/resturant-details.module.ts")).then(function (m) { return m.ResturantDetailsPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'resturant-feedback',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | resturant-feedback-resturant-feedback-module */ "resturant-feedback-resturant-feedback-module").then(__webpack_require__.bind(null, /*! ../resturant-feedback/resturant-feedback.module */ "./src/app/home/resturant-feedback/resturant-feedback.module.ts")).then(function (m) { return m.ResturantFeedbackPageModule; });
                        }
                    }
                ]
            },
            {
                path: 'profile-otheruser',
                children: [
                    {
                        path: '',
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() | profileother-profileother-module */ "profileother-profileother-module").then(__webpack_require__.bind(null, /*! ../profileother/profileother.module */ "./src/app/home/profileother/profileother.module.ts")).then(function (m) { return m.ProfileotherPageModule; });
                        }
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/home/map',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/map',
        pathMatch: 'full'
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/tabs/tabs.module.ts":
/*!******************************************!*\
  !*** ./src/app/home/tabs/tabs.module.ts ***!
  \******************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "./src/app/home/tabs/tabs-routing.module.ts");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/home/tabs/tabs.page.ts");







var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/home/tabs/tabs.page.scss":
/*!******************************************!*\
  !*** ./src/app/home/tabs/tabs.page.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mytab-button {\n  --background: #000000;\n  border-right: solid 1px #fff;\n  color: #fff;\n  font-size: medium;\n}\n\n.mytab-button:last-child {\n  border-right: none;\n}\n\n.mytab-button.selected {\n  color: #379DE5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL2hvbWUvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS90YWJzL3RhYnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSw0QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teXRhYi1idXR0b24ge1xuICAgIC0tYmFja2dyb3VuZDogIzAwMDAwMDtcbiAgICBib3JkZXItcmlnaHQ6IHNvbGlkIDFweCAjZmZmO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogbWVkaXVtO1xufVxuXG4ubXl0YWItYnV0dG9uOmxhc3QtY2hpbGQge1xuICAgIGJvcmRlci1yaWdodDogbm9uZTtcbn1cblxuLm15dGFiLWJ1dHRvbi5zZWxlY3RlZCB7XG4gICAgY29sb3I6ICMzNzlERTVcbn0iLCIubXl0YWItYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMDAwMDAwO1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIDFweCAjZmZmO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiBtZWRpdW07XG59XG5cbi5teXRhYi1idXR0b246bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1yaWdodDogbm9uZTtcbn1cblxuLm15dGFiLWJ1dHRvbi5zZWxlY3RlZCB7XG4gIGNvbG9yOiAjMzc5REU1O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/tabs/tabs.page.ts":
/*!****************************************!*\
  !*** ./src/app/home/tabs/tabs.page.ts ***!
  \****************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tabsList = [{
                name: 'Trending',
                label: 'Trending',
            }, {
                name: 'Map',
                label: 'Map',
            }, {
                name: 'Vibez',
                label: 'Live',
            }, {
                name: 'Vibez',
                label: 'Vibez',
            }, {
                name: 'Chat',
                label: 'Chat',
            }];
    }
    TabsPage.prototype.setTitle = function () {
        var currentTab = this.tabs.getSelected();
        var matchingTab = this.tabsList.filter(function (tab) { return tab.name === currentTab; })[0];
        // uses the first array element as the currentTab
        this.pageTitle = this.tabsList.filter(function (tab) { return tab.name === currentTab; })[0];
        alert(currentTab);
    };
    TabsPage.prototype.myevent = function (event) {
        var value = event.details;
        console.log('tab value' + value);
        this.pageTitle = value;
    };
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/tabs/tabs.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tabs.page.scss */ "./src/app/home/tabs/tabs.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=home-tabs-tabs-module.js.map