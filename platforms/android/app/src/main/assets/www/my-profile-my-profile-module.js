(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-profile-my-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/my-profile/my-profile.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/my-profile/my-profile.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-title>My Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <div *ngIf=\"userInfo\">\n      <div class=\"profileTop\">\n          <img class=\"profileChanges\" (click)=\"onClickPhotos()\" src=\"assets/images/dslr-camera.svg\" alt=\"Change Picture\" title=\"Change Picture\">\n          <img class=\"profileEdit\" (click)=\"onClickEditProfile()\" src=\"assets/images/edit.svg\" alt=\"Edit Profile\" title=\"Edit Profile\">\n      </div>\n      <div class=\"profileDesc\">\n          <table>\n              <tr>\n                  <td colspan=\"2\">\n                      <img style=\"max-height: 200px;width: 100%;\" src=\"{{url}}thumbnail/{{general.getUserPhotos()}}_thumb.jpg\" alt=\"\" title=\"\">\n                  </td>\n              </tr>\n              <tr>\n                  <td>\n                      <span>Passion Dating Name</span>\n                      <h4> {{userInfo.name}}</h4>\n                  </td>\n                  <td>\n                      <span>Birthdate</span>\n                      <h4>{{userInfo.birthdate}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">\n                      <span>Location</span>\n                      <h4>{{userInfo.city}} {{userInfo.zipcode}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">\n                      <span>Email</span>\n                      <h4>{{userInfo.email_address}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <th colspan=\"2\">\n                      <h4>Basic</h4>\n                  </th>\n              </tr>\n              <tr>\n                  <td>\n                      <span>Sign</span>\n                      <h4>Aquaries</h4>\n                  </td>\n                  <td>\n                      <span>Height</span>\n                      <h4>{{userInfo.height}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td>\n                      <span>Gender</span>\n                      <h4>{{userInfo.gender}}</h4>\n                  </td>\n                  <td>\n                      <span>Interested In</span>\n                      <h4>{{userInfo.Interested}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td>\n                      <span>Ethenticity </span>\n                      <h4>{{userInfo.city}}</h4>\n                  </td>\n                  <td>\n                      <span>Body Type</span>\n                      <h4>{{userInfo.body_type}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <th colspan=\"2\">\n                      <span>Relationship History</span>\n                      <h4>Add your relationship history</h4>\n                  </th>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">\n                      <span>Children</span>\n                      <h4>{{userInfo.no_of_children}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">\n                      <span>Education</span>\n                      <h4>{{userInfo.education}}</h4>\n                  </td>\n              </tr>\n              <tr>\n                  <td colspan=\"2\">\n                      <span>Smoking</span>\n                      <h4>{{userInfo.smoke}}</h4>\n                  </td>\n              </tr>\n          </table>\n      </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/my-profile/my-profile-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/home/my-profile/my-profile-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: MyProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePageRoutingModule", function() { return MyProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _my_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-profile.page */ "./src/app/home/my-profile/my-profile.page.ts");




var routes = [
    {
        path: '',
        component: _my_profile_page__WEBPACK_IMPORTED_MODULE_3__["MyProfilePage"]
    }
];
var MyProfilePageRoutingModule = /** @class */ (function () {
    function MyProfilePageRoutingModule() {
    }
    MyProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MyProfilePageRoutingModule);
    return MyProfilePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/my-profile/my-profile.module.ts":
/*!******************************************************!*\
  !*** ./src/app/home/my-profile/my-profile.module.ts ***!
  \******************************************************/
/*! exports provided: MyProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePageModule", function() { return MyProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-profile-routing.module */ "./src/app/home/my-profile/my-profile-routing.module.ts");
/* harmony import */ var _my_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-profile.page */ "./src/app/home/my-profile/my-profile.page.ts");







var MyProfilePageModule = /** @class */ (function () {
    function MyProfilePageModule() {
    }
    MyProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyProfilePageRoutingModule"]
            ],
            declarations: [_my_profile_page__WEBPACK_IMPORTED_MODULE_6__["MyProfilePage"]]
        })
    ], MyProfilePageModule);
    return MyProfilePageModule;
}());



/***/ }),

/***/ "./src/app/home/my-profile/my-profile.page.scss":
/*!******************************************************!*\
  !*** ./src/app/home/my-profile/my-profile.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvbXktcHJvZmlsZS9teS1wcm9maWxlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/home/my-profile/my-profile.page.ts":
/*!****************************************************!*\
  !*** ./src/app/home/my-profile/my-profile.page.ts ***!
  \****************************************************/
/*! exports provided: MyProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePage", function() { return MyProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");





var MyProfilePage = /** @class */ (function () {
    function MyProfilePage(general, router) {
        this.general = general;
        this.router = router;
    }
    MyProfilePage.prototype.ngOnInit = function () {
        this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].root_URL;
        /* this.events.subscribe('user:updateProfile', () => {
           console.log("updateProfile");
           this.userInfo = this.general.getUserInfo();
         });*/
    };
    MyProfilePage.prototype.ionViewDidEnter = function () {
        console.log("ionViewDidEnter");
        this.userInfo = this.general.getUserInfo();
        console.log(this.userInfo);
    };
    MyProfilePage.prototype.onClickPhotos = function () {
        var _this = this;
        this.router.navigate(['home/gallery', {
                user: JSON.stringify(this.general.getUserInfo())
            }]).then(function (res) {
            _this.userInfo = _this.general.getUserInfo();
        });
    };
    MyProfilePage.prototype.onClickEditProfile = function () {
        var _this = this;
        this.router.navigate(['home/editprofile', {
                user: JSON.stringify(this.general.getUserInfo())
            }]).then(function (res) {
            console.log("refresh");
            _this.userInfo = _this.general.getUserInfo();
        });
    };
    MyProfilePage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    MyProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-my-profile',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./my-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/my-profile/my-profile.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./my-profile.page.scss */ "./src/app/home/my-profile/my-profile.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], MyProfilePage);
    return MyProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=my-profile-my-profile-module.js.map