(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signup-signup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n\n    <a href=\"\" class=\"round-button-create-profile gradient-background\" style=\"margin-top: 20%;\">\n        <ion-icon name=\"person-outline\" style=\"font-size: 50px;margin-top: 10%;\"></ion-icon>\n    </a>\n    <br>\n    <h2><span>CREATE <b>PROFILE</b></span></h2>\n    <div class=\"form-div\">\n        <div class=\"formElement\">\n            <ion-input type=\"text\" [(ngModel)]=\"userNickName\" class=\"general-input ion-input-class\" placeholder=\"Name\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"email\" [(ngModel)]=\"userEmail\" class=\"general-input ion-input-class\" placeholder=\"Email\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"tel\" [(ngModel)]=\"userMobile\" class=\"general-input ion-input-class\" placeholder=\"Mobile Number\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-input type=\"password\" [(ngModel)]=\"userPass\" class=\"general-input ion-input-class\" placeholder=\"password\"></ion-input>\n        </div>\n        <div class=\"formElement\">\n            <ion-checkbox slot=\"start\" color=\"light\" checked=\"true\" (ionChange)=\"checkEvent()\">\n            </ion-checkbox>\n            <ion-label color=\"light\"> Let Vibez know your location</ion-label>\n        </div>\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_getstart()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/signup-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/signup/signup-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageRoutingModule", function() { return SignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup.page */ "./src/app/signup/signup.page.ts");




var routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_3__["SignupPage"]
    },
    {
        path: 'gettingstart',
        loadChildren: function () { return __webpack_require__.e(/*! import() | gettingstart-gettingstart-module */ "gettingstart-gettingstart-module").then(__webpack_require__.bind(null, /*! ./gettingstart/gettingstart.module */ "./src/app/signup/gettingstart/gettingstart.module.ts")).then(function (m) { return m.GettingstartPageModule; }); }
    },
    {
        path: 'registeraboutyou',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registeraboutyou-registeraboutyou-module */ "registeraboutyou-registeraboutyou-module").then(__webpack_require__.bind(null, /*! ./registeraboutyou/registeraboutyou.module */ "./src/app/signup/registeraboutyou/registeraboutyou.module.ts")).then(function (m) { return m.RegisteraboutyouPageModule; }); }
    },
    {
        path: 'registerfinished',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registerfinished-registerfinished-module */ "registerfinished-registerfinished-module").then(__webpack_require__.bind(null, /*! ./registerfinished/registerfinished.module */ "./src/app/signup/registerfinished/registerfinished.module.ts")).then(function (m) { return m.RegisterfinishedPageModule; }); }
    },
    {
        path: 'otpcheck',
        loadChildren: function () { return Promise.all(/*! import() | otpcheck-otpcheck-module */[__webpack_require__.e("common"), __webpack_require__.e("otpcheck-otpcheck-module")]).then(__webpack_require__.bind(null, /*! ./otpcheck/otpcheck.module */ "./src/app/signup/otpcheck/otpcheck.module.ts")).then(function (m) { return m.OtpcheckPageModule; }); }
    },
    {
        path: 'profile-location',
        loadChildren: function () { return __webpack_require__.e(/*! import() | profile-location-profile-location-module */ "profile-location-profile-location-module").then(__webpack_require__.bind(null, /*! ./profile-location/profile-location.module */ "./src/app/signup/profile-location/profile-location.module.ts")).then(function (m) { return m.ProfileLocationPageModule; }); }
    },
    {
        path: 'registersummary',
        loadChildren: function () { return __webpack_require__.e(/*! import() | registersummary-registersummary-module */ "registersummary-registersummary-module").then(__webpack_require__.bind(null, /*! ./registersummary/registersummary.module */ "./src/app/signup/registersummary/registersummary.module.ts")).then(function (m) { return m.RegistersummaryPageModule; }); }
    }
];
var SignupPageRoutingModule = /** @class */ (function () {
    function SignupPageRoutingModule() {
    }
    SignupPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], SignupPageRoutingModule);
    return SignupPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.module.ts":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.module.ts ***!
  \*****************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup-routing.module */ "./src/app/signup/signup-routing.module.ts");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "./src/app/signup/signup.page.ts");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");









var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignupPageRoutingModule"]
            ],
            declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]],
            providers: [
                _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_7__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_8__["Facebook"]
            ]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());



/***/ }),

/***/ "./src/app/signup/signup.page.scss":
/*!*****************************************!*\
  !*** ./src/app/signup/signup.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 40px;\n  width: 100%;\n  border: solid 1px #981F40;\n  text-align: center;\n  outline: none;\n  text-transform: uppercase;\n  padding-top: 10px;\n}\n\ndatetime-text {\n  padding: 13px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIiwic3JjL2FwcC9zaWdudXAvc2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0FDQ0o7O0FERUE7RUFDSSx3QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL3NpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXlzZWxlY3Qge1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjOTgxRjQwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbmRhdGV0aW1lLXRleHQge1xuICAgIHBhZGRpbmc6IDEzcHggIWltcG9ydGFudDtcbn0iLCIubXlzZWxlY3Qge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IHNvbGlkIDFweCAjOTgxRjQwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG5kYXRldGltZS10ZXh0IHtcbiAgcGFkZGluZzogMTNweCAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/signup/signup.page.ts":
/*!***************************************!*\
  !*** ./src/app/signup/signup.page.ts ***!
  \***************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "./node_modules/@ionic-native/google-plus/ngx/index.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");










var SignupPage = /** @class */ (function () {
    function SignupPage(toastCtrl, authSvc, nav, googlePlus, fb, gen, geolocation, nativeGeocoder) {
        this.toastCtrl = toastCtrl;
        this.authSvc = authSvc;
        this.nav = nav;
        this.googlePlus = googlePlus;
        this.fb = fb;
        this.gen = gen;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.emailpattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
        this.mobilePattern = '/^([+]\d{2})?\d{10}$/';
        this.autoLocation = false;
    }
    SignupPage.prototype.ngOnInit = function () {
        var _this = this;
        // this.gen.presentLoading("");
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
            var options = {
                useLocale: true,
                maxResults: 5
            };
            _this.nativeGeocoder.reverseGeocode(_this.latitude, _this.longitude, options)
                .then(function (result) {
                console.log(JSON.stringify(result));
                _this.gen.closePresentLoading();
                _this.userArea = result[0].subLocality;
                _this.usercity = result[0].locality;
                _this.userPincode = result[0].postalCode;
                _this.userState = result[0].administrativeArea;
                // this.gen.presentToast('area ' + this.userArea + 'city ' + this.usercity + 'pincode ' + 
                // this.userPincode + 'state ' + this.userState);
            })
                .catch(function (error) {
                // this.gen.closePresentLoading();
                // this.gen.presentToast('Error in location');
                console.log('Error getting location', error);
            });
        }).catch(function (error) {
            // this.gen.closePresentLoading();
            // this.gen.presentToast('Error in address location');
            console.log('Error getting location', error);
        });
        var options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
            .then(function (result) {
            console.log('address ' + JSON.stringify(result[0]));
            _this.geoAddress = _this.generateAddress(result[0]);
        })
            .catch(function (error) { return console.log(error); });
    };
    // Return Comma saperated address
    SignupPage.prototype.generateAddress = function (addressObj) {
        var obj = [];
        var address = '';
        // tslint:disable-next-line:forin
        for (var key in addressObj) {
            obj.push(addressObj[key]);
        }
        obj.reverse();
        for (var val in obj) {
            if (obj[val].length) {
                address += obj[val] + ', ';
            }
        }
        return address.slice(0, -2);
    };
    SignupPage.prototype.checkEvent = function () {
        this.autoLocation = !this.autoLocation;
        if (this.autoLocation === true) {
            this.gen.presentToast('You need to fill your location data Manually.');
            this.autoLocation = false;
        }
        else {
            this.gen.presentToast('Auto Location Check Allowed.');
            this.autoLocation = true;
        }
    };
    SignupPage.prototype.goto_getstart = function () {
        var _this = this;
        if (this.userNickName === undefined || this.userNickName === '') {
            this.gen.presentToast('Please enter Nickname');
        }
        else if (this.userEmail === undefined || this.userEmail === '') {
            this.gen.presentToast('Please enter Email id');
        }
        else if (!this.userEmail.match(this.emailpattern)) {
            this.gen.presentToast('Please enter proper Email id');
        }
        else if (this.userMobile === undefined || this.userMobile === '') {
            this.gen.presentToast('Please enter mobile number with country code.');
        }
        else if (this.userPass === undefined || this.userPass === '') {
            this.gen.presentToast('Please enter password');
        }
        else {
            this.gen.presentLoading("Please wait... ");
            this.authSvc.sendotptoEmail(this.userEmail, this.userMobile, this.userNickName).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.gen.closePresentLoading();
                    v = JSON.parse(req.data);
                    if (v.success) {
                        this.nav.navigateForward(['signup/otpcheck', {
                                email: this.userEmail,
                                NickName: this.userNickName,
                                mobile: this.userMobile,
                                Pass: this.userPass,
                                location: this.autoLocation,
                                area: this.userArea,
                                pincode: this.userPincode,
                                state: this.userState,
                                usercity: this.usercity,
                                latitude: this.latitude,
                                longitude: this.longitude,
                                otp: v.passcode
                            }]);
                    }
                    else {
                        this.errorToast(v.message);
                    }
                    return [2 /*return*/];
                });
            }); });
        }
    };
    SignupPage.prototype.goto_google = function () {
        var _this = this;
        this.googlePlus.login({})
            .then(function (res) {
            console.log(res),
                _this.nav.navigateForward(['signup/gettingstart', {
                        email: res.email, name: res.displayName,
                        googleID: res.userId, facebookId: ""
                    }]);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    SignupPage.prototype.goto_Facebook = function () {
        var _this = this;
        this.fb.getLoginStatus().then(function (res) {
            console.log(res);
            if (res.status === 'connected') {
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.fb.login(['email'])
                    .then(function (res) {
                    if (res.status === 'connected') {
                        _this.getUserDetail(res.authResponse.userID);
                    }
                    else {
                    }
                })
                    .catch(function (e) { return console.log('Error logging into Facebook', e); });
            }
        });
    };
    SignupPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api('/' + userid + '/?fields=id,email,name', ['public_profile'])
            .then(function (res) {
            console.log(res);
            _this.nav.navigateForward(['signup/gettingstart', {
                    name: res.name, email: res.email,
                    googleID: "", facebookId: res.id
                }]);
            //this.users = res;
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    SignupPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    SignupPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__["GooglePlus"] },
        { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__["Facebook"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_4__["GeneralService"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"] },
        { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_8__["NativeGeocoder"] }
    ]; };
    SignupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/signup.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signup.page.scss */ "./src/app/signup/signup.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _services_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
            _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__["GooglePlus"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_3__["Facebook"], _services_general__WEBPACK_IMPORTED_MODULE_4__["GeneralService"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_7__["Geolocation"],
            _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_8__["NativeGeocoder"]])
    ], SignupPage);
    return SignupPage;
}());



/***/ })

}]);
//# sourceMappingURL=signup-signup-module.js.map