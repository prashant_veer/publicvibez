(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-list-chat-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/chat-list/chat-list.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/chat-list/chat-list.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-title>VIBEZ LIST</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content style=\"margin-top: 40px;\">\n    <ion-list>\n        <ion-item *ngFor='let user of userList'>\n            <ion-grid>\n                <ion-row>\n                    <ion-col size=\"3\" (click)=\"openUserProfile(user)\">\n                        <img *ngIf=\"user.photoList.length &gt; 0\" style=\"width: 50px;height: 50px;\" src=\"{{imgPath}}thumbnail/{{user.photoList[0]['photo_name']}}_thumb.jpg\">\n                    </ion-col>\n                    <ion-col size=\"9\" style=\"position: relative;\">\n                        <div (click)=\"openUserProfile(user)\">\n                            <div>{{user.name}}</div>\n                            <div>{{user.email_address}}</div>\n                            <div>{{user.isVerify==0? \"Waiting\":\"\" }}</div>\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-item>\n    </ion-list>\n</ion-content>");

/***/ }),

/***/ "./src/app/home/chat-list/chat-list-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/home/chat-list/chat-list-routing.module.ts ***!
  \************************************************************/
/*! exports provided: ChatListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPageRoutingModule", function() { return ChatListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _chat_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat-list.page */ "./src/app/home/chat-list/chat-list.page.ts");




var routes = [
    {
        path: '',
        component: _chat_list_page__WEBPACK_IMPORTED_MODULE_3__["ChatListPage"]
    }
];
var ChatListPageRoutingModule = /** @class */ (function () {
    function ChatListPageRoutingModule() {
    }
    ChatListPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ChatListPageRoutingModule);
    return ChatListPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/chat-list/chat-list.module.ts":
/*!****************************************************!*\
  !*** ./src/app/home/chat-list/chat-list.module.ts ***!
  \****************************************************/
/*! exports provided: ChatListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPageModule", function() { return ChatListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-list-routing.module */ "./src/app/home/chat-list/chat-list-routing.module.ts");
/* harmony import */ var _chat_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat-list.page */ "./src/app/home/chat-list/chat-list.page.ts");







var ChatListPageModule = /** @class */ (function () {
    function ChatListPageModule() {
    }
    ChatListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _chat_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatListPageRoutingModule"]
            ],
            declarations: [_chat_list_page__WEBPACK_IMPORTED_MODULE_6__["ChatListPage"]]
        })
    ], ChatListPageModule);
    return ChatListPageModule;
}());



/***/ }),

/***/ "./src/app/home/chat-list/chat-list.page.scss":
/*!****************************************************!*\
  !*** ./src/app/home/chat-list/chat-list.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvY2hhdC1saXN0L2NoYXQtbGlzdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/home/chat-list/chat-list.page.ts":
/*!**************************************************!*\
  !*** ./src/app/home/chat-list/chat-list.page.ts ***!
  \**************************************************/
/*! exports provided: ChatListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatListPage", function() { return ChatListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");







var ChatListPage = /** @class */ (function () {
    function ChatListPage(navCtrl, gen, userService, router, activatedroute) {
        this.navCtrl = navCtrl;
        this.gen = gen;
        this.userService = userService;
        this.router = router;
        this.activatedroute = activatedroute;
        this.userList = [];
        this.imgPath = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].root_URL;
    }
    ChatListPage.prototype.ngOnInit = function () {
        /*this.events.subscribe('user:updateProfile', () => {
          console.log("updateProfile");
          this.getUserList();
        });*/
    };
    ChatListPage.prototype.ionViewWillEnter = function () {
        this.getUserList();
    };
    ChatListPage.prototype.getUserList = function () {
        var _this = this;
        this.userService.getUserInvitationList().then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                v = JSON.parse(req.data);
                console.log(v);
                if (v.success) {
                    console.log(v.data);
                    this.userList = v.data;
                }
                return [2 /*return*/];
            });
        }); });
    };
    ChatListPage.prototype.openUserProfile = function (user) {
        if (user.isVerify == 0) {
            this.router.navigate(['home/map-component/user-profile', {
                    userProfile: JSON.stringify(user),
                    pageType: 1
                }]);
        }
        else if (user.isVerify == 1) {
            this.router.navigate(['home/chat', {
                    userProfile: JSON.stringify(user),
                    pageType: 1
                }]);
        }
    };
    ChatListPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
    ]; };
    ChatListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/chat-list/chat-list.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat-list.page.scss */ "./src/app/home/chat-list/chat-list.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _services_general__WEBPACK_IMPORTED_MODULE_5__["GeneralService"], _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], ChatListPage);
    return ChatListPage;
}());



/***/ })

}]);
//# sourceMappingURL=chat-list-chat-list-module.js.map