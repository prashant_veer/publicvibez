(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["myProfile-edit-profile-edit-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/edit-profile/edit-profile.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/edit-profile/edit-profile.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"secondary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home/tabs/myprofile\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Update Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"wrapper\">\n    <div class=\"loginForm\">\n        <div class=\"tableCell\">\n          <div class=\"formElement\">\n            <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"userInfo.name\" placeholder=\"User Name\"></ion-input>\n          </div>\n          <div class=\"formElement\">\n            <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"userInfo.email_address\" placeholder=\"User Email\"></ion-input>\n          </div>\n          <div class=\"formElement\">\n            <ion-select [(ngModel)]=\"userInfo.gender\" class=\"myselect\" (ionChange)=\"genderValue($event)\" [selectedText]=\"genderSelectedtext\">\n                <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.Gender}}</ion-select-option>\n            </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-datetime class=\"myselect\" [(ngModel)]=\"userInfo.birthdate\" displayFormat=\"DD-MMM-YYYY\" placeholder=\"BIRTHDAY\" min=\"1950-03-14\" max=\"2001-01-01\"></ion-datetime>\n          </div>\n\n          <div class=\"formElement\">\n            <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"userInfo.city\" placeholder=\"City\"></ion-input>\n          </div>\n          <div class=\"formElement\">\n            <ion-input type=\"text\" class=\"myselect\" [(ngModel)]=\"userInfo.zipcode\" placeholder=\"Zip Code\"></ion-input>\n          </div>\n\n          <div class=\"formElement\">\n              <ion-select [(ngModel)]=\"userInfo.body_type\" class=\"myselect\" (ionChange)=\"bodytypeValue($event)\" >\n                  <ion-select-option *ngFor=\"let bodyt of bodytypearray\">{{bodyt.bodytype}}</ion-select-option>\n              </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-select [(ngModel)]=\"userInfo.height\" class=\"myselect\" (ionChange)=\"heightValue($event)\" >\n                  <ion-select-option *ngFor=\"let ht of userheightarray\">{{ht.height}}</ion-select-option>\n              </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-select [(ngModel)]=\"userInfo.no_of_children\" class=\"myselect\" (ionChange)=\"childValue($event)\" >\n                  <ion-select-option *ngFor=\"let ch of userchildarray\">{{ch.child}}</ion-select-option>\n              </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-select [(ngModel)]=\"userInfo.education\" class=\"myselect\" (ionChange)=\"educationValue($event)\" >\n                  <ion-select-option *ngFor=\"let ch of usereducationarray\">{{ch.education}}</ion-select-option>\n              </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-select [(ngModel)]=\"userInfo.smoke\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" >\n                  <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n              </ion-select>\n          </div>\n          <div class=\"formElement\">\n            <ion-select [(ngModel)]=\"userInfo.Interested\" class=\"myselect\" (ionChange)=\"genderInterestedValue($event)\" >\n                <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.Gender}}</ion-select-option>\n            </ion-select>\n          </div>\n          <div class=\"formElement\">\n              <ion-button color=\"secondary\" expand=\"full\" class=\"btn-font-size-large\" (click)=\"goto_updateProfile()\">CONTINUE</ion-button>\n          </div>\n        </div>\n\n    </div>\n</div>\n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/myProfile/edit-profile/edit-profile-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/myProfile/edit-profile/edit-profile-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: EditProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function() { return EditProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/myProfile/edit-profile/edit-profile.page.ts");




var routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
    }
];
var EditProfilePageRoutingModule = /** @class */ (function () {
    function EditProfilePageRoutingModule() {
    }
    EditProfilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], EditProfilePageRoutingModule);
    return EditProfilePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/myProfile/edit-profile/edit-profile.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/myProfile/edit-profile/edit-profile.module.ts ***!
  \***************************************************************/
/*! exports provided: EditProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-profile-routing.module */ "./src/app/myProfile/edit-profile/edit-profile-routing.module.ts");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/myProfile/edit-profile/edit-profile.page.ts");







var EditProfilePageModule = /** @class */ (function () {
    function EditProfilePageModule() {
    }
    EditProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"]
            ],
            declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
        })
    ], EditProfilePageModule);
    return EditProfilePageModule;
}());



/***/ }),

/***/ "./src/app/myProfile/edit-profile/edit-profile.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/myProfile/edit-profile/edit-profile.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 40px;\n  width: 100%;\n  border: solid 1px #981F40;\n  text-align: center;\n  outline: none;\n  text-transform: uppercase;\n  padding-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL215UHJvZmlsZS9lZGl0LXByb2ZpbGUvZWRpdC1wcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbXlQcm9maWxlL2VkaXQtcHJvZmlsZS9lZGl0LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL215UHJvZmlsZS9lZGl0LXByb2ZpbGUvZWRpdC1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teXNlbGVjdCB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlcjogc29saWQgMXB4ICM5ODFGNDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbn0iLCIubXlzZWxlY3Qge1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IHNvbGlkIDFweCAjOTgxRjQwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/myProfile/edit-profile/edit-profile.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/myProfile/edit-profile/edit-profile.page.ts ***!
  \*************************************************************/
/*! exports provided: EditProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePage", function() { return EditProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");






var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(activatedroute, toastCtrl, general, authSvc) {
        this.activatedroute = activatedroute;
        this.toastCtrl = toastCtrl;
        this.general = general;
        this.authSvc = authSvc;
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userchildarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.genderprefer = [];
    }
    EditProfilePage.prototype.ngOnInit = function () {
        this.genderSelectedtext = 'GENDER AND PREFERENCES';
        this.userInfo = JSON.parse(this.activatedroute.snapshot.paramMap.get('user'));
        console.log(this.userInfo);
        this.showbodytype();
        this.showchild();
        this.showeducation();
        this.showheight();
        this.showsmoke();
        this.showgenderPrefer();
    };
    EditProfilePage.prototype.ionViewDidLeave = function () {
        // this.events.publish('user:updateProfile');
    };
    EditProfilePage.prototype.showgenderPrefer = function () {
        this.genderprefer = [{ Gender: 'MALE' },
            { Gender: 'FEMALE' },
            { Gender: 'BOTH' }];
    };
    EditProfilePage.prototype.genderValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.gender = event.detail.value;
    };
    EditProfilePage.prototype.genderInterestedValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.Interested = event.detail.value;
    };
    EditProfilePage.prototype.showbodytype = function () {
        this.bodytypearray = [{ bodytype: 'Muscles' },
            { bodytype: 'Athletic' },
            { bodytype: 'Curvy' },
            { bodytype: 'Overweight' },
            { bodytype: 'Underweight' }
        ];
    };
    EditProfilePage.prototype.bodytypeValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.body_type = event.detail.value;
    };
    EditProfilePage.prototype.showheight = function () {
        this.userheightarray = [{ height: '<5 Foot' },
            { height: '>5 Foot' },
            { height: '>6 Foot' },
            { height: '<6 Foot' }
        ];
    };
    EditProfilePage.prototype.heightValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.height = event.detail.value;
    };
    EditProfilePage.prototype.showchild = function () {
        this.userchildarray = [{ child: 'YES' },
            { child: 'NO' }];
    };
    EditProfilePage.prototype.childValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.no_of_children = event.detail.value;
    };
    EditProfilePage.prototype.showeducation = function () {
        this.usereducationarray = [{ education: 'HS GED' },
            { education: 'HS Graduate' },
            { education: 'Attending College' },
            { education: 'College Graduate' },
            { education: 'Attending Advance Degree' },
            { education: 'Graduate Degree' }
        ];
    };
    EditProfilePage.prototype.educationValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.education = event.detail.value;
    };
    EditProfilePage.prototype.showsmoke = function () {
        this.usersmokearray = [{ smoke: 'YES' },
            { smoke: 'NO' }
        ];
    };
    EditProfilePage.prototype.smokeValue = function (event) {
        console.log(event.detail.value);
        this.userInfo.smoke = event.detail.value;
    };
    EditProfilePage.prototype.goto_updateProfile = function () {
        var _this = this;
        this.general.presentLoading("Please wait... ");
        this.authSvc.updateProfile(this.userInfo).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.general.closePresentLoading();
                v = JSON.parse(req.data);
                if (v.success) {
                    this.errorToast("Profile updated successfully");
                    this.general.setUserInfo(this.userInfo);
                }
                else {
                    this.errorToast(v.message);
                }
                return [2 /*return*/];
            });
        }); });
    };
    EditProfilePage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] }
    ]; };
    EditProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit-profile',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edit-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/myProfile/edit-profile/edit-profile.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edit-profile.page.scss */ "./src/app/myProfile/edit-profile/edit-profile.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _services_general__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], EditProfilePage);
    return EditProfilePage;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");





var LoginService = /** @class */ (function () {
    function LoginService(http, file) {
        this.http = http;
        this.file = file;
        this.http.setDataSerializer('urlencoded');
    }
    LoginService.prototype.sendotptoEmail = function (userEmail, userMobile, userNickName) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "userEmail": userEmail,
            "userMobile": userMobile,
            "userNickName": userNickName
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.sendOtptoEmail(), formData, {});
    };
    LoginService.prototype.getuserCount = function (userEmail) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "userEmail": userEmail,
        };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.user.count(), formData, {});
    };
    LoginService.prototype.login = function (mobile, password, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "username": mobile, "password": password, "token": token };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.login(), formData, {});
    };
    LoginService.prototype.register = function (NickName, email, userMobile, Pass, usercity, userArea, userPincode, userState, username, userBirthday, userPhoto, preferredGender, userbodytype, userheight, userchild, usersmoke, userdrinks, userzodiac, userRace, userPreference, userAboutme, latitude, longitude, userweight) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "nickName": NickName, "userEmail": email,
            "userMobile": userMobile, "pass": Pass,
            "username": username, "userBirthday": userBirthday, "profileimg": userPhoto,
            "cityName": usercity, "addressName": userArea, "userPincode": userPincode, "userState": userState,
            "preferredGender": preferredGender, "userbodytype": userbodytype, "userheight": userheight, "userchild": userchild,
            "usersmoke": usersmoke, "userdrinks": userdrinks, "userzodiac": userzodiac, "userRace": userRace, "userPreference": userPreference, "userAboutme": userAboutme,
            "latitude": latitude, "longitude": longitude, "userweight": userweight
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.registerLong(), formData, {});
    };
    LoginService.prototype.getPhotoList = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.photoList(), formData, {});
    };
    LoginService.prototype.updateProfile = function (userInfo) {
        this.http.setDataSerializer('urlencoded');
        var formData = {
            "user_id": userInfo.user_id,
            "name": userInfo.name,
            "gender": userInfo.gender,
            "birthdate": userInfo.birthdate, "email_address": userInfo.email_address,
            "city": userInfo.city, "zipcode": userInfo.zipcode,
            "body_type": userInfo.body_type, "height": userInfo.height,
            "education": userInfo.education, "no_of_children": userInfo.no_of_children,
            "smoke": userInfo.smoke, "Interested": userInfo.Interested
        };
        console.log(formData);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfile(), formData, {});
    };
    LoginService.prototype.registerImage = function (usersmoke) {
        this.http.setDataSerializer('multipart');
        this.http.setRequestTimeout(7000);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.updateProfileImg(), usersmoke, {});
    };
    LoginService.prototype.googleLogin = function (userEmail, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "googleID": "", "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.googleLogin(), formData, {});
    };
    LoginService.prototype.facebookLogin = function (userEmail, facebookId, token) {
        this.http.setDataSerializer('urlencoded');
        var formData = { "userEmail": userEmail, "facebookId": facebookId, "token": token };
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urls.auth.facebookLogin(), formData, {});
    };
    LoginService.ctorParameters = function () { return [
        { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_2__["HTTP"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_4__["File"]])
    ], LoginService);
    return LoginService;
}());



/***/ })

}]);
//# sourceMappingURL=myProfile-edit-profile-edit-profile-module.js.map