(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registeraboutyou-registeraboutyou-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"other\">\n        <div class=\"mainDiv uploadInfo\">\n            <div class=\"tableCell\">\n                <h2><span>Upload <b>Information</b></span></h2>\n                <div class=\"form-div\">\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Height</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userheight\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ht of userheightarray\">{{ht.height}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Weight</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userweight\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ht of userweightarray\">{{ht.val}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Zodiac</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userzodiac\" class=\"myselect\" placeholder=\"{{zodiactext}}\">\n                                            <ion-select-option *ngFor=\"let ch of userZodiacarray\">{{ch.type}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Child</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userchild\" class=\"myselect\" (ionChange)=\"childValue($event)\" placeholder=\"{{userchildtext}}\">\n                                            <ion-select-option *ngFor=\"let ch of userchildarray\">{{ch.child}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Gender Preference</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userPreference\" placeholder=\"Select Prefecen\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let gender of genderprefer\">{{gender.gendertype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n                    <!-- \n                    <div class=\"formElement\">\n                        <div class=\"ion-textarea-Box\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"12\">\n                                        <ion-textarea [(ngModel)]=\"userAboutme\" placeholder=\"About Me\" class=\"box-label label-padding\"></ion-textarea>\n                                    </ion-col>\n\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div> -->\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">BodyType</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userbodytype\" class=\"myselect\" (ionChange)=\"bodytypeValue($event)\" [selectedText]=\"bodytypetext\">\n                                            <ion-select-option *ngFor=\"let bodyt of bodytypearray\">{{bodyt.bodytype}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Smoke</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"usersmoke\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" [selectedText]=\"usersmoketext\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Drinks</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userdrinks\" class=\"myselect\" (ionChange)=\"smokeValue($event)\" placeholder=\"{{userdrinkstext}}\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n                    <div class=\"formElement\" style=\"display: none;\">\n                        <div class=\"selectBox\">\n                            <ion-grid>\n                                <ion-row class=\"ion-text-left\">\n                                    <ion-col size=\"1\" class=\"ion-padding-vertical box-left\">\n                                    </ion-col>\n                                    <ion-col size=\"4\" class=\"ion-padding-vertical box-label\">\n                                        <ion-label class=\"label-padding\">Race</ion-label>\n                                    </ion-col>\n                                    <ion-col size=\"7\">\n                                        <ion-select [(ngModel)]=\"userRace\" placeholder=\"Select Race\" class=\"myselect\">\n                                            <ion-select-option *ngFor=\"let ch of usersmokearray\">{{ch.smoke}}</ion-select-option>\n                                        </ion-select>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>\n                        </div>\n                    </div>\n\n\n\n\n                    <ion-grid>\n                        <ion-row class=\"ion-text-center\">\n                            <ion-col>\n                                <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_registerfinish()\">\n                                    <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                                </a>\n\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: RegisteraboutyouPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPageRoutingModule", function() { return RegisteraboutyouPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registeraboutyou.page */ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts");




var routes = [
    {
        path: '',
        component: _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_3__["RegisteraboutyouPage"]
    }
];
var RegisteraboutyouPageRoutingModule = /** @class */ (function () {
    function RegisteraboutyouPageRoutingModule() {
    }
    RegisteraboutyouPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], RegisteraboutyouPageRoutingModule);
    return RegisteraboutyouPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.module.ts ***!
  \********************************************************************/
/*! exports provided: RegisteraboutyouPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPageModule", function() { return RegisteraboutyouPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _registeraboutyou_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registeraboutyou-routing.module */ "./src/app/signup/registeraboutyou/registeraboutyou-routing.module.ts");
/* harmony import */ var _registeraboutyou_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registeraboutyou.page */ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts");







var RegisteraboutyouPageModule = /** @class */ (function () {
    function RegisteraboutyouPageModule() {
    }
    RegisteraboutyouPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _registeraboutyou_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisteraboutyouPageRoutingModule"]
            ],
            declarations: [_registeraboutyou_page__WEBPACK_IMPORTED_MODULE_6__["RegisteraboutyouPage"]]
        })
    ], RegisteraboutyouPageModule);
    return RegisteraboutyouPageModule;
}());



/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".myselect {\n  height: 50px;\n  outline: none;\n  width: 100%;\n  border: none;\n  padding: 0 15px 0 30px;\n  background-color: transparent;\n  position: relative;\n  z-index: 1;\n  font-size: 14px;\n}\n\n.box-left {\n  background: #37A4DC;\n  padding-right: -10px;\n}\n\n.box-label {\n  background: #CCD1D1;\n  font-size: small;\n}\n\n.label-padding {\n  padding-left: 15px;\n}\n\n.myselect-label {\n  font-size: small;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9yZWdpc3RlcmFib3V0eW91L3JlZ2lzdGVyYWJvdXR5b3UucGFnZS5zY3NzIiwic3JjL2FwcC9zaWdudXAvcmVnaXN0ZXJhYm91dHlvdS9yZWdpc3RlcmFib3V0eW91LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQztFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLDZCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQ0NMOztBREVDO0VBQ0ksbUJBQUE7RUFDQSxvQkFBQTtBQ0NMOztBREVDO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQ0NMOztBREVDO0VBQ0ksa0JBQUE7QUNDTDs7QURFQztFQUtJLGdCQUFBO0FDSEwiLCJmaWxlIjoic3JjL2FwcC9zaWdudXAvcmVnaXN0ZXJhYm91dHlvdS9yZWdpc3RlcmFib3V0eW91LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAubXlzZWxlY3Qge1xuICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgIG91dGxpbmU6IG5vbmU7XG4gICAgIHdpZHRoOiAxMDAlO1xuICAgICBib3JkZXI6IG5vbmU7XG4gICAgIHBhZGRpbmc6IDAgMTVweCAwIDMwcHg7XG4gICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIHotaW5kZXg6IDE7XG4gICAgIGZvbnQtc2l6ZTogMTRweDtcbiB9XG4gXG4gLmJveC1sZWZ0IHtcbiAgICAgYmFja2dyb3VuZDogIzM3QTREQztcbiAgICAgcGFkZGluZy1yaWdodDogLTEwcHg7XG4gfVxuIFxuIC5ib3gtbGFiZWwge1xuICAgICBiYWNrZ3JvdW5kOiAjQ0NEMUQxO1xuICAgICBmb250LXNpemU6IHNtYWxsO1xuIH1cbiBcbiAubGFiZWwtcGFkZGluZyB7XG4gICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiB9XG4gXG4gLm15c2VsZWN0LWxhYmVsIHtcbiAgICAgLy8gIHdpZHRoOiAxMDAlO1xuICAgICAvLyAgaGVpZ2h0OiA0MHB4O1xuICAgICAvLyAgcGFkZGluZzogMThweCAxN3B4IDE4cHggMzBweDtcbiAgICAgLy8gIGJhY2tncm91bmQ6ICNDQ0QxRDE7XG4gICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gfSIsIi5teXNlbGVjdCB7XG4gIGhlaWdodDogNTBweDtcbiAgb3V0bGluZTogbm9uZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgcGFkZGluZzogMCAxNXB4IDAgMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uYm94LWxlZnQge1xuICBiYWNrZ3JvdW5kOiAjMzdBNERDO1xuICBwYWRkaW5nLXJpZ2h0OiAtMTBweDtcbn1cblxuLmJveC1sYWJlbCB7XG4gIGJhY2tncm91bmQ6ICNDQ0QxRDE7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59XG5cbi5sYWJlbC1wYWRkaW5nIHtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4ubXlzZWxlY3QtbGFiZWwge1xuICBmb250LXNpemU6IHNtYWxsO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/signup/registeraboutyou/registeraboutyou.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/signup/registeraboutyou/registeraboutyou.page.ts ***!
  \******************************************************************/
/*! exports provided: RegisteraboutyouPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisteraboutyouPage", function() { return RegisteraboutyouPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var RegisteraboutyouPage = /** @class */ (function () {
    function RegisteraboutyouPage(general, toastCtrl, router, activatedroute, authSvc) {
        this.general = general;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.activatedroute = activatedroute;
        this.authSvc = authSvc;
        this.genderprefer = [];
        this.bodytypearray = [];
        this.userheightarray = [];
        this.userweightarray = [];
        this.userchildarray = [];
        this.usereducationarray = [];
        this.usersmokearray = [];
        this.userDrinksarray = [];
        this.userZodiacarray = [];
    }
    RegisteraboutyouPage.prototype.ngOnInit = function () {
        this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.email = this.activatedroute.snapshot.paramMap.get('email');
        this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.username = this.activatedroute.snapshot.paramMap.get('username');
        this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
        this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
        this.preferredGender = this.activatedroute.snapshot.paramMap.get('preferredGender');
        this.genderSelectedtext = 'Gender';
        this.bodytypetext = 'What\'s your body type?';
        this.userheighttext = 'What\'s your height?';
        this.userchildtext = 'Do you have children?';
        this.usersmoketext = 'do you smoke?';
        this.userdrinkstext = 'do you drink?';
        this.zodiactext = 'zodiac';
        this.showbodytype();
        this.showchild();
        this.showheight();
        this.showsmoke();
        this.showseight();
        this.showgender();
        this.showZodiac();
    };
    RegisteraboutyouPage.prototype.showZodiac = function () {
        this.userZodiacarray = [{ type: 'Aries' }, { type: 'Taurus' }, { type: 'Gemini' },
            { type: 'Cancer' }, { type: 'Leo' }, { type: 'Virgo' },
            { type: 'Libra' }, { type: 'Scorpio' }, { type: 'Sagittarius' },
            { type: 'Capricorn' }, { type: 'Aquarius and Pisces' }];
    };
    RegisteraboutyouPage.prototype.showgender = function () {
        this.genderprefer = [{ gendertype: 'Male' },
            { gendertype: 'Female' }, { gendertype: 'Undecided' }];
    };
    RegisteraboutyouPage.prototype.showseight = function () {
        this.userweightarray = [{ val: '< 100' },
            { val: '< 120' },
            { val: '< 150' },
            { val: '< 200' }
        ];
    };
    RegisteraboutyouPage.prototype.showbodytype = function () {
        this.bodytypearray = [{ bodytype: 'Muscles' },
            { bodytype: 'Athletic' },
            { bodytype: 'Curvy' },
            { bodytype: 'Overweight' },
            { bodytype: 'Underweight' }
        ];
    };
    RegisteraboutyouPage.prototype.bodytypeValue = function (event) {
        console.log(event.detail.value);
        this.bodytypetext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.genderValue = function (event) {
        console.log(event.detail.value);
        this.genderSelectedtext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showheight = function () {
        this.userheightarray = [{ height: '<5 Foot' },
            { height: '>5 Foot' },
            { height: '>6 Foot' },
            { height: '<6 Foot' }
        ];
    };
    RegisteraboutyouPage.prototype.heightValue = function (event) {
        console.log(event.detail.value);
        this.userheighttext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showchild = function () {
        this.userchildarray = [{ child: 'YES' },
            { child: 'NO' }];
    };
    RegisteraboutyouPage.prototype.childValue = function (event) {
        console.log(event.detail.value);
        this.userchildtext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.showsmoke = function () {
        this.usersmokearray = [{ smoke: 'YES' },
            { smoke: 'NO' }
        ];
    };
    RegisteraboutyouPage.prototype.smokeValue = function (event) {
        console.log(event.detail.value);
        this.usersmoketext = event.detail.value;
    };
    RegisteraboutyouPage.prototype.goto_registerfinish = function () {
        var _this = this;
        if (this.userheight == undefined || this.userheight === '') {
            this.general.presentToast('Please select Preference');
        }
        else if (this.userweight == undefined || this.userweight === '') {
            this.general.presentToast('Please select Preference');
        }
        else if (this.userzodiac == undefined || this.userzodiac === '') {
            this.general.presentToast('Please select Preference');
        }
        else if (this.userchild == undefined || this.userchild === '') {
            this.general.presentToast('Please select Preference');
        }
        else if (this.userPreference == undefined || this.userPreference === '') {
            this.general.presentToast('Please select Preference');
        }
        else {
            this.general.presentLoading("Please wait... ");
            this.authSvc.register(this.NickName, this.email, this.userMobile, this.Pass, this.usercity, this.userArea, this.userPincode, this.userState, this.username, this.userBirthday, this.userPhoto, this.preferredGender, this.userbodytype, this.userheight, this.userchild, this.usersmoke, this.userdrinks, this.userzodiac, this.userRace, this.userPreference, this.userAboutme, this.latitude, this.longitude, this.userweight).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var v;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.general.closePresentLoading();
                    console.log(req.data);
                    v = JSON.parse(req.data);
                    console.log(v);
                    if (v.success) {
                        this.router.navigate(['signup/registerfinished', {
                                "userAccount": v.userAccount
                            }]);
                    }
                    else {
                        this.errorToast(v.message);
                    }
                    return [2 /*return*/];
                });
            }); });
            // this.general.presentToast('about Birthday' + this.userBirthday);
            // this.router.navigate(['signup/registersummary', {
            //   NickName: this.NickName, email: this.email, userMobile: this.userMobile, Pass: this.Pass,
            //   usercity: this.usercity, userArea: this.userArea, userPincode: this.userPincode, userState: this.userState,
            //   username: this.username, userBirthday: this.userBirthday, userPhoto: this.userPhoto,
            //   preferredGender: this.preferredGender, userbodytype: this.userbodytype,userweight:this.userweight, userheight: this.userheight, 
            //   userchild: this.userchild,
            //   usersmoke: this.usersmoke, userdrinks: this.userdrinks, userzodiac: this.userzodiac, userRace: this.userRace, userPreference: 
            //   this.userPreference, userAboutme: this.userAboutme,
            //   latitude: this.latitude, longitude: this.longitude
            // }]);
            // this.authSvc.register( this.NickName, this.email, this.userMobile, this.Pass,
            //   this.usercity, this.userArea, this.userPincode,this.userState,
            //   this.username, this.userBirthday, this.userPhoto,
            //   this.preferredGender, this.userbodytype,this.userheight,this.userchild,
            //   this.usersmoke,this.userdrinks,this.userzodiac,this.userRace,this.userPreference,this.userAboutme,
            //   this.latitude, this.longitude).then(async req => {
            //     this.general.closePresentLoading();
            //     console.log(req.data);
            //     var v = JSON.parse(req.data);
            //     console.log(v);
            //     if (v.success) {
            //       this.router.navigate(['signup/registerfinished', {
            //         "NickName":this.NickName, "email":this.email, "userMobile":this.userMobile, "Pass":this.Pass,
            //         "usercity":this.usercity, "userArea":this.userArea, "userPincode":this.userPincode,"userState":this.userState,
            //         "username":this.username, "userBirthday":this.userBirthday, "userPhoto":this.userPhoto,
            //         "preferredGender":this.preferredGender, "userbodytype":this.userbodytype,"userheight":this.userheight,
            // "userchild":this.userchild ,
            //         "usersmoke":this.usersmoke,"userdrinks":this.userdrinks,"userzodiac":this.userzodiac,"userRace":this.userRace,
            // "userPreference":this.userPreference,"userAboutme":this.userAboutme,
            //         "latitude":this.latitude, "longitude":this.longitude
            //       }]);
            //     } else {
            //       this.errorToast(v.message);
            //     }
            //   });
        }
    };
    RegisteraboutyouPage.prototype.errorToast = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            header: 'Registration',
                            color: 'danger',
                            duration: 4000,
                            message: message
                        })];
                    case 1: return [4 /*yield*/, (_a.sent()).present()];
                    case 2:
                        (_a.sent());
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisteraboutyouPage.ctorParameters = function () { return [
        { type: _services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] }
    ]; };
    RegisteraboutyouPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registeraboutyou',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registeraboutyou.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/registeraboutyou/registeraboutyou.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registeraboutyou.page.scss */ "./src/app/signup/registeraboutyou/registeraboutyou.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_general__WEBPACK_IMPORTED_MODULE_2__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], RegisteraboutyouPage);
    return RegisteraboutyouPage;
}());



/***/ })

}]);
//# sourceMappingURL=registeraboutyou-registeraboutyou-module.js.map