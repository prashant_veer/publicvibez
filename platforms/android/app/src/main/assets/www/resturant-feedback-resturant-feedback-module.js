(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resturant-feedback-resturant-feedback-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-feedback/resturant-feedback.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-feedback/resturant-feedback.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\n    <ion-toolbar class=\"new-background-color\">\n        <ion-buttons slot=\"start\" color=\"danger\">\n            <ion-icon name=\"arrow-back-outline\" style=\"font-size: medium;\" (click)=\"back_trending()\"></ion-icon>\n\n        </ion-buttons>\n        <ion-title class=\"ion-text-center\">\n            <b> Restaurant Feedback</b>\n        </ion-title>\n\n    </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div class=\"myrestaurant\">\n        <div class=\"imageSub\" style=\"width: 100%;\">\n            <img src=\"assets/images/rest0.jpg\" alt=\"Something\" />\n            <div class=\"blackbg\"></div>\n            <div class=\"label\">\n                <ion-icon name=\"document-text-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\">\n                </ion-icon>\n                <!-- <ion-icon name=\"create-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\"></ion-icon> -->\n                <ion-icon name=\"heart-circle-outline\" style=\"float: right;font-size: 30px;color:red\"></ion-icon>\n            </div>\n        </div>\n    </div>\n    <br>\n\n    <ion-list lines=\"none\">\n        <ion-list-header>\n            Restaurant Name\n        </ion-list-header>\n\n        <ion-item class=\"feedback-box\">\n\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/images/sg1.jpg\">\n            </ion-avatar>\n            <ion-label class=\"feedback_label\">\n                <h2>Finn</h2>\n                <h3>I'm a big deal</h3>\n                <p>Listen, I've had a pretty messed up day...</p>\n            </ion-label>\n\n        </ion-item>\n\n        <ion-item class=\"feedback-box\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/images/sg1.jpg\">\n            </ion-avatar>\n            <ion-label class=\"feedback_label\">\n                <h2>Han</h2>\n                <h3>Look, kid...</h3>\n                <p>I've got enough on my plate as it is, and I...</p>\n            </ion-label>\n        </ion-item>\n    </ion-list>\n\n    <div class=\"formElement\">\n        <div class=\"ion-textarea-Box\">\n            <ion-grid>\n                <ion-row class=\"ion-text-left\">\n                    <ion-col size=\"12\">\n                        <ion-textarea placeholder=\"Write feedback here in 80 words\" class=\"feed-textbox\"></ion-textarea>\n                    </ion-col>\n\n                </ion-row>\n            </ion-grid>\n        </div>\n    </div>\n\n    <ion-item>\n        <div slot=\"end\">\n            <ion-button color=\"dark\" fill=\"outline\" expand=\"full\" shape=\"round\">Save</ion-button>\n        </div>\n    </ion-item>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/resturant-feedback/resturant-feedback-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/home/resturant-feedback/resturant-feedback-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: ResturantFeedbackPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantFeedbackPageRoutingModule", function() { return ResturantFeedbackPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _resturant_feedback_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resturant-feedback.page */ "./src/app/home/resturant-feedback/resturant-feedback.page.ts");




var routes = [
    {
        path: '',
        component: _resturant_feedback_page__WEBPACK_IMPORTED_MODULE_3__["ResturantFeedbackPage"]
    }
];
var ResturantFeedbackPageRoutingModule = /** @class */ (function () {
    function ResturantFeedbackPageRoutingModule() {
    }
    ResturantFeedbackPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ResturantFeedbackPageRoutingModule);
    return ResturantFeedbackPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/resturant-feedback/resturant-feedback.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/home/resturant-feedback/resturant-feedback.module.ts ***!
  \**********************************************************************/
/*! exports provided: ResturantFeedbackPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantFeedbackPageModule", function() { return ResturantFeedbackPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _resturant_feedback_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./resturant-feedback-routing.module */ "./src/app/home/resturant-feedback/resturant-feedback-routing.module.ts");
/* harmony import */ var _resturant_feedback_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./resturant-feedback.page */ "./src/app/home/resturant-feedback/resturant-feedback.page.ts");







var ResturantFeedbackPageModule = /** @class */ (function () {
    function ResturantFeedbackPageModule() {
    }
    ResturantFeedbackPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _resturant_feedback_routing_module__WEBPACK_IMPORTED_MODULE_5__["ResturantFeedbackPageRoutingModule"]
            ],
            declarations: [_resturant_feedback_page__WEBPACK_IMPORTED_MODULE_6__["ResturantFeedbackPage"]]
        })
    ], ResturantFeedbackPageModule);
    return ResturantFeedbackPageModule;
}());



/***/ }),

/***/ "./src/app/home/resturant-feedback/resturant-feedback.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/home/resturant-feedback/resturant-feedback.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".new-background-color {\n  --background: skyblue;\n}\n\n.feedback-box {\n  margin: 10px;\n  --background: lightgrey;\n  background-image: -webkit-gradient(linear, left top, right bottom, from(white), to(grey));\n  background-image: linear-gradient(to bottom right, white, grey);\n}\n\n.feedback_label {\n  padding: 7px;\n}\n\n.feed-textbox {\n  width: 90%;\n  margin: 15px;\n  border: 1px solid;\n  padding: 7px;\n  font-size: small;\n  color: #AEB6BF;\n}\n\ndiv.imageSub {\n  position: relative;\n}\n\ndiv.imageSub img {\n  z-index: 1;\n}\n\ndiv.imageSub div {\n  position: absolute;\n  left: 1%;\n  right: 1%;\n  bottom: 0;\n  padding: 4px;\n  height: 50px;\n  text-align: center;\n  overflow: hidden;\n}\n\ndiv.imageSub div.blackbg {\n  z-index: 2;\n  background-color: #000;\n  -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\";\n  filter: alpha(opacity=50);\n  opacity: 0.5;\n}\n\ndiv.imageSub div.label {\n  z-index: 3;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL2hvbWUvcmVzdHVyYW50LWZlZWRiYWNrL3Jlc3R1cmFudC1mZWVkYmFjay5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvcmVzdHVyYW50LWZlZWRiYWNrL3Jlc3R1cmFudC1mZWVkYmFjay5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtFQUNBLHVCQUFBO0VBQ0EseUZBQUE7RUFBQSwrREFBQTtBQ0NKOztBREVBO0VBQ0ksWUFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUVBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9ob21lL3Jlc3R1cmFudC1mZWVkYmFjay9yZXN0dXJhbnQtZmVlZGJhY2sucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6IHNreWJsdWU7XG59XG5cbi5mZWVkYmFjay1ib3gge1xuICAgIG1hcmdpbjogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IGxpZ2h0Z3JleTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tIHJpZ2h0LCB3aGl0ZSwgZ3JleSk7XG59XG5cbi5mZWVkYmFja19sYWJlbCB7XG4gICAgcGFkZGluZzogN3B4O1xufVxuXG4uZmVlZC10ZXh0Ym94IHtcbiAgICB3aWR0aDogOTAlO1xuICAgIG1hcmdpbjogMTVweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgZm9udC1zaXplOiBzbWFsbDtcbiAgICBjb2xvcjogI0FFQjZCRjtcbn1cblxuZGl2LmltYWdlU3ViIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmRpdi5pbWFnZVN1YiBpbWcge1xuICAgIHotaW5kZXg6IDE7XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAxJTtcbiAgICByaWdodDogMSU7XG4gICAgYm90dG9tOiAwO1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgLy8gbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYuYmxhY2tiZyB7XG4gICAgei1pbmRleDogMjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xuICAgIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkFscGhhKE9wYWNpdHk9NTApXCI7XG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTUwKTtcbiAgICBvcGFjaXR5OiAwLjU7XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYubGFiZWwge1xuICAgIHotaW5kZXg6IDM7XG4gICAgY29sb3I6IHdoaXRlO1xufSIsIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogc2t5Ymx1ZTtcbn1cblxuLmZlZWRiYWNrLWJveCB7XG4gIG1hcmdpbjogMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiBsaWdodGdyZXk7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20gcmlnaHQsIHdoaXRlLCBncmV5KTtcbn1cblxuLmZlZWRiYWNrX2xhYmVsIHtcbiAgcGFkZGluZzogN3B4O1xufVxuXG4uZmVlZC10ZXh0Ym94IHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAxNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgcGFkZGluZzogN3B4O1xuICBmb250LXNpemU6IHNtYWxsO1xuICBjb2xvcjogI0FFQjZCRjtcbn1cblxuZGl2LmltYWdlU3ViIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG5kaXYuaW1hZ2VTdWIgaW1nIHtcbiAgei1pbmRleDogMTtcbn1cblxuZGl2LmltYWdlU3ViIGRpdiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMSU7XG4gIHJpZ2h0OiAxJTtcbiAgYm90dG9tOiAwO1xuICBwYWRkaW5nOiA0cHg7XG4gIGhlaWdodDogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2LmJsYWNrYmcge1xuICB6LWluZGV4OiAyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xuICAtbXMtZmlsdGVyOiBcInByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5BbHBoYShPcGFjaXR5PTUwKVwiO1xuICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9NTApO1xuICBvcGFjaXR5OiAwLjU7XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYubGFiZWwge1xuICB6LWluZGV4OiAzO1xuICBjb2xvcjogd2hpdGU7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/resturant-feedback/resturant-feedback.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/home/resturant-feedback/resturant-feedback.page.ts ***!
  \********************************************************************/
/*! exports provided: ResturantFeedbackPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResturantFeedbackPage", function() { return ResturantFeedbackPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var ResturantFeedbackPage = /** @class */ (function () {
    function ResturantFeedbackPage(nav) {
        this.nav = nav;
    }
    ResturantFeedbackPage.prototype.ngOnInit = function () {
    };
    ResturantFeedbackPage.prototype.back_trending = function () {
        this.nav.navigateForward('home/tabs/trending');
    };
    ResturantFeedbackPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
    ]; };
    ResturantFeedbackPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-resturant-feedback',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./resturant-feedback.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/resturant-feedback/resturant-feedback.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./resturant-feedback.page.scss */ "./src/app/home/resturant-feedback/resturant-feedback.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], ResturantFeedbackPage);
    return ResturantFeedbackPage;
}());



/***/ })

}]);
//# sourceMappingURL=resturant-feedback-resturant-feedback-module.js.map