(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["otpcheck-otpcheck-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"mainDiv uploadInfo\">\n\n    <div class=\"verifyotp\">\n        <a href=\"\" class=\"round-button-create-profile gradient-background\" style=\"margin-top: 20%;\">\n            <ion-icon name=\"person-outline\" style=\"font-size: 50px;margin-top: 10%;\"></ion-icon>\n        </a>\n        <br>\n        <h2><span>CREATE <b>PROFILE</b></span></h2>\n        <h5 style=\"color: #ffffff;\">A verification code has been sent to your email and phone. <br> Please enter the passcode here.\n        </h5>\n\n\n        <ion-grid>\n            <ion-row>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp1\" pattern=\"[0-9]{6}\" #otp1 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp2)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp2\" pattern=\"[0-9]{6}\" #otp2 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp3)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp3\" pattern=\"[0-9]{6}\" #otp3 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp4)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp4\" pattern=\"[0-9]{6}\" #otp4 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp5)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp5\" pattern=\"[0-9]{6}\" #otp5 class=\"ion-text-center ion-input-class\" maxlength=\"1\" (keyup)=\"next(otp6)\"></ion-input>\n                </ion-col>\n                <ion-col class=\"verifyotpcol\">\n                    <ion-input type=\"number\" [(ngModel)]=\"myotp6\" pattern=\"[0-9]{6}\" #otp6 class=\"ion-text-center ion-input-class\" maxlength=\"1\"></ion-input>\n                </ion-col>\n\n            </ion-row>\n        </ion-grid>\n\n        <ion-grid>\n            <ion-row class=\"ion-text-center\">\n                <ion-col style=\"text-align: right;\">\n                    <a href=\"JavaScript:void(0)\" (click)=\"goto_resendOtp()\">\n                        <!-- Resend OTP? -->\n                        Didn't receive a passcode? Resend\n                    </a>\n                </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-text-center\">\n                <ion-col>\n                    <a href=\"JavaScript:void(0)\" class=\"round-button gradient-background\" (click)=\"goto_getstart()\">\n                        <ion-icon color=\"light\" name=\"arrow-forward-outline\" style=\"font-size: 30px;margin-top: 15%;\"></ion-icon>\n                    </a>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n    </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/services/loading.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/loading.service.ts ***!
  \*********************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var LoadingService = /** @class */ (function () {
    function LoadingService(loadingController) {
        this.loadingController = loadingController;
        this.isLoading = false;
    }
    LoadingService.prototype.present = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingController.create({
                                duration: 5000,
                                spinner: 'circles',
                            }).then(function (a) {
                                a.present().then(function () {
                                    // console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoadingService.prototype.dismiss = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingController.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoadingService.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
    ]; };
    LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoadingService);
    return LoadingService;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck-routing.module.ts ***!
  \************************************************************/
/*! exports provided: OtpcheckPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPageRoutingModule", function() { return OtpcheckPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _otpcheck_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otpcheck.page */ "./src/app/signup/otpcheck/otpcheck.page.ts");




var routes = [
    {
        path: '',
        component: _otpcheck_page__WEBPACK_IMPORTED_MODULE_3__["OtpcheckPage"]
    }
];
var OtpcheckPageRoutingModule = /** @class */ (function () {
    function OtpcheckPageRoutingModule() {
    }
    OtpcheckPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], OtpcheckPageRoutingModule);
    return OtpcheckPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.module.ts":
/*!****************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.module.ts ***!
  \****************************************************/
/*! exports provided: OtpcheckPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPageModule", function() { return OtpcheckPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _otpcheck_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./otpcheck-routing.module */ "./src/app/signup/otpcheck/otpcheck-routing.module.ts");
/* harmony import */ var _otpcheck_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./otpcheck.page */ "./src/app/signup/otpcheck/otpcheck.page.ts");







var OtpcheckPageModule = /** @class */ (function () {
    function OtpcheckPageModule() {
    }
    OtpcheckPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _otpcheck_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtpcheckPageRoutingModule"]
            ],
            declarations: [_otpcheck_page__WEBPACK_IMPORTED_MODULE_6__["OtpcheckPage"]]
        })
    ], OtpcheckPageModule);
    return OtpcheckPageModule;
}());



/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.page.scss":
/*!****************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".verifyotp {\n  margin: 40px 20px;\n}\n\n.verifyotp h5 {\n  text-align: center;\n}\n\n.verifyotp p {\n  text-align: center;\n  font-size: 12px;\n}\n\n.verifyotpcol {\n  border-radius: 40px;\n  margin: 2px 2px 20px 2px;\n}\n\n.btn {\n  --background: rgb(212, 9, 9);\n  border-radius: 5px;\n  --color: #fff;\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n}\n\n.btn1 {\n  border: 2px solid #f1c732;\n  border-radius: 5px;\n  color: #d40909;\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n  margin-top: 10px;\n}\n\n.btn2 {\n  font-family: Helvetica;\n  font-weight: 600;\n  font-size: 13px;\n  margin-top: 10px;\n  --background: rgb(241, 199, 50);\n  color: #d40909;\n}\n\n#time {\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3NpZ251cC9vdHBjaGVjay9vdHBjaGVjay5wYWdlLnNjc3MiLCJzcmMvYXBwL3NpZ251cC9vdHBjaGVjay9vdHBjaGVjay5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVBO0VBRUksbUJBQUE7RUFDQSx3QkFBQTtBQ0FKOztBREdBO0VBQ0ksNEJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0FKOztBREdBO0VBRUkseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDREo7O0FESUE7RUFDSSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsK0JBQUE7RUFDQSxjQUFBO0FDREo7O0FESUE7RUFDSSxlQUFBO0FDREoiLCJmaWxlIjoic3JjL2FwcC9zaWdudXAvb3RwY2hlY2svb3RwY2hlY2sucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnZlcmlmeW90cCB7XG4gICAgbWFyZ2luOiA0MHB4IDIwcHg7XG59XG5cbi52ZXJpZnlvdHAgaDUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnZlcmlmeW90cCBwIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4udmVyaWZ5b3RwY29sIHtcbiAgICAvLyBib3JkZXI6IDJweCBzb2xpZCByZ2IoMjEyLCA5LCA5KTtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIG1hcmdpbjogMnB4IDJweCAyMHB4IDJweDtcbn1cblxuLmJ0biB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMjEyLCA5LCA5KTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1jb2xvcjogI2ZmZjtcbiAgICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uYnRuMSB7XG4gICAgLy8gLS1iYWNrZ3JvdW5kOiByZ2IoMjEyLCA5LCA5KTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCByZ2IoMjQxLCAxOTksIDUwKTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6IHJnYigyMTIsIDksIDkpO1xuICAgIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuLmJ0bjIge1xuICAgIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IHJnYigyNDEsIDE5OSwgNTApO1xuICAgIGNvbG9yOiByZ2IoMjEyLCA5LCA5KTtcbn1cblxuI3RpbWUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn0iLCIudmVyaWZ5b3RwIHtcbiAgbWFyZ2luOiA0MHB4IDIwcHg7XG59XG5cbi52ZXJpZnlvdHAgaDUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi52ZXJpZnlvdHAgcCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4udmVyaWZ5b3RwY29sIHtcbiAgYm9yZGVyLXJhZGl1czogNDBweDtcbiAgbWFyZ2luOiAycHggMnB4IDIwcHggMnB4O1xufVxuXG4uYnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2IoMjEyLCA5LCA5KTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAtLWNvbG9yOiAjZmZmO1xuICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5idG4xIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2YxYzczMjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBjb2xvcjogI2Q0MDkwOTtcbiAgZm9udC1mYW1pbHk6IEhlbHZldGljYTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uYnRuMiB7XG4gIGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiByZ2IoMjQxLCAxOTksIDUwKTtcbiAgY29sb3I6ICNkNDA5MDk7XG59XG5cbiN0aW1lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/signup/otpcheck/otpcheck.page.ts":
/*!**************************************************!*\
  !*** ./src/app/signup/otpcheck/otpcheck.page.ts ***!
  \**************************************************/
/*! exports provided: OtpcheckPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpcheckPage", function() { return OtpcheckPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/loading.service */ "./src/app/services/loading.service.ts");
/* harmony import */ var _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/present-toast.service */ "./src/app/services/present-toast.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _services_general__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/general */ "./src/app/services/general.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");








var OtpcheckPage = /** @class */ (function () {
    function OtpcheckPage(router, activatedroute, nav, gen, presentToast, authSvc, loadingservice) {
        this.router = router;
        this.activatedroute = activatedroute;
        this.nav = nav;
        this.gen = gen;
        this.presentToast = presentToast;
        this.authSvc = authSvc;
        this.loadingservice = loadingservice;
    }
    OtpcheckPage.prototype.next = function (el) {
        el.setFocus();
    };
    OtpcheckPage.prototype.ngOnInit = function () {
        // this.myotp1.focus();
        this.smobilenumber = this.activatedroute.snapshot.paramMap.get('mobile');
        this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
        this.userNickName = this.activatedroute.snapshot.paramMap.get('NickName');
        this.userPass = this.activatedroute.snapshot.paramMap.get('Pass');
        this.newotp = this.activatedroute.snapshot.paramMap.get('otp');
        this.autolocation = this.activatedroute.snapshot.paramMap.get('location');
        this.userArea = this.activatedroute.snapshot.paramMap.get('area');
        this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
        this.userState = this.activatedroute.snapshot.paramMap.get('state');
        this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
        this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
        this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    };
    OtpcheckPage.prototype.goto_getstart = function () {
        this.confirmOtp = this.myotp1 + '' + this.myotp2 + '' + this.myotp3 + '' + this.myotp4 + '' + this.myotp5 + '' + this.myotp6;
        if (this.confirmOtp !== this.newotp) {
            this.presentToast.presentToast('Please enter correct passcode');
            this.myotp1 = '';
            this.myotp2 = '';
            this.myotp3 = '';
            this.myotp4 = '';
            this.myotp5 = '';
            this.myotp6 = '';
        }
        else {
            this.nav.navigateForward(['signup/gettingstart', {
                    email: this.userEmail,
                    NickName: this.userNickName,
                    mobile: this.smobilenumber,
                    Pass: this.userPass,
                    location: this.autolocation,
                    area: this.userArea,
                    pincode: this.userPincode,
                    state: this.userState,
                    usercity: this.usercity,
                    latitude: this.latitude,
                    longitude: this.longitude
                }]);
        }
    };
    OtpcheckPage.prototype.goto_resendOtp = function () {
        var _this = this;
        this.gen.presentLoading("");
        this.authSvc.sendotptoEmail(this.userEmail, this.smobilenumber, this.userNickName).then(function (req) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var v;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.gen.closePresentLoading();
                v = JSON.parse(req.data);
                if (v.success) {
                    this.newotp = v.passcode;
                }
                return [2 /*return*/];
            });
        }); });
    };
    OtpcheckPage.prototype.ionViewWillLeave = function () {
        this.gen.closePresentLoading();
    };
    OtpcheckPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
        { type: _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"] },
        { type: _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__["PresentToastService"] },
        { type: _services_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"] },
        { type: _services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"] }
    ]; };
    OtpcheckPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-otpcheck',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./otpcheck.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/signup/otpcheck/otpcheck.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./otpcheck.page.scss */ "./src/app/signup/otpcheck/otpcheck.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _services_general__WEBPACK_IMPORTED_MODULE_6__["GeneralService"],
            _services_present_toast_service__WEBPACK_IMPORTED_MODULE_4__["PresentToastService"], _services_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"], _services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"]])
    ], OtpcheckPage);
    return OtpcheckPage;
}());



/***/ })

}]);
//# sourceMappingURL=otpcheck-otpcheck-module.js.map