(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["popup-message-popup-message-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/popup-message/popup-message.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/popup-message/popup-message.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n    <div class=\"container\">\n\n        You are now eligible for <br/><br/> half of the first two months <br/><br/> when upgrading to <br/><br/>\n        <strong>Vibez Premium</strong> just by <br/> <br/>Pre Registering with us.<br/><br/>\n\n        <ion-button (click)=\"close()\" color=\"light\" style=\"border:3px solid #000000; border-radius: 15px;\">Close</ion-button>\n\n\n\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/popup-message/popup-message-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/popup-message/popup-message-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: PopupMessagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupMessagePageRoutingModule", function() { return PopupMessagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _popup_message_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./popup-message.page */ "./src/app/popup-message/popup-message.page.ts");




var routes = [
    {
        path: '',
        component: _popup_message_page__WEBPACK_IMPORTED_MODULE_3__["PopupMessagePage"]
    }
];
var PopupMessagePageRoutingModule = /** @class */ (function () {
    function PopupMessagePageRoutingModule() {
    }
    PopupMessagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], PopupMessagePageRoutingModule);
    return PopupMessagePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/popup-message/popup-message.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/popup-message/popup-message.module.ts ***!
  \*******************************************************/
/*! exports provided: PopupMessagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupMessagePageModule", function() { return PopupMessagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _popup_message_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./popup-message-routing.module */ "./src/app/popup-message/popup-message-routing.module.ts");
/* harmony import */ var _popup_message_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./popup-message.page */ "./src/app/popup-message/popup-message.page.ts");







var PopupMessagePageModule = /** @class */ (function () {
    function PopupMessagePageModule() {
    }
    PopupMessagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _popup_message_routing_module__WEBPACK_IMPORTED_MODULE_5__["PopupMessagePageRoutingModule"]
            ],
            declarations: [_popup_message_page__WEBPACK_IMPORTED_MODULE_6__["PopupMessagePage"]]
        })
    ], PopupMessagePageModule);
    return PopupMessagePageModule;
}());



/***/ }),

/***/ "./src/app/popup-message/popup-message.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/popup-message/popup-message.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #FBFCFC;\n}\n\n.container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n  font-family: \"ClementePDae-Light\";\n}\n\n.container strong {\n  font-family: \"ClementePDao\";\n  font-size: 20px;\n  line-height: 26px;\n}\n\n.container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #000000;\n  margin: 0;\n}\n\n.container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL3BvcHVwLW1lc3NhZ2UvcG9wdXAtbWVzc2FnZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BvcHVwLW1lc3NhZ2UvcG9wdXAtbWVzc2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtBQ0NKOztBREdBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7RUFDQSxpQ0FBQTtBQ0FKOztBREdBO0VBQ0ksMkJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FDQUo7O0FER0E7RUFDSSxxQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvcG9wdXAtbWVzc2FnZS9wb3B1cC1tZXNzYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6ICNGQkZDRkM7XG4gICAgLy8gZm9udC1mYW1pbHk6ICdDYXZpYXJEcmVhbXMnO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIGZvbnQtZmFtaWx5OiAnQ2xlbWVudGVQRGFlLUxpZ2h0Jztcbn1cblxuLmNvbnRhaW5lciBzdHJvbmcge1xuICAgIGZvbnQtZmFtaWx5OiAnQ2xlbWVudGVQRGFvJztcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDI2cHg7XG59XG5cbi5jb250YWluZXIgcCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICAgIGNvbG9yOiAjMDAwMDAwO1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmNvbnRhaW5lciBhIHtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59IiwiaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGQkZDRkM7XG59XG5cbi5jb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIGZvbnQtZmFtaWx5OiBcIkNsZW1lbnRlUERhZS1MaWdodFwiO1xufVxuXG4uY29udGFpbmVyIHN0cm9uZyB7XG4gIGZvbnQtZmFtaWx5OiBcIkNsZW1lbnRlUERhb1wiO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4uY29udGFpbmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBjb2xvcjogIzAwMDAwMDtcbiAgbWFyZ2luOiAwO1xufVxuXG4uY29udGFpbmVyIGEge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59Il19 */");

/***/ }),

/***/ "./src/app/popup-message/popup-message.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/popup-message/popup-message.page.ts ***!
  \*****************************************************/
/*! exports provided: PopupMessagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupMessagePage", function() { return PopupMessagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var PopupMessagePage = /** @class */ (function () {
    function PopupMessagePage(modalController, navParams, popoverController) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.popoverController = popoverController;
    }
    PopupMessagePage.prototype.ngOnInit = function () {
    };
    PopupMessagePage.prototype.close = function () {
        this.modalController.dismiss({
            'dismissed': true
        });
        // this.popoverController.dismiss();
        // const onClosedData = 'Wrapped Up!';
        // await this.modalController.dismiss(onClosedData);
    };
    PopupMessagePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] }
    ]; };
    PopupMessagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-popup-message',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./popup-message.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/popup-message/popup-message.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./popup-message.page.scss */ "./src/app/popup-message/popup-message.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"]])
    ], PopupMessagePage);
    return PopupMessagePage;
}());



/***/ })

}]);
//# sourceMappingURL=popup-message-popup-message-module.js.map