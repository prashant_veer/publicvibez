(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["trending-trending-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/trending/trending.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/trending/trending.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n    <div class=\"myrestaurant\">\n        <ion-item>\n            <div slot=\"start\">\n                <ion-label> RESTAURANT NAME</ion-label>\n            </div>\n            <div slot=\"end\">\n                <ion-button color=\"dark\" fill=\"outline\" expand=\"full\" shape=\"round\">Take me there</ion-button>\n            </div>\n        </ion-item>\n        <div class=\"imageSub\" style=\"width: 100%;\">\n            <img src=\"assets/images/rest0.jpg\" alt=\"Something\" />\n            <div class=\"blackbg\"></div>\n            <div class=\"label\">\n                <ion-icon name=\"document-text-outline\" (click)=\"rest_feedback()\" style=\"float: left;font-size: 30px;padding-left: 10px;\">\n                </ion-icon>\n                <ion-icon name=\"create-outline\" (click)=\"rest_details()\" style=\"float: left;font-size: 30px;padding-left: 10px;\"></ion-icon>\n                <ion-icon name=\"heart-circle-outline\" style=\"float: right;font-size: 30px;color:red\"></ion-icon>\n            </div>\n        </div>\n    </div>\n\n\n    <div class=\"myrestaurant\">\n        <ion-item>\n            <div slot=\"start\">\n                <ion-label> RESTAURANT NAME</ion-label>\n            </div>\n            <div slot=\"end\">\n                <ion-button color=\"dark\" fill=\"outline\" expand=\"full\" shape=\"round\">Take me there</ion-button>\n            </div>\n        </ion-item>\n        <div class=\"imageSub\" style=\"width: 100%;\">\n            <img src=\"assets/images/rest1.jpg\" alt=\"Something\" />\n            <div class=\"blackbg\"></div>\n            <div class=\"label\">\n                <ion-icon name=\"document-text-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\">\n                </ion-icon>\n                <ion-icon name=\"create-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\"></ion-icon>\n                <ion-icon name=\"heart-circle-outline\" style=\"float: right;font-size: 30px;color:red\"></ion-icon>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"myrestaurant\">\n        <ion-item>\n            <div slot=\"start\">\n                <ion-label> RESTAURANT NAME</ion-label>\n            </div>\n            <div slot=\"end\">\n                <ion-button color=\"dark\" fill=\"outline\" expand=\"full\" shape=\"round\">Take me there</ion-button>\n            </div>\n        </ion-item>\n        <div class=\"imageSub\" style=\"width: 100%;\">\n            <img src=\"assets/images/rest2.jpg\" alt=\"Something\" />\n            <div class=\"blackbg\"></div>\n            <div class=\"label\">\n                <ion-icon name=\"document-text-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\">\n                </ion-icon>\n                <ion-icon name=\"create-outline\" style=\"float: left;font-size: 30px;padding-left: 10px;\"></ion-icon>\n                <ion-icon name=\"heart-circle-outline\" style=\"float: right;font-size: 30px;color:red\"></ion-icon>\n            </div>\n        </div>\n    </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/home/trending/trending-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/home/trending/trending-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: TrendingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendingPageRoutingModule", function() { return TrendingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _trending_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./trending.page */ "./src/app/home/trending/trending.page.ts");




var routes = [
    {
        path: '',
        component: _trending_page__WEBPACK_IMPORTED_MODULE_3__["TrendingPage"]
    }
];
var TrendingPageRoutingModule = /** @class */ (function () {
    function TrendingPageRoutingModule() {
    }
    TrendingPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], TrendingPageRoutingModule);
    return TrendingPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/trending/trending.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home/trending/trending.module.ts ***!
  \**************************************************/
/*! exports provided: TrendingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendingPageModule", function() { return TrendingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");
/* harmony import */ var _trending_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./trending-routing.module */ "./src/app/home/trending/trending-routing.module.ts");
/* harmony import */ var _trending_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./trending.page */ "./src/app/home/trending/trending.page.ts");







var TrendingPageModule = /** @class */ (function () {
    function TrendingPageModule() {
    }
    TrendingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _trending_routing_module__WEBPACK_IMPORTED_MODULE_5__["TrendingPageRoutingModule"]
            ],
            declarations: [_trending_page__WEBPACK_IMPORTED_MODULE_6__["TrendingPage"]]
        })
    ], TrendingPageModule);
    return TrendingPageModule;
}());



/***/ }),

/***/ "./src/app/home/trending/trending.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home/trending/trending.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("div.imageSub {\n  position: relative;\n}\n\ndiv.imageSub img {\n  z-index: 1;\n}\n\ndiv.imageSub div {\n  position: absolute;\n  left: 1%;\n  right: 1%;\n  bottom: 0;\n  padding: 4px;\n  height: 50px;\n  text-align: center;\n  overflow: hidden;\n}\n\ndiv.imageSub div.blackbg {\n  z-index: 2;\n  background-color: #000;\n  -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\";\n  filter: alpha(opacity=50);\n  opacity: 0.5;\n}\n\ndiv.imageSub div.label {\n  z-index: 3;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9wcmFzYWRnb2toYWxlL0RvY3VtZW50cy9kZXZlbG9wbWVudC9Jb25pY19TdHVkaW9fQXBwcy92aWJlemFwcC9NYXktZ2l0L3B1YmxpY3ZpYmV6X25ldy9zcmMvYXBwL2hvbWUvdHJlbmRpbmcvdHJlbmRpbmcucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL3RyZW5kaW5nL3RyZW5kaW5nLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxVQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0ksVUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUVBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9ob21lL3RyZW5kaW5nL3RyZW5kaW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImRpdi5pbWFnZVN1YiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG5kaXYuaW1hZ2VTdWIgaW1nIHtcbiAgICB6LWluZGV4OiAxO1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMSU7XG4gICAgcmlnaHQ6IDElO1xuICAgIGJvdHRvbTogMDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIC8vIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2LmJsYWNrYmcge1xuICAgIHotaW5kZXg6IDI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcbiAgICAtbXMtZmlsdGVyOiBcInByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5BbHBoYShPcGFjaXR5PTUwKVwiO1xuICAgIGZpbHRlcjogYWxwaGEob3BhY2l0eT01MCk7XG4gICAgb3BhY2l0eTogMC41O1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2LmxhYmVsIHtcbiAgICB6LWluZGV4OiAzO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn0iLCJkaXYuaW1hZ2VTdWIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmRpdi5pbWFnZVN1YiBpbWcge1xuICB6LWluZGV4OiAxO1xufVxuXG5kaXYuaW1hZ2VTdWIgZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxJTtcbiAgcmlnaHQ6IDElO1xuICBib3R0b206IDA7XG4gIHBhZGRpbmc6IDRweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbmRpdi5pbWFnZVN1YiBkaXYuYmxhY2tiZyB7XG4gIHotaW5kZXg6IDI7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gIC1tcy1maWx0ZXI6IFwicHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkFscGhhKE9wYWNpdHk9NTApXCI7XG4gIGZpbHRlcjogYWxwaGEob3BhY2l0eT01MCk7XG4gIG9wYWNpdHk6IDAuNTtcbn1cblxuZGl2LmltYWdlU3ViIGRpdi5sYWJlbCB7XG4gIHotaW5kZXg6IDM7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/trending/trending.page.ts":
/*!************************************************!*\
  !*** ./src/app/home/trending/trending.page.ts ***!
  \************************************************/
/*! exports provided: TrendingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrendingPage", function() { return TrendingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm5/ionic-angular.js");



var TrendingPage = /** @class */ (function () {
    function TrendingPage(nav) {
        this.nav = nav;
    }
    TrendingPage.prototype.ngOnInit = function () {
    };
    TrendingPage.prototype.rest_feedback = function () {
        this.nav.navigateForward('home/tabs/resturant-details');
    };
    TrendingPage.prototype.rest_details = function () {
        this.nav.navigateForward('home/tabs/resturant-feedback');
    };
    TrendingPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
    ]; };
    TrendingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-trending',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./trending.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/trending/trending.page.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./trending.page.scss */ "./src/app/home/trending/trending.page.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], TrendingPage);
    return TrendingPage;
}());



/***/ })

}]);
//# sourceMappingURL=trending-trending-module.js.map