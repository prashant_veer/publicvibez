import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';


@Component({
  selector: 'app-modalwin',
  templateUrl: './modalwin.component.html',
  styleUrls: ['./modalwin.component.scss'],
})
export class ModalwinComponent implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {}

  close() {
    this.modalController.dismiss({
      'dismissed': true
    });

    // this.popoverController.dismiss();
    // const onClosedData = 'Wrapped Up!';
    // await this.modalController.dismiss(onClosedData);
  }


}
