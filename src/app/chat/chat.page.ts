import { Component, OnInit,ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import {  ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { ModalController,AlertController } from '@ionic/angular';
import { PhotoVerificationPage } from './photo-verification/photo-verification.page';
import { GeneralService } from '../services/general';
import { VerificationPage } from './verification/verification.page';

declare var firebase: any;

export const snapshotToArray = snapshot => {
  const returnArr = [];

  snapshot.forEach(childSnapshot => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  @ViewChild(IonContent, { static: false }) content: IonContent;
  roomkey: string;
  nickname: string;
  offStatus = false;
  custname: string;
  requirement: string;
  temporderMasterId: any;
  pageType:string = "0";

  chat = { type: '', nickname: '', message: '' };
  chats = [];
  public reqDetail:any;
  ref = firebase.database().ref('chatrooms/');
  chatInter:any;
  constructor(public alertController: AlertController,public general:GeneralService, public modalCont:ModalController, public _userser:UserService, private activatedroute: ActivatedRoute) {
    this.reqDetail = JSON.parse(this.activatedroute.snapshot.paramMap.get('userProfile')) ;
    this.pageType = this.activatedroute.snapshot.paramMap.get('pageType') ;
    this.roomkey = this.reqDetail.chat_guid;
    this.nickname = this.reqDetail.name;
    this.chat.type = 'message';
    this.chat.nickname = this.nickname;
    
    this.chat.message = '';

    firebase.database().ref('chatrooms/' + this.roomkey + '/chats').on('value', resp => {
      this.chats = [];
      this.chats = snapshotToArray(resp);
      console.log(this.chats);
      setTimeout(() => {
        if (this.offStatus === false) {
          this.content.scrollToBottom(300);
        }
      }, 1000);
    });


  }

  ngOnInit(){

  }

  ionViewWillEnter(){
    var obj = this;
    obj.setInterval();
  }
  ionViewWillLeave(){
    var obj = this;
    clearInterval(obj.chatInter);
  }
  setInterval(){
    var obj = this;
    obj.chatInter = setInterval(function(){
      console.log("setInterval");
      obj._userser.checkPhoto(obj.reqDetail).then(async req => {
        console.log(req);
        if(req.data){
          var v= JSON.parse(req.data);
          if (v.success) {
            if(v.data.status == 0 && v.data.user_id==obj.general.getUserInfo().user_id){
              obj.presentModal(PhotoVerificationPage,v.data);
            }else if(v.data.status == 1 && v.data.user_id!=obj.general.getUserInfo().user_id){
              obj.presentModal(VerificationPage,v.data);
            }
            clearInterval(obj.chatInter);
          } 
        }
      });
    },10000);
  }
  async sendInvitation(){
    var obj = this;
    if(obj.general.getUserInfo(). isPad==0){
      const alert = await this.alertController.create({
        header: 'Confirm!',
        message: 'Photo varification cost: 0.99$',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary'
          }, {
            text: 'Okay',
            handler: () => {
              obj.sendAPIrequest();
            }
          }
        ]
      });
  
      await alert.present();
    }else{
      obj.sendAPIrequest();
    }
  }
  sendAPIrequest(){
    var obj = this;
    obj._userser.sendPhotoVarifi(obj.reqDetail).then(async req => {
      console.log(req);
      if(req.data){
        var v= JSON.parse(req.data);
        if (v.success) {
          obj.general.presentToast('Send photo verification invitation!');
        } 
      }
    });
  }
  async presentModal(com,request) {
    var obj = this;
    const modal = await this.modalCont.create({
      component: com,
      componentProps:{
        userInfo:request
      }
    });
    modal.onDidDismiss().then(()=>{obj.setInterval();});
    return await modal.present();
  }
  sendMessage() {
    const newData = firebase
      .database()
      .ref('chatrooms/' + this.roomkey + '/chats')
      .push();
    newData.set({
      // type: type,
      type: this.chat.type,
      user: this.chat.nickname,
      message: this.chat.message,

      // user: this.data.nickname,
      // message: this.data.message,
      sendDate: Date()
    });
    // this.data.message = '';
    this.chat.message = '';
    this.scrollBottom();
  }
  scrollBottom() {
    const that = this;
    setTimeout(function() {
      that.content.scrollToBottom();
    }, 300);
  }

}
