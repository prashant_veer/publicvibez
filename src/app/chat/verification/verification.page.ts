import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';
import { ModalController,ToastController } from '@ionic/angular';
@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss'],
})
export class VerificationPage implements OnInit {
  userInfo:any;
  public url = environment.root_URL;
  constructor(navParams: NavParams,public _userser:UserService,public modalCtrl: ModalController) { 
    this.userInfo = navParams.get('userInfo')
  }
  onclickClose(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  ngOnInit() {
  }
  approveChat(){
    var obj = this;
    obj._userser.setPhotoStatus(obj.userInfo.photo_verify_id,2).then( req => { obj.onclickClose(); });
  }
  rejectChat(){
    var obj = this;
    obj._userser.setPhotoStatus(obj.userInfo.photo_verify_id,3).then( req => { obj.onclickClose(); });
  }
}
