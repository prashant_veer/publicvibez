import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ChatPageRoutingModule } from './chat-routing.module';
import { ChatPage } from './chat.page';
import { PhotoVerificationPage } from './photo-verification/photo-verification.page';
import { VerificationPage } from './verification/verification.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatPageRoutingModule
  ],
  declarations: [ChatPage,PhotoVerificationPage,VerificationPage],
  entryComponents:[PhotoVerificationPage,VerificationPage]
})
export class ChatPageModule {}
