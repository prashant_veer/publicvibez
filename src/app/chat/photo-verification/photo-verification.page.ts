import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController,ToastController,ActionSheetController,Platform } from '@ionic/angular';
import { GeneralService } from '../../services/general';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { UserService } from '../../services/user.service';
import { LoginService } from '../../services/login.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';

@Component({
  selector: 'app-photo-verification',
  templateUrl: './photo-verification.page.html',
  styleUrls: ['./photo-verification.page.scss'],
})
export class PhotoVerificationPage implements OnInit {
  userInfo:any;
  images:any=[];
  constructor(private webview: WebView,private platform: Platform,private camera: Camera,public actionSheetController: ActionSheetController,private toastCtrl: ToastController,private authSvc: LoginService,public _userser:UserService,private file: File,private filePath: FilePath,public gen:GeneralService,navParams: NavParams,public modalCtrl: ModalController) { 
    this.userInfo = navParams.get('userInfo')
  }
  onclickClose(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  ngOnInit() {
  }
  deleteImage(imgEntry: { name: string; filePath: string; }, position: number) {
    this.images = [];
  }
  verifyNow() {
    this.gen.presentLoading('Uploading the image');
    var imgEntry = this.images[0];
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (entry as FileEntry).file(file => this.readFile(file));
      })
      .catch(err => {
        this.gen.presentToast('Error while reading file.');
      });
  }

  readFile(file: any) {
    const reader = new FileReader();
    reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append('file', imgBlob, file.name);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

   uploadImageData(formData: FormData) {
     var obj = this;
     obj.authSvc.registerImage(formData).then( req => {
      console.log(req);
      obj.gen.closePresentLoading();
      var v = JSON.parse(req.data);

      if (v.success) {
        obj._userser.addPhotos(obj.userInfo,v.data).then( req => {
          obj.onclickClose();
         });
      } else {
        obj.images=[];
        obj.errorToast(v.message);
      }
      obj.gen.closePresentLoading();
    });
  }
  private async errorToast(message) {
    (await (await this.toastCtrl.create({
        header: 'Registration',
        color: 'danger',
        duration: 4000,
        message
    })).present());
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Photo From',
      buttons: [{
        text: 'Load from Library',
        role: 'destructive',
        icon: 'images',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          console.log('Delete clicked');
        }
      }, {
        text: 'Use Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          console.log('Share clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }
  createFileName() {
    const d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg';
    return newFileName;
  }
  copyFileToLocalDir(namePath: string, currentName: string, newFileName: string) {

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.gen.presentToast('Error while storing file.');
    });
  }

  updateStoredImages(name: string) {
    const filePath = this.file.dataDirectory + name;
      const resPath = this.pathForImage(filePath);

      const newEntry = {
        name,
        path: resPath,
        filePath
      };

      this.images = [newEntry, ...this.images];
  }
  pathForImage(img: any) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

}
