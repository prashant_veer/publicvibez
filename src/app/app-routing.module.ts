import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { SignupAuthGuard } from './services/AuthGuardService';
import { LoggedAuthGuard } from './services/LoggedAuthGuardService';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },{
    path: 'landingpage',
    component:LandingpageComponent
  },{
    path: 'prelogin',
    loadChildren: () => import('./prelogin/prelogin.module').then( m => m.PreloginPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    canActivate: [SignupAuthGuard],
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },{
    path: 'home',
    canActivate: [LoggedAuthGuard],
    children:[
      {
        path: '',
        loadChildren: () => import('./home/tabs/tabs.module').then(m => m.TabsPageModule)
      },
      {
        path: 'editprofile',
        loadChildren: () => import('./myProfile/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
      },
      {
        path: 'gallery',
        loadChildren: () => import('./myProfile/photo-gallery/photo-gallery.module').then( m => m.PhotoGalleryPageModule)
      },
      {
        path: 'map-component',
        loadChildren: () => import('./map-component/map-component.module').then( m => m.MapComponentPageModule)
      },
      {
        path: 'chat',
        loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule)
      },
      {
        path: 'trending',
        loadChildren: () => import('./home/trending/trending.module').then( m => m.TrendingPageModule)
      },
      {
        path: 'vibezlist',
        loadChildren: () => import('./home/vibezlist/vibezlist.module').then( m => m.VibezlistPageModule)
      },
      {
        path: 'livelist',
        loadChildren: () => import('./home/livelist/livelist.module').then( m => m.LivelistPageModule)
      },
      {
        path: 'resturant-details',
        loadChildren: () => import('./home/resturant-details/resturant-details.module').then( m => m.ResturantDetailsPageModule)
      },
      {
        path: 'resturant-feedback',
        loadChildren: () => import('./home/resturant-feedback/resturant-feedback.module').then( m => m.ResturantFeedbackPageModule)
      },
      {
        path: 'otheruser-profile',
        loadChildren: () => import('./home/otheruser-profile/otheruser-profile.module').then( m => m.OtheruserProfilePageModule)
      }
    ]
  },
  {
    path: 'profileother',
    loadChildren: () => import('./home/profileother/profileother.module').then( m => m.ProfileotherPageModule)
  },
  {
    path: 'postlogin',
    loadChildren: () => import('./postlogin/postlogin.module').then( m => m.PostloginPageModule)
  },
  {
    path: 'popup-message',
    loadChildren: () => import('./popup-message/popup-message.module').then( m => m.PopupMessagePageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
