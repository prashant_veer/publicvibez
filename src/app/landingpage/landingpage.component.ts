import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavController } from '@ionic/angular';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss'],
})
export class LandingpageComponent implements OnInit {

  constructor(private router: Router,private nav:NavController) { }

  ngOnInit() {
    var obj =this;
    if(localStorage.getItem("userInfo")!=null){ 
      obj.nav.navigateForward('/login');
    }
  }
  goto_prelogin() {
    this.nav.navigateForward("login");
  }
}
