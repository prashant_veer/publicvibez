import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable, forkJoin } from 'rxjs';
import { environment } from '../../environments/environment';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  urlMain: any;

  constructor(public http: HTTP, public file: File) {
    this.http.setDataSerializer('urlencoded');
  }
  sendotptoEmail(userEmail, userMobile, userNickName) {
    this.http.setDataSerializer('urlencoded');
    var formData = {
      "userEmail": userEmail,
      "userMobile": userMobile,
      "userNickName": userNickName
    };
    return this.http.post(environment.urls.auth.sendOtptoEmail(), formData, {});
  }

  getuserCount(userEmail) {
    this.http.setDataSerializer('urlencoded');
    var formData = {
      "userEmail": userEmail,
    };
    return this.http.post(environment.urls.user.count(), formData, {});
  }

  login(mobile, password, token) {
    this.http.setDataSerializer('urlencoded');
    var formData = { "username": mobile, "password": password, "token": token };
    console.log(formData);
    return this.http.post(environment.urls.auth.login(), formData, {});
  }
  register(NickName, email, userMobile, Pass, usercity, userArea, userPincode, userState, username, userBirthday, userPhoto,
    preferredGender, userbodytype, userheight, userchild, usersmoke, userdrinks, userzodiac, userRace, userPreference, userAboutme, latitude, longitude, userweight) {
    this.http.setDataSerializer('urlencoded');
    var formData = {
      "nickName": NickName, "userEmail": email,
      "userMobile": userMobile, "pass": Pass,
      "username": username, "userBirthday": userBirthday, "profileimg": userPhoto,
      "cityName": usercity, "addressName": userArea, "userPincode": userPincode, "userState": userState,
      "preferredGender": preferredGender, "userbodytype": userbodytype, "userheight": userheight, "userchild": userchild,
      "usersmoke": usersmoke, "userdrinks": userdrinks, "userzodiac": userzodiac, "userRace": userRace, "userPreference": userPreference, "userAboutme": userAboutme,
      "latitude": latitude, "longitude": longitude, "userweight": userweight
    };
    console.log(formData);
    return this.http.post(environment.urls.auth.registerLong(), formData, {});
  }
  getPhotoList(userInfo) {
    this.http.setDataSerializer('urlencoded');
    var formData = {
      "user_id": userInfo.user_id,
    };
    console.log(formData);
    return this.http.post(environment.urls.auth.photoList(), formData, {});
  }
  updateProfile(userInfo) {
    this.http.setDataSerializer('urlencoded');
    var formData = {
      "user_id": userInfo.user_id,
      "name": userInfo.name,
      "gender": userInfo.gender,
      "birthdate": userInfo.birthdate, "email_address": userInfo.email_address,
      "city": userInfo.city, "zipcode": userInfo.zipcode,
      "body_type": userInfo.body_type, "height": userInfo.height,
      "education": userInfo.education, "no_of_children": userInfo.no_of_children,
      "smoke": userInfo.smoke, "Interested": userInfo.Interested
    };
    console.log(formData);
    return this.http.post(environment.urls.auth.updateProfile(), formData, {});
  }
  registerImage(usersmoke) {
    this.http.setDataSerializer('multipart');
    this.http.setRequestTimeout(7000);
    return this.http.post(environment.urls.auth.updateProfileImg(),
      usersmoke, {});
  }
  googleLogin(userEmail, token) {
    this.http.setDataSerializer('urlencoded');
    var formData = { "userEmail": userEmail, "googleID": "", "token": token };
    return this.http.post(environment.urls.auth.googleLogin(), formData, {});
  }
  facebookLogin(userEmail, facebookId, token) {
    this.http.setDataSerializer('urlencoded');
    var formData = { "userEmail": userEmail, "facebookId": facebookId, "token": token };
    return this.http.post(environment.urls.auth.facebookLogin(), formData, {});
  }
}
