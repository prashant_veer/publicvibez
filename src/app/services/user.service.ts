import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../environments/environment';
import { GeneralService } from '../services/general';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HTTP,public general:GeneralService) { }
  uploaduserImg(photos):any{
    this.http.setDataSerializer('urlencoded');
    this.http.setRequestTimeout(300);
    var formData ={
      "user_id":this.general.getUserInfo().user_id,
      "photos":photos,
    };
    console.log(formData);
    return this.http.post(environment.urls.user.addPhotos(), formData,{});
  }
  checkPhoto(chat):any{
    this.http.setDataSerializer('urlencoded');
    this.http.setRequestTimeout(300);
    var formData ={
      "user_id":this.general.getUserInfo().user_id,
      "chat_group":chat.chat_guid
    };
    return this.http.post(environment.urls.chat.checkPhoto(), formData,{});
  }
  userList(range,latitude,longitude)  {
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"latitude":latitude,"longitude":longitude,
    "radarrange":range,
    "user_id":this.general.getUserInfo().user_id,
    "Interested":this.general.getUserInfo().Interested};
    console.log(formData);
    return this.http.post(environment.urls.user.list(),formData,{});
  }
  getuserlistByZip(zipCode)  {
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"zipCode":zipCode,"user_id":this.general.getUserInfo().user_id};
    console.log(formData);
    return this.http.post(environment.urls.user.getUserByZip(),formData,{});
  }
  SendInvitation(senderid)  {
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"senderid":senderid,"user_id":this.general.getUserInfo().user_id};
    console.log(formData);
    return this.http.post(environment.urls.user.sendInvitation(),formData,{});
  }
  sendPhotoVarifi(chat)  {
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={
      "user_id":chat.user_id,
      "chat_group":chat.chat_guid
    };
    console.log(formData);
    return this.http.post(environment.urls.chat.sendPhotoVarifi(),formData,{});
  }
  addPhotos(chat,Photos){
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={
      "photo_verify_id":chat.photo_verify_id,
      "Photos":Photos
    };
    console.log(formData);
    return this.http.post(environment.urls.chat.addPhotoVarifi(),formData,{});

  }
  getUserInvitationList()  {
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"user_id":this.general.getUserInfo().user_id};
    console.log(formData);
    return this.http.post(environment.urls.user.getInvitationById(),formData,{});
  }
  getprofileById(userId){
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"userId":this.general.getUserInfo().user_id};
    console.log(formData);
    return this.http.post(environment.urls.user.getProfileById(),formData,{});

  }
  setChatStatus(groupId,Status){
    this.http.setDataSerializer('urlencoded');
    console.log(this.general.getUserInfo());
    var formData ={"groupId":groupId,"Status":Status};
    console.log(formData);
    return this.http.post(environment.urls.user.setChatStatus(),formData,{});

  }
  setPhotoStatus(photoVarifyId,Status){
    this.http.setDataSerializer('urlencoded');
    var formData ={"photoVarifyId":photoVarifyId,"Status":Status};
    console.log(formData);
    return this.http.post(environment.urls.chat.setPhotoStatus(),formData,{});

  }
}
