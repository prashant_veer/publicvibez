import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { GeneralService } from "./general";

@Injectable({
  providedIn: "root"
})
export class LoggedAuthGuard implements CanActivate {
  constructor(private router: Router, public gen: GeneralService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // console.log("LoggedAuthGuard");
    // console.log(this.gen.getUserInfo());

    // if (!this.gen.getUserInfo()) {
    //   this.router.navigate(["login"]);
    //   return false;
    // }

    return true;
  }
}