import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PresentToastService {
  isShowToast = false;

  constructor(public toastController: ToastController) { }

  async presentToast(message: any) {
    this.isShowToast = true;
    return await this.toastController.create({
      message,
      position: 'middle',
      cssClass: 'toast-back',
      duration: 1500,
    }).then(a => {
      a.present().then(() => {
         // console.log('presented');
      });
    });
  }

}
