import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { GeneralService } from "./general";

@Injectable({
  providedIn: "root"
})
export class SignupAuthGuard implements CanActivate {
  constructor(private router: Router, public gen: GeneralService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // console.log(route);

    // if (!this.gen.isSignup) {
    //   this.router.navigate(["login"]);
    //   return false;
    // }
    return true;
  }
}

