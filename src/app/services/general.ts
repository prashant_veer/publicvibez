import { Injectable } from '@angular/core';
import { AlertController,LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  public userInfo:any;
  public userbackImg:any;
  public invitaionCount:number =0;
  public loading:any;
  public isSignup:boolean = false;

  private secureKey: string;
  private secureIV: string;

  constructor(private device: Device,private toastCtrl: ToastController,public alertController: AlertController,public loadingController: LoadingController) { 
    this.generateSecureKeyAndIV(); 
  }

  async generateSecureKeyAndIV() {

    this.secureKey = "2312231276567" // Returns a 32 bytes string
    this.secureIV = this.device.uuid // Returns a 16 bytes string
 }
  setIsSignup(type){
    this.isSignup = type;
  }
  async presentLoading(message) {
    this.loading =  await this.loadingController.create({
      message: message
    });
    await this.loading.present();
  }
  async closePresentLoading() { 
    console.log(this.loading);
    if(this.loading){
      await  this.loading.dismiss();
    }
    
    console.log('Loading dismissed!');
  }
  restoreUserInfo(){
    var obj= this;
    obj.userInfo = JSON.parse(localStorage.getItem("userInfo"));
   
  }
  restoreUserPhoto(){
    var obj= this;
    obj.userbackImg  = JSON.parse(localStorage.getItem("userbackImg"));
  }
  setUserInfo(userInfoData){
    var obj= this;
    obj.userInfo = userInfoData;
    localStorage.setItem("userInfo",JSON.stringify(obj.userInfo));
  }
  setUserPhotos(img){
    var obj= this;
    obj.userbackImg = img;
    localStorage.setItem("userbackImg",JSON.stringify(obj.userbackImg));
  }
  getUserPhotos(){
    return this.userbackImg ;
  }
  getInvitaionCount(){
    return this.invitaionCount;
  }
  setInvitaionCount(count){
     this.invitaionCount = count;
  }
  getUserInfo(){
    return this.userInfo ;
  }
  async presentAlert(header,subHeader,message) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentToast(mess) {
    const toast = await this.toastCtrl.create({
      message: mess,
      duration: 2000
    });
    toast.present();
  }
}
