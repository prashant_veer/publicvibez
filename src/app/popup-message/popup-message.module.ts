import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopupMessagePageRoutingModule } from './popup-message-routing.module';

import { PopupMessagePage } from './popup-message.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopupMessagePageRoutingModule
  ],
  declarations: [PopupMessagePage]
})
export class PopupMessagePageModule {}
