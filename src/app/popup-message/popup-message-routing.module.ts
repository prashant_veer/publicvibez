import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopupMessagePage } from './popup-message.page';

const routes: Routes = [
  {
    path: '',
    component: PopupMessagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopupMessagePageRoutingModule {}
