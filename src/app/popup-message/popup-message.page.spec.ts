import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopupMessagePage } from './popup-message.page';

describe('PopupMessagePage', () => {
  let component: PopupMessagePage;
  let fixture: ComponentFixture<PopupMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopupMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
