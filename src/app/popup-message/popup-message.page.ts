import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popup-message',
  templateUrl: './popup-message.page.html',
  styleUrls: ['./popup-message.page.scss'],
})
export class PopupMessagePage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {
  }

  close() {
    this.modalController.dismiss({
      'dismissed': true
    });

    // this.popoverController.dismiss();
    // const onClosedData = 'Wrapped Up!';
    // await this.modalController.dismiss(onClosedData);
  }

}
