import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { NavController } from '@ionic/angular';
import { GeneralService } from '../../services/general';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  userInfo:any;
  public imgPath = environment.root_URL+"/Users/";
  url:string;
  public pageType:string = "";
  constructor(public gen:GeneralService, private navCtrl: NavController,private userService:UserService,private router: Router,private activatedroute: ActivatedRoute) { 
    var user = this.activatedroute.snapshot.paramMap.get('userProfile');
    this.pageType = this.activatedroute.snapshot.paramMap.get('pageType');
    this.userInfo = JSON.parse(user)
    this.url = environment.root_URL;
  }
  onclickBack(){
    if(this.pageType == '0'){
      this.navCtrl.navigateBack('home/map-component');
    }else{
      this.navCtrl.navigateBack('home/tabs/chat');
    }
  }
  ngOnInit() {
  }
  sendInvitaion(){
    this.gen.presentLoading("please wait..");
    this.userService.SendInvitation( this.userInfo.user_id).then(async req => {
      var v= JSON.parse(req.data);
      this.gen.closePresentLoading();
      console.log(v);
      if (v.success) {
        var Count = this.gen.getInvitaionCount();
        this.gen.setInvitaionCount(Count  + 1);
        //this.navCtrl.back();
        this.pageType = "2"
      } 
    });
  }
  approveChat(){
    this.gen.presentLoading("please wait..");
    this.userService.setChatStatus(this.userInfo.chat_guid,1).then(async req => {
      var v= JSON.parse(req.data);
      this.gen.closePresentLoading();
      console.log(v);
      if (v.success) {
        this.navCtrl.back();
      } 
    });
  }
  rejectChat(){
    this.gen.presentLoading("please wait..");
    this.userService.setChatStatus(this.userInfo.chat_guid,2).then(async req => {
      var v= JSON.parse(req.data);
      this.gen.closePresentLoading();
      if (v.success) {
        this.pageType = "2"
       // this.navCtrl.back();
      } 
    });
  }
}
