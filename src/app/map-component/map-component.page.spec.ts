import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapComponentPage } from './map-component.page';

describe('MapComponentPage', () => {
  let component: MapComponentPage;
  let fixture: ComponentFixture<MapComponentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapComponentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
