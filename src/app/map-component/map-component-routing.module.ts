import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponentPage } from './map-component.page';

const routes: Routes = [
  {
    path: '',
    component: MapComponentPage
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapComponentPageRoutingModule {}
