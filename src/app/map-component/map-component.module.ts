import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapComponentPageRoutingModule } from './map-component-routing.module';

import { MapComponentPage } from './map-component.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapComponentPageRoutingModule
  ],
  declarations: [MapComponentPage]
})
export class MapComponentPageModule {}
