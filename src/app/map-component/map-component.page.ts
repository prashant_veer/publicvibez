import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { environment } from '../../environments/environment';
import { GeneralService } from '../services/general';
@Component({
  selector: 'app-map-component',
  templateUrl: './map-component.page.html',
  styleUrls: ['./map-component.page.scss'],
})
export class MapComponentPage implements OnInit {
  public mapZipIcon:String;
  public userList:any = [];
  public invitaionCount:number =0;
  public imgPath = environment.root_URL;

  constructor(public gen:GeneralService, private userService:UserService,private router: Router,private activatedroute: ActivatedRoute) {
    this.mapZipIcon = this.activatedroute.snapshot.paramMap.get('mapZipIcon');
    
  }

  ngOnInit() {
    this.getuserList();
  }
  getUserProfile(user){
    console.log(this.gen.getUserInfo().isPad==0 && this.gen.getInvitaionCount() < 3);
    console.log(this.gen.getUserInfo().isPad==0 );
    console.log(this.gen.getInvitaionCount() < 3 );
    if(this.gen.getUserInfo().isPad==1 || (this.gen.getUserInfo().isPad==0 && this.gen.getInvitaionCount() < 3)){
      this.router.navigate(['home/map-component/user-profile', {
        userProfile: JSON.stringify(user) ,
        pageType:0
      }]);
    }else{
      this.gen.presentAlert('','','Please purchase package you have exceeded free limit');
    }
    
  }
  getuserList(){
    this.gen.presentLoading("please wait..");
    this.userService.getuserlistByZip( this.mapZipIcon).then(async req => {
      var v= JSON.parse(req.data);
      console.log(v);
      this.gen.closePresentLoading();
      if (v.success) {
          this.userList = v.data
          this.gen.setInvitaionCount(v.senderCount); 
      } 
    });
  }

}
