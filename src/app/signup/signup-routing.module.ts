import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupPage } from './signup.page';

const routes: Routes = [
  {
    path: '',
    component: SignupPage
  },
  {
    path: 'gettingstart',
    loadChildren: () => import('./gettingstart/gettingstart.module').then( m => m.GettingstartPageModule)
  },
  {
    path: 'registeraboutyou',
    loadChildren: () => import('./registeraboutyou/registeraboutyou.module').then( m => m.RegisteraboutyouPageModule)
  },
  {
    path: 'registerfinished',
    loadChildren: () => import('./registerfinished/registerfinished.module').then( m => m.RegisterfinishedPageModule)
  },
  {
    path: 'otpcheck',
    loadChildren: () => import('./otpcheck/otpcheck.module').then( m => m.OtpcheckPageModule)
  },
  {
    path: 'profile-location',
    loadChildren: () => import('./profile-location/profile-location.module').then( m => m.ProfileLocationPageModule)
  },
  {
    path: 'registersummary',
    loadChildren: () => import('./registersummary/registersummary.module').then( m => m.RegistersummaryPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupPageRoutingModule {}
