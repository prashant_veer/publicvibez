import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistersummaryPage } from './registersummary.page';

describe('RegistersummaryPage', () => {
  let component: RegistersummaryPage;
  let fixture: ComponentFixture<RegistersummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistersummaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistersummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
