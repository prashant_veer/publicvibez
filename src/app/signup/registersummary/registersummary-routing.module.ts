import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistersummaryPage } from './registersummary.page';

const routes: Routes = [
  {
    path: '',
    component: RegistersummaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistersummaryPageRoutingModule {}
