import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../services/general';
import { ToastController, ActionSheetController, Platform } from '@ionic/angular';
import { LoginService } from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

@Component({
  selector: 'app-registersummary',
  templateUrl: './registersummary.page.html',
  styleUrls: ['./registersummary.page.scss'],
})
export class RegistersummaryPage implements OnInit {

  img: any;

  NickName: string;
  email: string;
  Pass: string;
  username: string;
  userBirthday: string;
  userMobile: string;
  usercity: string;
  userArea: any;
  userPincode: any;
  userState: any;
  userPhoto: string;

  photoPath = environment.root_URL

  preferredGender: string;
  userbodytype: any;
  userheight: any;
  userweight: any;
  userchild: any;
  usersmoke: any;
  userdrinks: any;
  userzodiac: any;
  userRace: any;
  userPreference: any;
  userAboutme: any;

  bodytypetext: any;
  userheighttext: any;
  userchildtext: any;
  usereducationtext: any;
  usersmoketext: any;
  genderSelectedtext: any;
  userdrinkstext: any;
  zodiactext: any;
  petpeevestext: any;
  racetext: any;
  preferencetext: any;

  userZodiacarray: any = [];
  bodytypearray: any = [];
  userheightarray: any = [];
  userchildarray: any = [];
  userweightarray: any = [];
  usereducationarray: any = [];
  usersmokearray: any = [];
  genderprefer: any = [];
  userDrinksarray: any = [];
  latitude: string;
  longitude: string;

  constructor(
    private general: GeneralService, private filePath: FilePath, private file: File, private platform: Platform,
    private toastCtrl: ToastController, private webview: WebView, private camera: Camera,
    public router: Router, private activatedroute: ActivatedRoute, public actionSheetController: ActionSheetController,
    private authSvc: LoginService) { }

  ngOnInit() {
    
    this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
    this.email = this.activatedroute.snapshot.paramMap.get('email');
    this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
    this.username = this.activatedroute.snapshot.paramMap.get('username');
    this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
    this.userMobile = this.activatedroute.snapshot.paramMap.get('userMobile');
    this.userArea = this.activatedroute.snapshot.paramMap.get('userArea');
    this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
    this.userPincode = this.activatedroute.snapshot.paramMap.get('userPincode');
    this.userState = this.activatedroute.snapshot.paramMap.get('userState');
    this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
    this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
    this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    this.preferredGender = this.activatedroute.snapshot.paramMap.get('preferredGender');
    this.userzodiac = this.activatedroute.snapshot.paramMap.get('userzodiac');
    this.userheight = this.activatedroute.snapshot.paramMap.get('userheight');
    this.userweight = this.activatedroute.snapshot.paramMap.get('userweight');

    this.bodytypetext = this.activatedroute.snapshot.paramMap.get('userbodytype');

    this.userchildtext = this.activatedroute.snapshot.paramMap.get('userchild');

    this.usereducationtext = this.activatedroute.snapshot.paramMap.get('usersmoke');
    this.usersmoketext = this.activatedroute.snapshot.paramMap.get('usersmoke');

    this.userdrinkstext = this.activatedroute.snapshot.paramMap.get('usersmoke');
    this.zodiactext = this.activatedroute.snapshot.paramMap.get('userRace');
    this.petpeevestext = this.activatedroute.snapshot.paramMap.get('userRace');
    this.racetext = this.activatedroute.snapshot.paramMap.get('userRace');
    this.userPreference = this.activatedroute.snapshot.paramMap.get('userPreference');
    this.userAboutme = this.activatedroute.snapshot.paramMap.get('userAboutme');

    this.general.presentToast('Summry Birthday ' + this.userBirthday);

    this.showsmoke();
    this.showgender();
    this.showseight();
    this.showheight();
    this.showZodiac();
    this.showchild();
  }
  showchild() {
    this.userchildarray = [{ child: 'YES' }
      , { child: 'NO' }];
  }
  showZodiac() {
    this.userZodiacarray = [{ type: 'Aries' }, { type: 'Taurus' }, { type: 'Gemini' },
    { type: 'Cancer' }, { type: 'Leo' }, { type: 'Virgo' },
    { type: 'Libra' }, { type: 'Scorpio' }, { type: 'Sagittarius' },
    { type: 'Capricorn' }, { type: 'Aquarius and Pisces' }];
  }
  ionViewWillLeave() {
    this.general.closePresentLoading();
  }

  showheight() {
    this.userheightarray = [{ height: '<5 Foot' }
      , { height: '>5 Foot' }
      , { height: '>6 Foot' }
      , { height: '<6 Foot' }
    ];
  }

  showseight() {
    this.userweightarray = [{ val: '< 100' }
      , { val: '< 120' }
      , { val: '< 150' }
      , { val: '< 200' }
    ];
  }
  showgender() {
    this.genderprefer = [{ gendertype: 'Male' }
      , { gendertype: 'Female' }, { gendertype: 'Undecided' }];
  }
  showsmoke() {
    this.usersmokearray = [{ smoke: 'YES' }
      , { smoke: 'NO' }
    ];
  }

  goto_registerfinish() {
    if (this.bodytypetext === 'What\'s your body type?') {
      this.general.presentToast('Please select your body type.');
    } else if (this.userheighttext === 'What\'s your height?') {
      this.general.presentToast('Please select height.');
    } else if (this.userchildtext === 'Do you have children?') {
      this.general.presentToast('Please select details about children.');
    } else if (this.usersmoketext === 'do you smoke?') {
      this.general.presentToast('Please select details about smoking.');
    } else {
      this.general.presentLoading("Please wait... ");

      this.authSvc.register(this.NickName, this.email, this.userMobile, this.Pass,
        this.usercity, this.userArea, this.userPincode, this.userState,
        this.username, this.userBirthday, this.userPhoto,
        this.preferredGender, this.userbodytype, this.userheight, this.userchild,
        this.usersmoke, this.userdrinks, this.userzodiac, this.userRace, this.userPreference, this.userAboutme,
        this.latitude, this.longitude, this.userweight).then(async req => {
          this.general.closePresentLoading();
          console.log(req.data);
          var v = JSON.parse(req.data);
          console.log(v);
          if (v.success) {
            this.router.navigate(['signup/registerfinished', {
              "userAccount": v.userAccount
            }]);
          } else {
            this.errorToast(v.message);
          }
        });

    }
  }

  private async errorToast(message) {
    (await (await this.toastCtrl.create({
      header: 'Registration',
      color: 'danger',
      duration: 4000,
      message
    })).present());
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Photo From',
      buttons: [{
        text: 'Load from Library',
        role: 'destructive',
        icon: 'images',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          console.log('Delete clicked');
        }
      }, {
        text: 'Use Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          console.log('Share clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }

  createFileName() {
    const d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath: string, currentName: string, newFileName: string) {

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.general.presentToast('Error while storing file.');
    });
  }

  updateStoredImages(name: string) {
    const filePath = this.file.dataDirectory + name;
    const resPath = this.pathForImage(filePath);

    const newEntry = {
      name,
      path: resPath,
      filePath
    };
    this.img = newEntry;
    this.startUpload(newEntry);
  }

  pathForImage(img: any) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  startUpload(imgEntry: { filePath: string; }) {
    this.general.presentLoading('Uploading the image');
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (entry as FileEntry).file(file => this.readFile(file));
      })
      .catch(err => {
        this.general.presentToast('Error while reading file.');
      });
  }
  readFile(file: any) {
    const reader = new FileReader();
    reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append('file', imgBlob, file.name);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }
  uploadImageData(formData: FormData) {

    this.authSvc.registerImage(formData).then(req => {
      console.log(req);
      this.general.closePresentLoading();
      var v = JSON.parse(req.data);
      console.log(v.data);
      if (v.success) {
        this.userPhoto = v.data
      }
    }).catch((err) => { this.general.closePresentLoading(); })
  }
}
