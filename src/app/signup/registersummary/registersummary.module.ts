import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistersummaryPageRoutingModule } from './registersummary-routing.module';

import { RegistersummaryPage } from './registersummary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistersummaryPageRoutingModule
  ],
  declarations: [RegistersummaryPage]
})
export class RegistersummaryPageModule {}
