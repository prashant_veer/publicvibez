import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterfinishedPage } from './registerfinished.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterfinishedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterfinishedPageRoutingModule {}
