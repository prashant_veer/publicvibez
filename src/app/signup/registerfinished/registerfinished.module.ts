import { ModalwinComponent } from './../../modalwin/modalwin.component';
import { PopupMessagePage } from './../../popup-message/popup-message.page';
import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
// import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { RegisterfinishedPageRoutingModule } from './registerfinished-routing.module';
import { RegisterfinishedPage } from './registerfinished.page';
import { InfoModalComponent } from '../info-modal/info-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterfinishedPageRoutingModule
  ], providers:[],
  entryComponents : [ModalwinComponent,InfoModalComponent],
  exports:[InfoModalComponent],
  declarations: [ RegisterfinishedPage, ModalwinComponent,InfoModalComponent ],
})
export class RegisterfinishedPageModule {}
