import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RegisterfinishedPage } from './registerfinished.page';

describe('RegisterfinishedPage', () => {
  let component: RegisterfinishedPage;
  let fixture: ComponentFixture<RegisterfinishedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterfinishedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterfinishedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
