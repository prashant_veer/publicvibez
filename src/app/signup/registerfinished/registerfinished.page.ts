import { ModalwinComponent } from './../../modalwin/modalwin.component';
import { PopupMessagePage } from './../../popup-message/popup-message.page';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { ToastController, NavController, Platform } from '@ionic/angular';
import { GeneralService } from '../../services/general';
import { ModalController, PopoverController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
//  import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { InfoModalComponent } from '../info-modal/info-modal.component';

@Component({
  selector: 'app-registerfinished',
  templateUrl: './registerfinished.page.html',
  styleUrls: ['./registerfinished.page.scss'],
})
export class RegisterfinishedPage implements OnInit {

  userAccount: any;
  mycount = 10000;
  timer: any;
  usercount = 0 ; 
  showcount = 10000;


  constructor(
    public gen: GeneralService, private toastCtrl: ToastController, private authSvc: LoginService,
    public router: Router, private activatedroute: ActivatedRoute, public modalController: ModalController, private navCtrl: NavController,
    public popoverController: PopoverController, public alertController: AlertController, public platform: Platform
  ) {

  }


  ngOnInit() {
    this.userAccount = this.activatedroute.snapshot.paramMap.get('userAccount');
    //  this.run_count();
    this.stop_back();
   // this.get_user_count();
    this.mycount = this.mycount + parseInt(this.userAccount) ;
  }


  stop_back(){
    this.platform.backButton.subscribeWithPriority(9999, () => {
      document.addEventListener('backbutton', function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.gen.presentToast('Back Button disabled');
        console.log('hello');
      }, false);
    });
  }

  run_count() {
    this.timer = setInterval(() => {
      //run code
      if (this.mycount !== 30000) {
        this.mycount = this.mycount + 1;

      }
    }, 3000);
  }

  async presentPopover(ev: any) {
    const modal = await this.modalController.create({
      component: ModalwinComponent,
      cssClass: 'my-custom-modal-css',
      
    });
    return await modal.present();
    //   try {
    //   console.log('pop up executed');
    //   const popover = await this.popoverController.create({
    //     component: PopupMessagePage,
    //     event: ev,
    //     // translucent: true
    //   });
    //   return await popover.present();
    // } catch (e) {
    //   console.log('pop up executed with error ' + e);
    // }
  }


  async presentAlert(ev: any) {
    const alert = await this.alertController.create({
      header: 'Alert',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.presentPopover(ev);
          }

        }, {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }


}
