import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../services/general';
import { NavController, ActionSheetController, Platform, ToastController } from '@ionic/angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { LoginService } from '../../services/login.service';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { environment } from '../../../environments/environment';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-gettingstart',
  templateUrl: './gettingstart.page.html',
  styleUrls: ['./gettingstart.page.scss'],
})
export class GettingstartPage implements OnInit {

  NickName: string;
  email: any;
  Pass: string;
  userBirthday: any;
  username: string;
  googleID: string;
  facebookId: string;
  img: any;
  userMobile: any;
  userArea: any;
  userPincode: any;
  userState: any;
  userPhoto: any;
  usercity: string;
  latitude: string;
  longitude: string;
  preferredGender: string;
  genderprefer: any = [];

  constructor(
    private authSvc: LoginService, private platform: Platform, private filePath: FilePath, private file: File,
    private nav: NavController, private camera: Camera, private activatedroute: ActivatedRoute, private webview: WebView,
    public gen: GeneralService, public actionSheetController: ActionSheetController) { }

    updateMyDate($event) {
      const day: number = $event.detail.value.day.value;
      const month: number = $event.detail.value.month.value;
      const year: number = $event.detail.value.year.value;
  
      const birthdate = year + '-' + month + '-' + day;
      this.gen.presentToast('Check Birthday' + birthdate);

    }

  ngOnInit() {
    this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
    this.email = this.activatedroute.snapshot.paramMap.get('email');
    this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');

    this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
    this.userArea = this.activatedroute.snapshot.paramMap.get('area');
    this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
    this.userState = this.activatedroute.snapshot.paramMap.get('state');
    this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
    this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
    this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    this.showgender();
  }
  ionViewWillLeave() {
    this.gen.closePresentLoading();
  }
  showgender() {
    this.genderprefer = [{ gendertype: 'Male' }
      , { gendertype: 'Female' }, { gendertype: 'Undecided' }];
  }
  register_photo() {
    if (this.userPhoto === undefined || this.userPhoto === '') {
      this.gen.presentToast('Please select your profile image');
      this.selectImage();
    } else if (this.preferredGender === undefined || this.preferredGender === '') {
      this.gen.presentToast('Please select gender');
    } else if (this.userBirthday === undefined || this.userBirthday === '') {
      this.gen.presentToast('Please select your birthdate');
    } else {
      // this.gen.presentToast('Getting Birthday' + this.userBirthday);
      this.nav.navigateForward(['signup/registeraboutyou', {
        NickName: this.NickName,
        email: this.email,
        Pass: this.Pass,
        username: this.username,
        userBirthday: this.userBirthday,
        mobile: this.userMobile,
        area: this.userArea,
        pincode: this.userPincode,
        state: this.userState,
        userPhoto: this.userPhoto,
        usercity: this.usercity,
        latitude: this.latitude,
        longitude: this.longitude,
        preferredGender: this.preferredGender
      }]);
    }
  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Photo From',
      buttons: [{
        text: 'Load from Library',
        role: 'destructive',
        icon: 'images',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          console.log('Delete clicked');
        }
      }, {
        text: 'Use Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          console.log('Share clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }

  createFileName() {
    const d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath: string, currentName: string, newFileName: string) {

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.gen.presentToast('Error while storing file.');
    });
  }

  updateStoredImages(name: string) {
    const filePath = this.file.dataDirectory + name;
    const resPath = this.pathForImage(filePath);

    const newEntry = {
      name,
      path: resPath,
      filePath
    };
    this.img = newEntry;
    this.startUpload(newEntry);
  }

  pathForImage(img: any) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  startUpload(imgEntry: { filePath: string; }) {
    this.gen.presentLoading('Uploading the image');
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (entry as FileEntry).file(file => this.readFile(file));
      })
      .catch(err => {
        this.gen.presentToast('Error while reading file.');
      });
  }
  readFile(file: any) {
    const reader = new FileReader();
    reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      formData.append('file', imgBlob, file.name);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }
  uploadImageData(formData: FormData) {

    this.authSvc.registerImage(formData).then(req => {
      console.log(req);
      this.gen.closePresentLoading();
      var v = JSON.parse(req.data);
      console.log(v.data);
      if (v.success) {
        this.userPhoto = v.data
      }
    }).catch((err) => { this.gen.closePresentLoading(); })
  }
}
