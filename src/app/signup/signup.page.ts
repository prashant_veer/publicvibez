import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GeneralService } from '../services/general';
import { NavController, } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import { ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  userEmail: any;
  userNickName: String;
  userMobile: any;
  emailpattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
  mobilePattern = '/^([+]\d{2})?\d{10}$/';
  userPass: String;
  locationchecked: any;
  usercity: string;
  userArea: any;
  userPincode: any;
  userState: any;
  latitude: number;
  longitude: number;
  geoAddress: any;
  autoLocation = false;
  constructor(
    private toastCtrl: ToastController, private authSvc: LoginService, private nav: NavController,
    private googlePlus: GooglePlus, private fb: Facebook, public gen: GeneralService,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {

    // this.gen.presentLoading("");
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };

      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
        .then((result: NativeGeocoderResult[]) => {
          console.log(JSON.stringify(result))
          this.gen.closePresentLoading();
          this.userArea = result[0].subLocality;
          this.usercity = result[0].locality;
          this.userPincode = result[0].postalCode;
          this.userState = result[0].administrativeArea;

          // this.gen.presentToast('area ' + this.userArea + 'city ' + this.usercity + 'pincode ' + 
          // this.userPincode + 'state ' + this.userState);
        })
        .catch((error: any) => { 
          // this.gen.closePresentLoading();
          // this.gen.presentToast('Error in location');
          console.log('Error getting location', error);
        });
    }).catch((error) => {
      // this.gen.closePresentLoading();
      // this.gen.presentToast('Error in address location');
      console.log('Error getting location', error);
    });

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log('address ' + JSON.stringify(result[0]));
        this.geoAddress = this.generateAddress(result[0]);
      }

      )
      .catch((error: any) => console.log(error));
  }

  // Return Comma saperated address
  generateAddress(addressObj) {
    let obj = [];
    let address = '';
    // tslint:disable-next-line:forin
    for (const key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length) {
        address += obj[val] + ', ';
      }
    }
    return address.slice(0, -2);
  }

  checkEvent() {
    this.autoLocation = !this.autoLocation;
    if (this.autoLocation === true) {
      this.gen.presentToast('You need to fill your location data Manually.');
      this.autoLocation = false;
    } else {
      this.gen.presentToast('Auto Location Check Allowed.');
      this.autoLocation = true;
    }

  }

  goto_getstart() {
    if (this.userNickName === undefined || this.userNickName === '') {
      this.gen.presentToast('Please enter Nickname');
    } else if (this.userEmail === undefined || this.userEmail === '') {
      this.gen.presentToast('Please enter Email id');
    } else if (!this.userEmail.match(this.emailpattern)) {
      this.gen.presentToast('Please enter proper Email id');
    } else if (this.userMobile === undefined || this.userMobile === '') {
      this.gen.presentToast('Please enter mobile number with country code.');
    } else if (this.userPass === undefined || this.userPass === '') {
      this.gen.presentToast('Please enter password');
    } else {
      this.gen.presentLoading("Please wait... ");
      this.authSvc.sendotptoEmail(this.userEmail, this.userMobile, this.userNickName).then(async req => {
        this.gen.closePresentLoading();
        var v = JSON.parse(req.data);
        if (v.success) {
          this.nav.navigateForward(['signup/otpcheck', {
            email: this.userEmail,
            NickName: this.userNickName,
            mobile: this.userMobile,
            Pass: this.userPass,
            location: this.autoLocation,
            area: this.userArea,
            pincode: this.userPincode,
            state: this.userState,
            usercity: this.usercity,
            latitude: this.latitude,
            longitude: this.longitude,
            otp: v.passcode
          }]);
        } else {
          this.errorToast(v.message);
        }
      });

    }
  }
  goto_google() {

    this.googlePlus.login({})
      .then(res => {
        console.log(res),
          this.nav.navigateForward(['signup/gettingstart', {
            email: res.email, name: res.displayName,
            googleID: res.userId, facebookId: ""
          }]);

      })
      .catch(err => {
        console.log(err)
      });
  }
  goto_Facebook() {
    this.fb.getLoginStatus().then((res) => {
      console.log(res);
      if (res.status === 'connected') {
        this.getUserDetail(res.authResponse.userID);
      } else {
        this.fb.login(['email'])
          .then(res => {
            if (res.status === 'connected') {
              this.getUserDetail(res.authResponse.userID);
            } else {
            }
          })
          .catch(e => console.log('Error logging into Facebook', e));
      }
    });

  }
  getUserDetail(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name', ['public_profile'])
      .then(res => {
        console.log(res);
        this.nav.navigateForward(['signup/gettingstart', {
          name: res.name, email: res.email,
          googleID: "", facebookId: res.id
        }]);
        //this.users = res;
      })
      .catch(e => {
        console.log(e);
      });
  }
  private async errorToast(message) {
    (await (await this.toastCtrl.create({
      header: 'Registration',
      color: 'danger',
      duration: 4000,
      message
    })).present());
  }
}
