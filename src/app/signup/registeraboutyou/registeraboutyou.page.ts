import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GeneralService } from '../../services/general';
import { ToastController } from '@ionic/angular';
import { LoginService } from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registeraboutyou',
  templateUrl: './registeraboutyou.page.html',
  styleUrls: ['./registeraboutyou.page.scss'],
})
export class RegisteraboutyouPage implements OnInit {

  NickName: string;
  email: string;
  Pass: string;
  username: string;
  userBirthday: string;
  userMobile: string;
  usercity: string;
  userArea: any;
  userPincode: any;
  userState: any;
  userPhoto: any;
  preferredGender:any;
  
  userbodytype: any;
  userheight: any;
  userweight:any;
  userchild: any;
  usersmoke: any;
  userdrinks: any;
  userzodiac: any;
  userRace: any;
  userPreference: any;
  userAboutme: any;

  bodytypetext: any;
  userheighttext: any;
  userchildtext: any;
  usereducationtext: any;
  usersmoketext: any;
  genderSelectedtext: any;
  userdrinkstext: any;
  zodiactext: any;
  petpeevestext: any;
  racetext: any;
  preferencetext: any;

  genderprefer:any = [];
  bodytypearray: any = [];
  userheightarray: any = [];
  userweightarray:any = [];
  userchildarray: any = [];
  usereducationarray: any = [];
  usersmokearray: any = [];
  userDrinksarray: any = [];
  userZodiacarray:any = [];
  latitude: string;
  longitude: string;

  constructor(
    private general: GeneralService,
    private toastCtrl: ToastController,
    public router: Router, private activatedroute: ActivatedRoute,
    private authSvc: LoginService) { }

  ngOnInit() {

    this.NickName = this.activatedroute.snapshot.paramMap.get('NickName');
    this.email = this.activatedroute.snapshot.paramMap.get('email');
    this.Pass = this.activatedroute.snapshot.paramMap.get('Pass');
    this.username = this.activatedroute.snapshot.paramMap.get('username');
    this.userBirthday = this.activatedroute.snapshot.paramMap.get('userBirthday');
    this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
    this.userArea = this.activatedroute.snapshot.paramMap.get('area');
    this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
    this.userState = this.activatedroute.snapshot.paramMap.get('state');
    this.userPhoto = this.activatedroute.snapshot.paramMap.get('userPhoto');
    this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
    this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
    this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    this.preferredGender = this.activatedroute.snapshot.paramMap.get('preferredGender');

    this.genderSelectedtext = 'Gender';
    this.bodytypetext = 'What\'s your body type?';
    this.userheighttext = 'What\'s your height?';
    this.userchildtext = 'Do you have children?';
    this.usersmoketext = 'do you smoke?';
    this.userdrinkstext = 'do you drink?';
    this.zodiactext = 'zodiac';


    this.showbodytype();
    this.showchild();
    this.showheight();
    this.showsmoke();
    this.showseight();
    this.showgender();
    this.showZodiac();
  }
  
  showZodiac(){
    this.userZodiacarray = [{ type: 'Aries' }, { type: 'Taurus' },{ type: 'Gemini' },
    { type: 'Cancer' },{ type: 'Leo' },{ type: 'Virgo' },
    { type: 'Libra' },{ type: 'Scorpio' },{ type: 'Sagittarius' },
    { type: 'Capricorn' },{ type: 'Aquarius and Pisces' }];
  }
  showgender() {
    this.genderprefer = [{ gendertype: 'Male' }
      , { gendertype: 'Female' }, { gendertype: 'Undecided' }];
  }
  showseight(){
    this.userweightarray = [{ val: '< 100' }
    , { val: '< 120' }
    , { val: '< 150' }
    , { val: '< 200' }
  ];
  }
  showbodytype() {
    this.bodytypearray = [{ bodytype: 'Muscles' }
      , { bodytype: 'Athletic' }
      , { bodytype: 'Curvy' }
      , { bodytype: 'Overweight' }
      , { bodytype: 'Underweight' }
    ];
  }

  bodytypeValue(event: any) {
    console.log(event.detail.value);
    this.bodytypetext = event.detail.value;
  }

  genderValue(event: any) {
    console.log(event.detail.value);
    this.genderSelectedtext = event.detail.value;
  }
  showheight() {
    this.userheightarray = [{ height: '<5 Foot' }
      , { height: '>5 Foot' }
      , { height: '>6 Foot' }
      , { height: '<6 Foot' }
    ];
  }

  heightValue(event: any) {
    console.log(event.detail.value);
    this.userheighttext = event.detail.value;
  }

  showchild() {
    this.userchildarray = [{ child: 'YES' }
      , { child: 'NO' }];
  }

  childValue(event: any) {
    console.log(event.detail.value);
    this.userchildtext = event.detail.value;
  }

  showsmoke() {
    this.usersmokearray = [{ smoke: 'YES' }
      , { smoke: 'NO' }
    ];
  }
  smokeValue(event: any) {
    console.log(event.detail.value);
    this.usersmoketext = event.detail.value;
  }

  goto_registerfinish() {
     if (this.userheight == undefined || this.userheight === '') {
      this.general.presentToast('Please select Preference');
    }else if (this.userweight == undefined || this.userweight === '') {
      this.general.presentToast('Please select Preference');
    }else if (this.userzodiac == undefined || this.userzodiac === '') {
      this.general.presentToast('Please select Preference');
    }else if (this.userchild == undefined || this.userchild === '') {
      this.general.presentToast('Please select Preference');
    }else if (this.userPreference == undefined || this.userPreference === '') {
      this.general.presentToast('Please select Preference');
    } else {
      this.general.presentLoading("Please wait... ");

      this.authSvc.register(this.NickName, this.email, this.userMobile, this.Pass,
        this.usercity, this.userArea, this.userPincode, this.userState,
        this.username, this.userBirthday, this.userPhoto,
        this.preferredGender, this.userbodytype, this.userheight, this.userchild,
        this.usersmoke, this.userdrinks, this.userzodiac, this.userRace, this.userPreference, this.userAboutme,
        this.latitude, this.longitude, this.userweight).then(async req => {
          this.general.closePresentLoading();
          console.log(req.data);
          var v = JSON.parse(req.data);
          console.log(v);
          if (v.success) {
            this.router.navigate(['signup/registerfinished', {
              "userAccount": v.userAccount
            }]);
          } else {
            this.errorToast(v.message);
          }
        });


      // this.general.presentToast('about Birthday' + this.userBirthday);
      // this.router.navigate(['signup/registersummary', {
      //   NickName: this.NickName, email: this.email, userMobile: this.userMobile, Pass: this.Pass,
      //   usercity: this.usercity, userArea: this.userArea, userPincode: this.userPincode, userState: this.userState,
      //   username: this.username, userBirthday: this.userBirthday, userPhoto: this.userPhoto,
      //   preferredGender: this.preferredGender, userbodytype: this.userbodytype,userweight:this.userweight, userheight: this.userheight, 
      //   userchild: this.userchild,
      //   usersmoke: this.usersmoke, userdrinks: this.userdrinks, userzodiac: this.userzodiac, userRace: this.userRace, userPreference: 
      //   this.userPreference, userAboutme: this.userAboutme,
      //   latitude: this.latitude, longitude: this.longitude
      // }]);
      // this.authSvc.register( this.NickName, this.email, this.userMobile, this.Pass,
      //   this.usercity, this.userArea, this.userPincode,this.userState,
      //   this.username, this.userBirthday, this.userPhoto,
      //   this.preferredGender, this.userbodytype,this.userheight,this.userchild,
      //   this.usersmoke,this.userdrinks,this.userzodiac,this.userRace,this.userPreference,this.userAboutme,
      //   this.latitude, this.longitude).then(async req => {
      //     this.general.closePresentLoading();
      //     console.log(req.data);
      //     var v = JSON.parse(req.data);
      //     console.log(v);
      //     if (v.success) {
      //       this.router.navigate(['signup/registerfinished', {
      //         "NickName":this.NickName, "email":this.email, "userMobile":this.userMobile, "Pass":this.Pass,
      //         "usercity":this.usercity, "userArea":this.userArea, "userPincode":this.userPincode,"userState":this.userState,
      //         "username":this.username, "userBirthday":this.userBirthday, "userPhoto":this.userPhoto,
      //         "preferredGender":this.preferredGender, "userbodytype":this.userbodytype,"userheight":this.userheight,
                  // "userchild":this.userchild ,
      //         "usersmoke":this.usersmoke,"userdrinks":this.userdrinks,"userzodiac":this.userzodiac,"userRace":this.userRace,
      // "userPreference":this.userPreference,"userAboutme":this.userAboutme,
      //         "latitude":this.latitude, "longitude":this.longitude
      //       }]);
      //     } else {
      //       this.errorToast(v.message);
      //     }
      //   });

    }
  }

  private async errorToast(message) {
    (await (await this.toastCtrl.create({
      header: 'Registration',
      color: 'danger',
      duration: 4000,
      message
    })).present());
  }

}
