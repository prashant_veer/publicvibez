import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisteraboutyouPage } from './registeraboutyou.page';

const routes: Routes = [
  {
    path: '',
    component: RegisteraboutyouPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisteraboutyouPageRoutingModule {}
