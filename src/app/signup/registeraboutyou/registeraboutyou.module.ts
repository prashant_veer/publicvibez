import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisteraboutyouPageRoutingModule } from './registeraboutyou-routing.module';

import { RegisteraboutyouPage } from './registeraboutyou.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisteraboutyouPageRoutingModule
  ],
  declarations: [RegisteraboutyouPage]
})
export class RegisteraboutyouPageModule {}
