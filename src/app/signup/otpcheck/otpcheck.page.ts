import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../services/loading.service';
import { PresentToastService } from '../../services/present-toast.service';
import { NavController, } from '@ionic/angular';
import { GeneralService } from '../../services/general';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-otpcheck',
  templateUrl: './otpcheck.page.html',
  styleUrls: ['./otpcheck.page.scss'],
})
export class OtpcheckPage implements OnInit {

  myotp1: any;
  myotp2: any;
  myotp3: any;
  myotp4: any;
  myotp5: any;
  myotp6: any;

  smobilenumber: any;
  userEmail: any;
  userNickName: String;
  userPass: String;
  newotp: any;
  confirmOtp: string;
  autolocation: any;

  usercity: string;
  userArea: any;
  userPincode: any;
  userState: any;
  latitude: any;
  longitude: any;
  geoAddress: any;

  constructor(
    private router: Router, private activatedroute: ActivatedRoute, private nav: NavController, public gen: GeneralService,
    public presentToast: PresentToastService, private authSvc: LoginService, public loadingservice: LoadingService) { }

  next(el: { setFocus: () => void; }) {
    el.setFocus();
  }

  ngOnInit() {
    // this.myotp1.focus();
    this.smobilenumber = this.activatedroute.snapshot.paramMap.get('mobile');
    this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
    this.userNickName = this.activatedroute.snapshot.paramMap.get('NickName');
    this.userPass = this.activatedroute.snapshot.paramMap.get('Pass');
    this.newotp = this.activatedroute.snapshot.paramMap.get('otp');
    this.autolocation = this.activatedroute.snapshot.paramMap.get('location');
    this.userArea = this.activatedroute.snapshot.paramMap.get('area');
    this.userPincode = this.activatedroute.snapshot.paramMap.get('pincode');
    this.userState = this.activatedroute.snapshot.paramMap.get('state');
    this.usercity = this.activatedroute.snapshot.paramMap.get('usercity');
    this.latitude = this.activatedroute.snapshot.paramMap.get('latitude');
    this.longitude = this.activatedroute.snapshot.paramMap.get('longitude');
    console.log(this.newotp);
  }

  goto_getstart() {
    this.confirmOtp = this.myotp1 + '' + this.myotp2 + '' + this.myotp3 + '' + this.myotp4 + '' + this.myotp5 + '' + this.myotp6;
    if (this.confirmOtp !== this.newotp) {
      this.presentToast.presentToast('Please enter correct passcode');
      this.myotp1 = '';
      this.myotp2 = '';
      this.myotp3 = '';
      this.myotp4 = '';
      this.myotp5 = '';
      this.myotp6 = '';
    } else {
      this.nav.navigateForward(['signup/gettingstart', {
        email: this.userEmail,
        NickName: this.userNickName,
        mobile: this.smobilenumber,
        Pass: this.userPass,
        location: this.autolocation,
        area: this.userArea,
            pincode: this.userPincode,
            state: this.userState,
            usercity: this.usercity,
            latitude: this.latitude,
            longitude: this.longitude
      }]);
    }
  }
  goto_resendOtp() {
    this.gen.presentLoading("");
    this.authSvc.sendotptoEmail(this.userEmail, this.smobilenumber, this.userNickName).then(async req => {
      this.gen.closePresentLoading();
      var v = JSON.parse(req.data);

      if (v.success) {
        this.newotp = v.passcode;
      }
    });
  }
  ionViewWillLeave() {
    this.gen.closePresentLoading();
  }
}
