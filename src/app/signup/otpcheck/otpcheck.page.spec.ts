import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtpcheckPage } from './otpcheck.page';

describe('OtpcheckPage', () => {
  let component: OtpcheckPage;
  let fixture: ComponentFixture<OtpcheckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpcheckPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtpcheckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
