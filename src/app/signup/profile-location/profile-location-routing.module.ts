import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileLocationPage } from './profile-location.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileLocationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileLocationPageRoutingModule {}
