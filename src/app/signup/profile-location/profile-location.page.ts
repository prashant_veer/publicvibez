import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../services/general';
import { NavController, } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-profile-location',
  templateUrl: './profile-location.page.html',
  styleUrls: ['./profile-location.page.scss'],
})
export class ProfileLocationPage implements OnInit {

  userEmail: any;
  userNickName: String;
  userMobile: any;
  userPass: String;
  usercity: string;
  userArea: any;
  userPincode: any;
  userState: any;
  latitude: number;
  longitude: number;
  geoAddress: any;
  autolocation: any;
  pincodepattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';
  statepattern = '[A-Za-z0-9._%+-]{2,}@[a-zA-Z-_.]{2,}[.]{1}[a-zA-Z]{2,}';

  constructor(
    private nav: NavController, public gen: GeneralService,
    private activatedroute: ActivatedRoute, private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {

    this.userMobile = this.activatedroute.snapshot.paramMap.get('mobile');
    this.userEmail = this.activatedroute.snapshot.paramMap.get('email');
    this.userNickName = this.activatedroute.snapshot.paramMap.get('NickName');
    this.userPass = this.activatedroute.snapshot.paramMap.get('Pass');
    this.autolocation = this.activatedroute.snapshot.paramMap.get('location');
    if (this.autolocation === "true") {
      this.gen.presentLoading("");
      this.geolocation.getCurrentPosition().then((resp) => {
        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;
        let options: NativeGeocoderOptions = {
          useLocale: true,
          maxResults: 5
        };

        this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
          .then((result: NativeGeocoderResult[]) => {
            console.log(JSON.stringify(result))
            this.gen.closePresentLoading();
            this.userArea = result[0].subLocality;
            this.usercity = result[0].locality;
            this.userPincode = result[0].postalCode;
            this.userState = result[0].administrativeArea;
          })
          .catch((error: any) => { this.gen.closePresentLoading(); });
      }).catch((error) => {
        this.gen.closePresentLoading();
        console.log('Error getting location', error);
      });

      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };

      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude, options)
        .then((result: NativeGeocoderResult[]) => {
          console.log('address ' + JSON.stringify(result[0]));
          this.geoAddress = this.generateAddress(result[0]);
        }

        )
        .catch((error: any) => console.log(error));
    } else {
      this.gen.presentToast('Please enter area, City, Pincode and State');
    }


  }

  // Return Comma saperated address
  generateAddress(addressObj) {
    let obj = [];
    let address = '';
    // tslint:disable-next-line:forin
    for (const key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length) {
        address += obj[val] + ', ';
      }
    }
    return address.slice(0, -2);
  }

  goto_getstart() {

    if (this.userArea == "") {
      this.gen.presentToast('Please enter user area');
    } else if (this.usercity == "") {
      this.gen.presentToast('Please enter user city');
    } else if (this.userPincode == "") {
      this.gen.presentToast('Please enter user pin code');
    } else if (this.userState == "") {
      this.gen.presentToast('Please enter user state');
    } else {
      this.nav.navigateForward(['signup/gettingstart', {
        email: this.userEmail,
        NickName: this.userNickName,
        mobile: this.userMobile,
        Pass: this.userPass,
        area: this.userArea,
        pincode: this.userPincode,
        state: this.userState,
        usercity: this.usercity,
        latitude: this.latitude,
        longitude: this.longitude
      }]);
    }



  }
  ionViewWillLeave() {
    this.gen.closePresentLoading();
  }
}
