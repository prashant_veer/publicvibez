import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileLocationPageRoutingModule } from './profile-location-routing.module';

import { ProfileLocationPage } from './profile-location.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileLocationPageRoutingModule
  ],
  declarations: [ProfileLocationPage]
})
export class ProfileLocationPageModule {}
