import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostloginPage } from './postlogin.page';

const routes: Routes = [
  {
    path: '',
    component: PostloginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostloginPageRoutingModule {}
