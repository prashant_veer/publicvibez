import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostloginPageRoutingModule } from './postlogin-routing.module';

import { PostloginPage } from './postlogin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostloginPageRoutingModule
  ],
  declarations: [PostloginPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA], // here is the schema declaration to add
})
export class PostloginPageModule {}
