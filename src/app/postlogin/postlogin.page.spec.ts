import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostloginPage } from './postlogin.page';

describe('PostloginPage', () => {
  let component: PostloginPage;
  let fixture: ComponentFixture<PostloginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostloginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostloginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
