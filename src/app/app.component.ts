import { Component } from '@angular/core';

import { Platform,AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
declare var firebase: any;
// import * as firebase from  ‘fir’;


const firebaseConfig = {
  apiKey: 'AIzaSyC4S88b9n-9z0UjDERD5jOcR9AUfzxPMfI',
  authDomain: 'vibezapp-be48f.firebaseapp.com',
  databaseURL: 'https://publicvibez.firebaseio.com',
  projectId: 'vibezapp-be48f',
  storageBucket: 'publicvibez.appspot.com',
  messagingSenderId: '369096780393',
  appId: '1:369096780393:web:30e271ce4cf492bd9d90f8',
  measurementId: 'G-84Y2LPRX6V'
};

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public alertController: AlertController
  ) {
    this.initializeApp();
  }

  async presentAlertConfirm() {
    var obj = this;
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure logout app.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Okay',
          handler: () => {
            navigator[ 'app' ].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      setTimeout(() => {
        this.splashScreen.hide();
        // this.router.navigateByUrl('signup/registerfinished'); // landingpage
        // this.statusBar.styleDefault();
        // this.splashScreen.hide();
      }, 5000);
      
      firebase.initializeApp(firebaseConfig);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.platform.backButton.subscribe(() => {
      if(this.router.url.indexOf('/home/map') > 0 ){
          this.presentAlertConfirm();
      }
    });
  }
}
