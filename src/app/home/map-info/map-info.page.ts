import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { UserService } from '../../services/user.service';
import { GeneralService } from '../../services/general';
import { Router, ActivatedRoute } from '@angular/router';

declare var google: any;

@Component({
  selector: 'app-map-info',
  templateUrl: './map-info.page.html',
  styleUrls: ['./map-info.page.scss'],
})
export class MapInfoPage implements OnInit {
  latitude: any;
  longitude: any;
  radarrange: any = 30;
  useList: any = [];

  map: any;
  markers: any = [];
  @ViewChild('mapElement', { static: false }) mapNativeElement: ElementRef;

  constructor(
    private router: Router, public gen: GeneralService,
    private userService: UserService, private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {
    var obj = this;
    obj.geolocation.getCurrentPosition().then((resp) => {
      obj.latitude = resp.coords.latitude;
      obj.longitude = resp.coords.longitude;
      //  this.latitude =28.5821195;
      //  this.longitude = 77.3266991;
      obj.gen.presentLoading("Find user location..");
      obj.loadUserList(obj.radarrange);
    }).catch((error) => {
      obj.latitude = obj.gen.getUserInfo().Latitude;
      obj.longitude = obj.gen.getUserInfo().Longitude;
      obj.gen.presentLoading("Find user location..");
      // obj.loadUserList(obj.radarrange);
    });
  }

  ngAfterViewInit(): void {
    var obj = this;
    var latlng = new google.maps.LatLng(28.7041, 77.1025);
    obj.map = new google.maps.Map(this.mapNativeElement.nativeElement, {
      zoom: 12,
      center: latlng,
      mapTypeId: 'terrain'
    });
  }
  loadUserList(range) {
    var obj = this;
    var latlng = new google.maps.LatLng(obj.latitude, obj.longitude);
    obj.map.setCenter(latlng);
    obj.userService.userList(range, obj.latitude, obj.longitude).then(async req => {
      var v = JSON.parse(req.data);
      console.log(v);
      obj.gen.closePresentLoading();
      obj.deleteMarkers();
      if (v.success) {
        obj.useList = v.data;
        for (var i = 0; i < obj.useList.length; i++) {
          obj.addMarker(obj.useList[i]['Latitude'], obj.useList[i]['Longitude'], obj.useList[i]['zipcode']);
        }
      }
    });
  }
  addMarker(Longitude, Latitude, zipcode) {
    var obj = this;
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(Longitude, Latitude),
      map: obj.map,
      title: zipcode
    });
    obj.markers.push(marker);
    marker.addListener('click', function (mark, j) {
      console.log(marker);
      console.log(j);
      obj.getZipUserList(marker.title);
    });

  }
  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }
  // Sets the map on all markers in the array.
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  myrange(event: any) {
    console.log(event);
    this.radarrange = event.detail.value;
    console.log('range=' + event.detail.value);
    this.loadUserList(this.radarrange);
  }

  getZipUserList(zipCode) {
    this.router.navigate(['home/map-component', {
      mapZipIcon: zipCode,
    }]);
  }
}
