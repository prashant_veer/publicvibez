import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-resturant-details',
  templateUrl: './resturant-details.page.html',
  styleUrls: ['./resturant-details.page.scss'],
})
export class ResturantDetailsPage implements OnInit {

  constructor(private nav: NavController) { }

  ngOnInit() {
  }

  back_trending() {
    this.nav.navigateForward('home/tabs/trending');
  }

}
