import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResturantDetailsPageRoutingModule } from './resturant-details-routing.module';

import { ResturantDetailsPage } from './resturant-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResturantDetailsPageRoutingModule
  ],
  declarations: [ResturantDetailsPage]
})
export class ResturantDetailsPageModule {}
