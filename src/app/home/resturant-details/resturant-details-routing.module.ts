import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResturantDetailsPage } from './resturant-details.page';

const routes: Routes = [
  {
    path: '',
    component: ResturantDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResturantDetailsPageRoutingModule {}
