import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../../services/general';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  userInfo:any;
  url:string;

  constructor(public general:GeneralService,private router: Router) {}
  ngOnInit() {
    this.url = environment.root_URL;
   /* this.events.subscribe('user:updateProfile', () => {
      console.log("updateProfile");
      this.userInfo = this.general.getUserInfo();
    });*/
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter");
    this.userInfo = this.general.getUserInfo();
    console.log(this.userInfo);
  }
  onClickPhotos(){
    this.router.navigate(['home/gallery', {
      user:JSON.stringify(this.general.getUserInfo()) 
    }]).then( res =>{
      this.userInfo = this.general.getUserInfo();
    });
  }
  onClickEditProfile(){
    this.router.navigate(['home/editprofile', {
      user:JSON.stringify(this.general.getUserInfo()) 
    }]).then( res =>{
      console.log("refresh");
      this.userInfo = this.general.getUserInfo();
    });
  }

}
