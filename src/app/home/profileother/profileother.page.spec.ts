import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfileotherPage } from './profileother.page';

describe('ProfileotherPage', () => {
  let component: ProfileotherPage;
  let fixture: ComponentFixture<ProfileotherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileotherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileotherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
