import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profileother',
  templateUrl: './profileother.page.html',
  styleUrls: ['./profileother.page.scss'],
})
export class ProfileotherPage implements OnInit {


  NickName: string;
  email: any;
  Pass: string;
  userBirthday: any;
  username: string;
  googleID: string;
  facebookId: string;
  img: any;
  userMobile: any;
  userArea: any;
  userPincode: any;
  userState: any;

  preferredGender: any;
  userEmail: any;
  cityzip: any;
  profileimg: any;
  addressName: string;

  userbodytype: any;
  userheight: any;
  userchild: any;
  usereducation: any;
  usersmoke: any;
  userdrinks: any;
  userzodiac: any;
  userPetpeeves: any;
  userRace: any;
  userPreference: any;
  userAboutme: any;

  bodytypetext: any;
  userheighttext: any;
  userchildtext: any;
  usereducationtext: any;
  usersmoketext: any;
  genderSelectedtext: any;
  userdrinkstext: any;
  zodiactext: any;
  petpeevestext: any;
  racetext: any;
  preferencetext: any;

  bodytypearray: any = [];
  userheightarray: any = [];
  userchildarray: any = [];
  usereducationarray: any = [];
  usersmokearray: any = [];
  genderprefer: any = [];
  userDrinksarray: any = [];

  latitude: number;
  longitude: number;

  cityName: string;

  constructor(public nav: NavController) { }

  ngOnInit() {

    this.genderSelectedtext = 'Gender';
    this.bodytypetext = 'What\'s your body type?';
    this.userheighttext = 'What\'s your height?';
    this.userchildtext = 'Do you have children?';
    this.usereducationtext = 'What\'s your education?';
    this.usersmoketext = 'do you smoke?';
    this.userdrinkstext = 'do you drink?';
    this.zodiactext = 'zodiac';
    this.petpeevestext = 'Pet Peeves';

    this.showgender();
    this.showbodytype();
    this.showchild();
    this.showeducation();
    this.showheight();
    this.showsmoke();

  }

  showbodytype() {
    this.bodytypearray = [{ bodytype: 'Muscles' }
      , { bodytype: 'Athletic' }
      , { bodytype: 'Curvy' }
      , { bodytype: 'Overweight' }
      , { bodytype: 'Underweight' }
    ];
  }

  bodytypeValue(event: any) {
    console.log(event.detail.value);
    this.bodytypetext = event.detail.value;
  }

  
  showgender() {
    this.genderprefer = [{ gendertype: 'Male' }
      , { gendertype: 'Female' }
    ];
  }
  genderValue(event: any) {
    console.log(event.detail.value);
    this.genderSelectedtext = event.detail.value;
  }
  showheight() {
    this.userheightarray = [{ height: '<5 Foot' }
      , { height: '>5 Foot' }
      , { height: '>6 Foot' }
      , { height: '<6 Foot' }
    ];
  }

  heightValue(event: any) {
    console.log(event.detail.value);
    this.userheighttext = event.detail.value;
  }

  showchild() {
    this.userchildarray = [{ child: 'YES' }
      , { child: 'NO' }];
  }

  childValue(event: any) {
    console.log(event.detail.value);
    this.userchildtext = event.detail.value;
  }

  showeducation() {
    this.usereducationarray = [{ education: 'HS GED' }
      , { education: 'HS Graduate' }
      , { education: 'Attending College' }
      , { education: 'College Graduate' }
      , { education: 'Attending Advance Degree' }
      , { education: 'Graduate Degree' }
    ];
  }
  educationValue(event: any) {
    console.log(event.detail.value);
    this.usereducationtext = event.detail.value;
  }
  showsmoke() {
    this.usersmokearray = [{ smoke: 'YES' }
      , { smoke: 'NO' }
    ];
  }
  smokeValue(event: any) {
    console.log(event.detail.value);
    this.usersmoketext = event.detail.value;
  }

  back_livelist() {
  this.nav.navigateForward('home/tabs/live');
  }

}
