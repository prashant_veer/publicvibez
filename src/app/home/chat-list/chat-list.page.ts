import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { environment } from '../../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../services/general';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {

  public userList:any = [];
  public imgPath = environment.root_URL;
  constructor(private navCtrl: NavController,public gen:GeneralService, private userService:UserService,private router: Router,private activatedroute: ActivatedRoute) { }

  ngOnInit() {
    /*this.events.subscribe('user:updateProfile', () => {
      console.log("updateProfile");
      this.getUserList();
    });*/
  }
  ionViewWillEnter(){
    this.getUserList();
  }
  getUserList(){
    this.userService.getUserInvitationList().then(async req => {
      var v= JSON.parse(req.data);
      console.log(v);
      if (v.success) {
        console.log( v.data);
          this.userList = v.data;
      } 
    });
  }
  openUserProfile(user){
    if(user.isVerify==0 ){
      this.router.navigate(['home/map-component/user-profile', {
        userProfile: JSON.stringify(user) ,
        pageType:1
      }]);
    }else if(user.isVerify==1){
      this.router.navigate(['home/chat', {
        userProfile: JSON.stringify(user) ,
        pageType:1
      }]);
    }

  }

}
