import { Component } from '@angular/core';
import { IonTabs } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public tabsList: any[] = [{
    name: 'Trending',
    label: 'Trending',
    }, {
    name: 'Map',
    label: 'Map',
    }, {
    name: 'Vibez',
    label: 'Live',
}, {
  name: 'Vibez',
  label: 'Vibez',
},{
  name: 'Chat',
  label: 'Chat',
}];

public tabs: IonTabs;
pageTitle: any;

  constructor() {}
  setTitle() {
    const currentTab: string = this.tabs.getSelected();
    const matchingTab: string = this.tabsList.filter((tab: any) => tab.name === currentTab)[0];
    // uses the first array element as the currentTab
    this.pageTitle = this.tabsList.filter((tab: any) => tab.name === currentTab)[0];
    alert(currentTab);

  }

  myevent(event) {
    let value = event.details;
    console.log('tab value' + value);
    this.pageTitle = value;

  }

}
