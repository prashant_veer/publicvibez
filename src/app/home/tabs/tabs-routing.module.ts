import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../map-info/map-info.module').then(m => m.MapInfoPageModule)
          }
        ]
      },
      {
        path: 'myprofile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../my-profile/my-profile.module').then(m => m.MyProfilePageModule)
          }
        ]
      },
      {
        path: 'membership',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../membership/membership.module').then(m => m.MembershipPageModule)
          }
        ]
      },
      {
        path: 'chat',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../chat-list/chat-list.module').then(m => m.ChatListPageModule)
          }
        ]
      },
      {
        path: 'trending',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../trending/trending.module').then(m => m.TrendingPageModule)
          }
        ]
      },
      {
        path: 'live',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../livelist/livelist.module').then(m => m.LivelistPageModule)
          }
        ]
      },
      {
        path: 'vibez',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../vibezlist/vibezlist.module').then(m => m.VibezlistPageModule)
          }
        ]
      },
      {
        path: 'resturant-details',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../resturant-details/resturant-details.module').then(m => m.ResturantDetailsPageModule)
          }
        ]
      },
      {
        path: 'resturant-feedback',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../resturant-feedback/resturant-feedback.module').then(m => m.ResturantFeedbackPageModule)
          }
        ]
      },
      {
        path: 'profile-otheruser',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profileother/profileother.module').then(m => m.ProfileotherPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/map',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/map',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
