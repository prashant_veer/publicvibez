import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtheruserProfilePage } from './otheruser-profile.page';

describe('OtheruserProfilePage', () => {
  let component: OtheruserProfilePage;
  let fixture: ComponentFixture<OtheruserProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtheruserProfilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtheruserProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
