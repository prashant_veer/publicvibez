import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtheruserProfilePageRoutingModule } from './otheruser-profile-routing.module';

import { OtheruserProfilePage } from './otheruser-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtheruserProfilePageRoutingModule
  ],
  declarations: [OtheruserProfilePage]
})
export class OtheruserProfilePageModule {}
