import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtheruserProfilePage } from './otheruser-profile.page';

const routes: Routes = [
  {
    path: '',
    component: OtheruserProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtheruserProfilePageRoutingModule {}
