import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-trending',
  templateUrl: './trending.page.html',
  styleUrls: ['./trending.page.scss'],
})
export class TrendingPage implements OnInit {

  constructor(private nav: NavController) { }

  ngOnInit() {
  }

  rest_feedback() {
    this.nav.navigateForward('home/tabs/resturant-details');
  }

  rest_details() {
    this.nav.navigateForward('home/tabs/resturant-feedback');
  }

}
