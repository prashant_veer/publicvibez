import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResturantFeedbackPage } from './resturant-feedback.page';

const routes: Routes = [
  {
    path: '',
    component: ResturantFeedbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResturantFeedbackPageRoutingModule {}
