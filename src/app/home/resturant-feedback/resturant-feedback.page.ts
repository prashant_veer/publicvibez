import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-resturant-feedback',
  templateUrl: './resturant-feedback.page.html',
  styleUrls: ['./resturant-feedback.page.scss'],
})
export class ResturantFeedbackPage implements OnInit {

  constructor(private nav: NavController) { }

  ngOnInit() {
  }
  
  back_trending() {
    this.nav.navigateForward('home/tabs/trending');
  }

}
