import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResturantFeedbackPageRoutingModule } from './resturant-feedback-routing.module';

import { ResturantFeedbackPage } from './resturant-feedback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResturantFeedbackPageRoutingModule
  ],
  declarations: [ResturantFeedbackPage]
})
export class ResturantFeedbackPageModule {}
