import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivelistPageRoutingModule } from './livelist-routing.module';

import { LivelistPage } from './livelist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivelistPageRoutingModule
  ],
  declarations: [LivelistPage]
})
export class LivelistPageModule {}
