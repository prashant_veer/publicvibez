import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LivelistPage } from './livelist.page';

const routes: Routes = [
  {
    path: '',
    component: LivelistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LivelistPageRoutingModule {}
