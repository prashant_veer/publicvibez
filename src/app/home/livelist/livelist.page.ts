import { NavController } from '@ionic/angular';
import { PresentToastService } from './../../services/present-toast.service';
import { Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-livelist',
  templateUrl: './livelist.page.html',
  styleUrls: ['./livelist.page.scss'],
})
export class LivelistPage implements OnInit {

  constructor(
    public presentToast: PresentToastService, public nav: NavController) { }

  ngOnInit() {
  }

  add_vibezlist() {
    this.presentToast.presentToast('User will be added to Vibez List');
  }

  add_blocklist() {
    this.presentToast.presentToast('User will be added to Blocked List');
  }

  sort_by_name() {
    this.presentToast.presentToast('User List will be sorted as per Names');
  }

  User_profile() {
    this.presentToast.presentToast('User Profile will be shown');
    this.nav.navigateForward('home/tabs/profile-otheruser');
  }

  User_location() {
    this.presentToast.presentToast('User Location will be shown on map');
  }

}
