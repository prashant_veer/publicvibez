import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LivelistPage } from './livelist.page';

describe('LivelistPage', () => {
  let component: LivelistPage;
  let fixture: ComponentFixture<LivelistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivelistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LivelistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
