import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VibezlistPage } from './vibezlist.page';

const routes: Routes = [
  {
    path: '',
    component: VibezlistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VibezlistPageRoutingModule {}
