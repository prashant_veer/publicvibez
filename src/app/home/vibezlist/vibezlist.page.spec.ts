import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VibezlistPage } from './vibezlist.page';

describe('VibezlistPage', () => {
  let component: VibezlistPage;
  let fixture: ComponentFixture<VibezlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VibezlistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VibezlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
