import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VibezlistPageRoutingModule } from './vibezlist-routing.module';

import { VibezlistPage } from './vibezlist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VibezlistPageRoutingModule
  ],
  declarations: [VibezlistPage]
})
export class VibezlistPageModule {}
