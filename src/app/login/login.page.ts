import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../services/general';
import { ToastController, NavController } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Router } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  useremail: any;
  userpwd: any;
  sessionUseremail: any;
  token: any;
  constructor(
    private nav: NavController, private fcm: FCM,
    private general: GeneralService,
    private toastCtrl: ToastController, private authSvc: LoginService,
    private googlePlus: GooglePlus, private fb: Facebook, private router: Router) { }

  ngOnInit() {
    var obj = this;
    // if (localStorage.getItem("userInfo") != null) {
    //   //obj.general.presentLoading("Please wait... ");
    //   setTimeout(function () {
    //     console.log("Please");
    //     obj.general.restoreUserInfo();
    //     obj.router.navigateByUrl('signup/registerfinished');
    //     // obj.router.navigateByUrl("/home/tabs/trending");
    //   }, 1000);
    // }
    obj.fcm.getToken().then(token => {
      console.log(token);
      obj.token = token;
    });
    obj.fcm.onNotification().subscribe(data => {
      if (data.wasTapped) {
        console.log("Received in background");
      }
    });
  }
  goto_tab() {
    if (!this.useremail || this.useremail == "") {
      this.errorToast("Please enter Email Id/Password");
    } else if (!this.userpwd || this.userpwd == "") {
      this.errorToast("Please enter password");
    } else {
     // this.router.navigateByUrl('signup/registerfinished');
     this.general.presentLoading("");
       this.authSvc.login(this.useremail, this.userpwd, this.token).then(async req => {
         var v = JSON.parse(req.data);
         this.general.closePresentLoading();
         if (v.success) {
           this.router.navigate(['signup/registerfinished', {
             "userAccount": v.userAccount
           }]);
         }else {
                this.errorToast(v.message);
         }
       });
    }

  }
  goto_social(res, socialType) {
    
    this.general.presentLoading("Please wait... ");
    this.authSvc.googleLogin(res.email, this.token).then(async req => {
      if (req.data) {
        var v = JSON.parse(req.data);
        this.general.closePresentLoading();
        if (v.success) {
          this.router.navigate(['signup/registerfinished', {
            "userAccount": v.userAccount
          }]);
        } else {
          this.errorToast(v.message);
        }
      }
    });
  }
  goto_google() {
    this.googlePlus.login({
    })
      .then(res => {
        this.goto_social(res, 2);
      })
      .catch(err => {
        console.error(err)
      });

  }
  goto_Facebook() {
    this.fb.getLoginStatus().then((res) => {
      console.log(res);
      if (res.status === 'connected') {
        this.getUserDetail(res.authResponse.userID);
      } else {
        this.fb.login(['email'])
          .then(res => {
            if (res.status === 'connected') {
              this.getUserDetail(res.authResponse.userID);
            } else {
            }
          })
          .catch(e => console.log('Error logging into Facebook', e));
      }
    });
  }
  getUserDetail(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then(res => {
        console.log(res);
        this.goto_social(res, 1);
        //this.users = res;
      })
      .catch(e => {
        console.log(e);
      });
  }
  goto_SignUp() {
    this.general.isSignup = true;
    this.nav.navigateForward("signup");
  }
  private async errorToast(message) {
    (await (await this.toastCtrl.create({
      header: 'Registration',
      color: 'danger',
      duration: 4000,
      message
    })).present());
  }
}
