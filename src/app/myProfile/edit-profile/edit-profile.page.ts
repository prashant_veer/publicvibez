import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../services/general';
import { LoginService } from '../../services/login.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  public userInfo:any;

  bodytypearray: any = [];
  userheightarray: any = [];
  userchildarray: any = [];
  usereducationarray: any = [];
  usersmokearray: any = [];
  genderprefer: any = [];
  genderSelectedtext: any;
  constructor(private activatedroute: ActivatedRoute,private toastCtrl: ToastController,private general:GeneralService,private authSvc: LoginService) { }

  ngOnInit() {
    this.genderSelectedtext = 'GENDER AND PREFERENCES';
    this.userInfo = JSON.parse(this.activatedroute.snapshot.paramMap.get('user')) ;
    console.log(this.userInfo);
    this.showbodytype();
    this.showchild();
    this.showeducation();
    this.showheight();
    this.showsmoke();
    this.showgenderPrefer();
  }
  ionViewDidLeave(){
   // this.events.publish('user:updateProfile');
  }
  showgenderPrefer() {
    this.genderprefer = [{ Gender: 'MALE' }
      , { Gender: 'FEMALE' }
      , { Gender: 'BOTH' }];
  }
  genderValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.gender = event.detail.value;
  }
  genderInterestedValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.Interested = event.detail.value;
  }
  showbodytype() {
    this.bodytypearray = [{ bodytype: 'Muscles' }
      , { bodytype: 'Athletic' }
      , { bodytype: 'Curvy' }
      , { bodytype: 'Overweight' }
      , { bodytype: 'Underweight' }
    ];
  }

  bodytypeValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.body_type = event.detail.value;
  }

  showheight() {
    this.userheightarray = [{ height: '<5 Foot' }
      , { height: '>5 Foot' }
      , { height: '>6 Foot' }
      , { height: '<6 Foot' }
    ];
  }

  heightValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.height = event.detail.value;
  }

  showchild() {
    this.userchildarray = [{ child: 'YES' }
      , { child: 'NO' }];
  }

  childValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.no_of_children = event.detail.value;
  }

  showeducation() {
    this.usereducationarray = [{ education: 'HS GED' }
      , { education: 'HS Graduate' }
      , { education: 'Attending College' }
      , { education: 'College Graduate' }
      , { education: 'Attending Advance Degree' }
      , { education: 'Graduate Degree' }
    ];
  }
  educationValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.education = event.detail.value;
  }
  showsmoke() {
    this.usersmokearray = [{ smoke: 'YES' }
      , { smoke: 'NO' }
    ];
  }
  smokeValue(event: any) {
    console.log(event.detail.value);
    this.userInfo.smoke = event.detail.value;
  }
  goto_updateProfile(){
    this.general.presentLoading("Please wait... ");
    this.authSvc.updateProfile(this.userInfo).then(async req => {
        this.general.closePresentLoading();
        var v= JSON.parse(req.data);
      if (v.success) {
        this.errorToast("Profile updated successfully");
        this.general.setUserInfo(this.userInfo);
      } else {
          this.errorToast(v.message);
      }
    });
  }


  private async errorToast(message) {
    (await (await this.toastCtrl.create({
        header: 'Registration',
        color: 'danger',
        duration: 4000,
        message
    })).present());
  }

}
