import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../services/general';
import { UserService } from '../../services/user.service';
import { LoginService } from '../../services/login.service';
import { ToastController,ActionSheetController,Platform } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.page.html',
  styleUrls: ['./photo-gallery.page.scss'],
})
export class PhotoGalleryPage implements OnInit {

  public userInfo:any;
  public userPhotos:any;
  public url = environment.root_URL;

  images = [];
  constructor(private webview: WebView,private ref: ChangeDetectorRef,private file: File,private filePath: FilePath,private platform: Platform, private camera: Camera,public actionSheetController: ActionSheetController,private activatedroute: ActivatedRoute,private toastCtrl: ToastController,private general:GeneralService,private authSvc: LoginService,private userSvc: UserService) { }

  ngOnInit() {

  }
  ionViewWillEnter(){
    this.userInfo = JSON.parse(this.activatedroute.snapshot.paramMap.get('user'));
    this.goto_profilePhoto();
  }
  goto_profilePhoto(){
    this.general.presentLoading("Please wait... ");
    this.authSvc.getPhotoList(this.userInfo).then(async req => {
        this.general.closePresentLoading();
        var v= JSON.parse(req.data);
        console.log(v);
      if (v.success) {
          this.userPhotos = v.data;
      }
    });
  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Photo From',
      buttons: [{
        text: 'Load from Library',
        role: 'destructive',
        icon: 'images',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          console.log('Delete clicked');
        }
      }, {
        text: 'Use Camera',
        role: 'destructive',
        icon: 'camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          console.log('Share clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  takePicture(sourceType: PictureSourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    this.camera.getPicture(options).then(imagePath => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });
  }
  createFileName() {
    const d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg';
    return newFileName;
  }
  copyFileToLocalDir(namePath: string, currentName: string, newFileName: string) {

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      console.log(error);
      this.general.presentToast('Error while storing file.');
    });
  }
  updateStoredImages(name: string) {
 
      const filePath = this.file.dataDirectory + name;
      const resPath = this.pathForImage(filePath);

      const newEntry = {
        name,
        path: resPath,
        filePath
      };

      this.images = [newEntry, ...this.images];
      console.log(this.images);
      this.ref.detectChanges(); // trigger change detection cycle
      this.startUpload(this.images[0]) ;

  }
  pathForImage(img: any) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
  startUpload(imgEntry: { filePath: string; }) {

    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
        (entry as FileEntry).file(file => this.readFile(file));
      })
      .catch(err => {
        this.general.presentToast('Error while reading file.');
      });
  }
  readFile(file: any) {
    console.log(file);
    const reader = new FileReader();
    reader.onload = () => {
      this.general.presentLoading('Uploading the image');
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
        type: file.type
      });
      console.log(imgBlob);
      formData.append('file', imgBlob, file.name);
      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }
   uploadImageData(formData: FormData) {
    // this.loadingController.present();
    var obj = this;
    console.log("uploadImageData");
 
    obj.authSvc.registerImage(formData).then( req => {
      console.log(req);
      var v = JSON.parse(req.data);
      
      if (v.success) {
        obj.userSvc.uploaduserImg(v.data).then( req => { 
          console.log("uploaduserImg");
          this.images = [];
          console.log(req);
          var imageReq = JSON.parse(req.data);
          obj.userPhotos.push({
            "photo_name":v.data,
            "photo_id":imageReq.photosId,
            "user_id":obj.general.getUserInfo().user_id
          });
          obj.general.closePresentLoading();
        });
        
      } else {
        obj.general.closePresentLoading();
        obj.general.presentToast(v.message);
      } 
      
    });
  }

}
