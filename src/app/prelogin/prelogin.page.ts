import { Component, OnInit } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { GeneralService } from '../services/general';
@Component({
  selector: 'app-prelogin',
  templateUrl: './prelogin.page.html',
  styleUrls: ['./prelogin.page.scss'],
})
export class PreloginPage implements OnInit {

  constructor(private nav:NavController,public gen:GeneralService) { }

  ngOnInit() {
  }


  goto_login() {
    this.nav.navigateForward('login');
  }

  goto_signup() {
    this.gen.setIsSignup(true);
    this.nav.navigateForward('signup');
  }
}
