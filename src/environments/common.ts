const BASE_URL = 'https://api.publicvibez.com/index.php/';
const path_URL = 'https://api.publicvibez.com/Users/';
export const common = {
    urls: {
        auth: {
            login: () => `${BASE_URL}Users/login`,
            forgetPassword: () => `${BASE_URL}Users/forgetPassword`,
            registerLong: () => `${BASE_URL}Users/registration`,
            updateProfile: () => `${BASE_URL}Users/updateProfile`,
            updateProfileImg: () => `${BASE_URL}Users/uploadUseePhoto`,
            facebookLogin: () => `${BASE_URL}Users/facebookLogin`,
            googleLogin: () => `${BASE_URL}Users/googleLogin`,
            photoList: () => `${BASE_URL}Users/getphotoList`,
            sendOtptoEmail: () => `${BASE_URL}Users/sendOtptoEmail`,
        },
        user: {
            count: () => `${BASE_URL}Users/userCount`,
            list: () => `${BASE_URL}Users/userList`,
            getUserByZip: () => `${BASE_URL}Users/userListByZipCode`,
            sendInvitation: () => `${BASE_URL}Users/sendInvitation`,
            getInvitationById: () => `${BASE_URL}Users/getInvitationById`,
            getProfileById: () => `${BASE_URL}Users/getProfileById`,
            setChatStatus: () => `${BASE_URL}Users/setChatStatus`,
            addPhotos: () => `${BASE_URL}Users/addPhotos`,
        },
        chat: {
            checkPhoto: () => `${BASE_URL}Users/checkPhoto`,
            sendPhotoVarifi: () => `${BASE_URL}Users/sendPhotoVarifi`,
            addPhotoVarifi: () => `${BASE_URL}Users/addPhotoVarifi`,
            setPhotoStatus: () => `${BASE_URL}Users/setPhotoStatus`,
        }
    },
    root_URL: path_URL
};
